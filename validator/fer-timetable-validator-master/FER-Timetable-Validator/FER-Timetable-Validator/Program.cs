﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace FER_Timetable_Validator
{
    internal class StudentsClass
    {
        public ulong student_id { get; set; }
        public ulong activity_id { get; set; }
        public ulong swap_weight { get; set; }
        public ulong group_id { get; set; }
        public ulong new_group_id { get; set; }
    }

    internal class RequestsClass
    {
        public ulong student_id { get; set; }
        public ulong activity_id { get; set; }
        public ulong req_group_id { get; set; }
    }

    internal class OverlapsClass
    {
        public ulong group1_id { get; set; }
        public ulong group2_id { get; set; }
    }

    internal class LimitsClass : ICloneable
    {
        public ulong group_id { get; set; }
        public ulong students_cnt { get; set; }
        public ulong min { get; set; }
        public ulong min_preferred { get; set; }
        public ulong max { get; set; }
        public ulong max_preferred { get; set; }

        public object Clone()
        {
            var newObject = new LimitsClass
            {
                group_id = group_id,
                students_cnt = students_cnt,
                min = min,
                min_preferred = min_preferred,
                max = max,
                max_preferred = max_preferred
            };
            return newObject;
        }
    }


    class Program
    {
        static int Main(string[] args)
        {
            // Input files:
            //  - student.csv 
            //  - requests.csv
            //  - overlaps.csv
            //  - limits.csv

            var studentsFile = "student.csv";
            var requestsFile = "requests.csv";
            var overlapsFile = "overlaps.csv";
            var limitsFile = "limits.csv";
            var debug = true;
            var calculateSolution = true;
            var verifySolution = true;
            var detailedCalculation = false;
            var groupSize = false;
            var maxSolution = true;
            string awardActivity = "1,2,3,4,5";
            long awardStudent = 1;
            long minMaxPenalty = 1;

            // Up to -1 since all parameters are "-name value" pairs
            try
            {
                if (args.Length % 2 == 1)
                {
                    HelpMessage();
                    return 1;
                }
                for (var i = 0; i < args.Length - 1; ++i)
                {
                    switch (args[i])
                    {
                        case "-debug":
                            debug = (args[++i] == "1");
                            break;
                        case "-calculate":
                            calculateSolution = (args[++i] == "1");
                            break;
                        case "-detailed":
                            detailedCalculation = (args[++i] == "1");
                            break;
                        case "-groupsize":
                            groupSize = (args[++i] == "1");
                            break;
                        case "-verify":
                            verifySolution = (args[++i] == "1");
                            break;
                        case "-maxvalue":
                            maxSolution = (args[++i] == "1");
                            break;
                        case "-award-activity":
                            awardActivity = args[++i];
                            break;
                        case "-award-student":
                            awardStudent = long.Parse(args[++i]);
                            break;
                        case "-minmax-penalty":
                            minMaxPenalty = long.Parse(args[++i]);
                            break;
                        case "-students-file":
                            studentsFile = args[++i];
                            break;
                        case "-requests-file":
                            requestsFile = args[++i];
                            break;
                        case "-overlaps-file":
                            overlapsFile = args[++i];
                            break;
                        case "-limits-file":
                            limitsFile = args[++i];
                            break;
                        default:
                            throw new Exception("Invalid parameter ("+i+"): " + args[i]);
                    }
                }
            }
            catch (Exception e)
            {
                HelpMessage();
                Console.WriteLine("Invalid program arguments.\n" + e.Message + "\n");
                return 1;
            }

            // Load all files
            var studentsContent = LoadFile<StudentsClass>(studentsFile, debug);
            var requestsContent = LoadFile<RequestsClass>(requestsFile, debug);
            var overlapsContent = LoadFile<OverlapsClass>(overlapsFile, debug);
            var limitsContent = LoadFile<LimitsClass>(limitsFile, debug);

            // Check if all files loaded correctly
            if (studentsContent == null || requestsContent == null || overlapsContent == null || limitsContent == null)
                return 2;

            if (verifySolution)
            {
                // Check the result
                if (!CheckForTheGroupLimits(studentsContent, limitsContent, debug))
                    return 3;

                if (!CheckForTheOverlaps(studentsContent, overlapsContent, debug))
                    return 4;

                if (!CheckForNotRequestedSwaps(studentsContent, requestsContent, debug))
                    return 5;

                if (!CheckForDoubleGroups(studentsContent, debug))
                    return 6;

                if (!CheckForLimitGrupsInRequests(studentsContent, limitsContent, debug))
                    return 7;

                if (!CheckForRequestsNotInInitialPlacement(studentsContent, requestsContent, debug))
                    return 8;
            }

            if (calculateSolution)
            {
                var solution = EvaluateSolution(studentsContent, limitsContent, requestsContent, awardActivity, awardStudent,
                    minMaxPenalty,
                    debug, detailedCalculation, groupSize);
                Console.WriteLine("Total solution value:\n" + solution + "\n");
            }


            if (maxSolution)
            {
                var maxSolutionValue = CalculateMaxSolutionValue(studentsContent, awardActivity, awardStudent);
                Console.WriteLine("Maximal solution value:\n" + maxSolutionValue + "\n");
            }

            return 0;
        }

        private static void HelpMessage()
        {
            Console.WriteLine("Available program parameters:\n" +
                              "  -debug 0     => Hide error message and detailed errors (default: 1)\n" +
                              "  -calculate 0 => Don't evaluate the solution - display total score (default: 1)\n" +
                              "  -detailed 1  => Display detailed scoring evaluation (default: 0)\n" +
                              "  -groupsize 1 => Display number of students in groups after the swap is performed (default: 0)\n" +
                              "  -verify 0    => Dont verify the solution (default: 1)\n" +
                              "  -maxvalue 0  => Display maximal available score (default: 1)\n" +
                              "\n" +
                              "Parameters for algorithm:\n" +
                              "  -award-student  0,2,4    => Additional score for swap combinations for a single student (default: 1,2,3,4)\n" +
                              "  -minmax-penalty 2        => Penalty for over or below max_preffered and min_preffered (default: 1)\n" +
                              "  -students-file stud.csv  => Name of students file (default: students.csv)\n" +
                              "  -requests-file reqs.csv  => Name of requests file (default: requests.csv)\n" +
                              "  -overlaps-file over.csv  => Name of overlaps file (default: overlaps.csv)\n" +
                              "  -limits-file limt.csv    => Name of limits file (default: limits.csv)\n" +
                              "\n");
        }

        /// <summary>
        /// Loads the CSV file into given structure - in case of a problem, displays the message and returns the null value
        /// </summary>
        /// <typeparam name="T">Class name for return value</typeparam>
        /// <param name="fileName">Name of the file</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>null if error or List of loaded items</returns>
        private static List<T> LoadFile<T>(string fileName, bool debug)
        {
            try
            {
                using (var textReader = File.OpenText(fileName))
                {
                    var csvReaderFileContents = new CsvReader(textReader);
                    csvReaderFileContents.Configuration.Delimiter = ",";
                    return csvReaderFileContents.GetRecords<T>().ToList();
                }
            }
            catch (Exception e)
            {
                if (!debug)
                    return null;

                Console.WriteLine("Invalid input file: " + fileName + "\n\nProblem: " + e.Message);
                Console.WriteLine("Check if the header line has any spaces...");
                return null;
            }
        }


        private static Dictionary<ulong, LimitsClass> GetGroupLimitsAfterSwap(IEnumerable<StudentsClass> studentsFile,
            IEnumerable<LimitsClass> limitsContent, bool debug)
        {

            // Create a copy of the list since the values are updated in the function
            var groupLimits = limitsContent.Select(row => row.Clone() as LimitsClass).ToList().ToDictionary(row => row.group_id, row => row);
            foreach (var student in studentsFile)
            {
                // Skip if the group is not changed
                if (student.new_group_id == 0 || student.new_group_id == student.group_id)
                    continue;

                // Check if the group (old or new one) exists
                if (!groupLimits.ContainsKey(student.group_id) ||
                    !groupLimits.ContainsKey(student.new_group_id))
                {
                    if (!debug)
                        return null;

                    Console.WriteLine("Student is in invalid group!\n" +
                                      "Student: " + student.student_id + " Old: " + student.group_id);
                    return null;
                }

                groupLimits[student.group_id].students_cnt--;
                groupLimits[student.new_group_id].students_cnt++;
            }

            return groupLimits;
        }

        /// <summary>
        /// Check the groups for the limit
        /// </summary>
        /// <param name="studentsContent"></param>
        /// <param name="limitsContent"></param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>true - all ok, false - error</returns>
        private static bool CheckForTheGroupLimits(IEnumerable<StudentsClass> studentsContent,
            List<LimitsClass> limitsContent, bool debug)
        {
            var groupLimits = GetGroupLimitsAfterSwap(studentsContent, limitsContent, debug);
            if (groupLimits == null)
                return false;

            // Check for the limits after the swap
            foreach (var group in groupLimits)
            {
                if (group.Value.students_cnt >= group.Value.min &&
                    group.Value.students_cnt <= group.Value.max) continue;

                if (!debug)
                    return false;

                Console.WriteLine("Number of students in group after the swap is invalid!\n" +
                                  "Group: " + group.Value.group_id + " Students: " + group.Value.students_cnt +
                                  " Min: " + group.Value.min + " Max: " + group.Value.max);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates the chartesian product far a student group dictionary
        /// </summary>
        /// <param name="studentGroups">List of groups for student (old or new)</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>Dictionary - all ok, null - error</returns>
        private static Dictionary<ulong, Dictionary<string, bool>> CreateChartesianProduct(
            Dictionary<ulong, List<ulong>> studentGroups,
            bool debug)
        {
            // Create allowed mapping table for the student in form:
            // chartesingProductDictionary[student_id] = [ [group1_group2, true], [group2_group1, true] , ... ]
            var chartesingProductDictionary = new Dictionary<ulong, Dictionary<string, bool>>();

            foreach (var student in studentGroups)
            {
                // Chartesian product of all allowed sequences
                foreach (var group1 in student.Value)
                {
                    foreach (var group2 in student.Value)
                    {
                        // Skip the same groups
                        if (group1 == group2)
                            continue;

                        // Items are stored in dictionary as:
                        var indexKey = group1 + "_" + group2;

                        // Add the item into the dictionary
                        if (!chartesingProductDictionary.ContainsKey(student.Key))
                        {
                            // Add pair for new student
                            chartesingProductDictionary.Add(student.Key,
                                new Dictionary<string, bool> {{indexKey, true}});
                            continue;
                        }
                        
                        if (!chartesingProductDictionary[student.Key].ContainsKey(indexKey))
                        {
                            // Add pair for existing student
                            chartesingProductDictionary[student.Key].Add(indexKey, true);
                            continue;
                        }

                        if (!debug)
                            return null;

                        Console.WriteLine("Group combination for this student already exists!\n" +
                                          "Student: " + student.Key + " group1: " + group1 + " group2: " +
                                          group2);
                        return null;
                    }
                }
            }

            return chartesingProductDictionary;
        }

        /// <summary>
        /// Checks if overlaps in output are valid
        /// </summary>
        /// <param name="studentsFile">Student file input</param>
        /// <param name="overlapsContent">Overlaps file input</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>true - all ok, false - error</returns>
        private static bool CheckForTheOverlaps(IEnumerable<StudentsClass> studentsFile,
            IEnumerable<OverlapsClass> overlapsContent, bool debug)
        {

            // Generate list of all accepted overlappings
            // Each student is allowed to be in the same groups that existed before the swap
            var studentOldGroups = new Dictionary<ulong, List<ulong>>();

            // List of all new groups for student
            var studentNewGroups = new Dictionary<ulong, List<ulong>>();

            // Generate list of groups for every student
            foreach (var student in studentsFile)
            {
                // Old groups
                if (!studentOldGroups.ContainsKey(student.student_id))
                    studentOldGroups.Add(student.student_id, new List<ulong> {student.group_id});
                else
                    studentOldGroups[student.student_id].Add(student.group_id);

                // New groups
                if (!studentNewGroups.ContainsKey(student.student_id))
                    studentNewGroups.Add(student.student_id, new List<ulong> {student.group_id});
                else
                    studentNewGroups[student.student_id].Add(student.group_id);
            }

            // Create chartesian product for all groups
            var oldGroups = CreateChartesianProduct(studentOldGroups, debug);
            var newGroups = CreateChartesianProduct(studentNewGroups, debug);

            // Check for errors
            if (oldGroups == null || newGroups == null)
                return false;

            // Create hash table for overlaps
            var hashOverlapsDictionary =
                overlapsContent.ToDictionary(overlaps => overlaps.group1_id + "_" + overlaps.group2_id,
                    overlaps => true);

            // Check overlapping for each student, each group pair
            foreach (var studentGroup in newGroups)
            {
                foreach (var groupPair in studentGroup.Value)
                {
                    // Check if the student in old group existed
                    if (!oldGroups.ContainsKey(studentGroup.Key))
                    {
                        if (debug)
                        {
                            Console.WriteLine("Invalid student in new group combination!\n" +
                                              "Student: " + studentGroup.Key);
                        }

                        return false;
                    }

                    if (!hashOverlapsDictionary.ContainsKey(groupPair.Key) ||
                        oldGroups[studentGroup.Key].ContainsKey(groupPair.Key)) continue;

                    // Student is in a group that is not allowed by overlaps and was not in that group combination before student swap
                    if (debug)
                    {
                        Console.WriteLine("Invalid new group combination!\n" +
                                          "Student: " + studentGroup.Key + " NewGroup1_NewGroup2: " + groupPair.Key);
                    }

                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if all swaps are valid - swap is valid if the student was in that group or if the swap was requested
        /// </summary>
        /// <param name="studentsFile">Student file input</param>
        /// <param name="requestsContent">Requests file input</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>true - all ok, false - error</returns>
        private static bool CheckForNotRequestedSwaps(IEnumerable<StudentsClass> studentsFile,
            IEnumerable<RequestsClass> requestsContent, bool debug)
        {
            // Create hash table for all requests
            var hashRequestsDictionary = new Dictionary<ulong, Dictionary<string, bool>>();
            foreach (var request in requestsContent)
            {
                var indexKey = request.activity_id + "_" + request.req_group_id;
                if (!hashRequestsDictionary.ContainsKey(request.student_id))
                {
                    hashRequestsDictionary.Add(request.student_id, new Dictionary<string, bool> {{indexKey, true}});
                }
                else
                {
                    hashRequestsDictionary[request.student_id].Add(indexKey, true);
                }
            }

            // Check all group changes
            foreach (var student in studentsFile)
            {
                // No change
                if (student.new_group_id == student.group_id || student.new_group_id == 0)
                    continue;

                // There is a request
                var indexKey = student.activity_id + "_" + student.new_group_id;
                if (hashRequestsDictionary.ContainsKey(student.student_id) &&
                    hashRequestsDictionary[student.student_id].ContainsKey(indexKey)) continue;

                if (debug)
                {
                    Console.WriteLine("Invalid student swap with no request!\n" +
                                      "Student: " + student.student_id + " Activity:" + student.activity_id +
                                      " Group: " + student.new_group_id);
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Check if all requests are having limits defined
        /// </summary>
        /// <param name="studentsContent">Student file input</param>
        /// <param name="limitsContent">Limits file input</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns></returns>
        private static bool CheckForLimitGrupsInRequests(IEnumerable<StudentsClass> studentsContent,
            IEnumerable<LimitsClass> limitsContent, bool debug)
        {

            // Generate hash table
            var limitsListDictionary = limitsContent.Select(row => row.group_id).Distinct().ToDictionary(row => row, row => true);
            foreach (var student in studentsContent)
            {
                if (!limitsListDictionary.ContainsKey(student.group_id))
                {
                    if (debug)
                    {
                        Console.WriteLine("Invalid student initial placement! Initial placement group has no limits defined!\n" +
                                          "Student: " + student.student_id + " Activity:" + student.activity_id +
                                          " Group: " + student.group_id);
                    }
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the student is in one group in each activity (no double groups)
        /// </summary>
        /// <param name="studentsFile">Student file input</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>true - all ok, false - error</returns>
        private static bool CheckForDoubleGroups(IEnumerable<StudentsClass> studentsFile, bool debug)
        {
            var checkDoubleEntries = studentsFile.GroupBy(row => row.student_id + "_" + row.activity_id)
                .Where(row => row.Count() > 1).ToList();
            if (checkDoubleEntries.Count == 0)
                return true;

            if (debug)
            {
                Console.WriteLine("Student listed more than once for same activity\nList: " + checkDoubleEntries.Count + " errors.");
                foreach (var entry in checkDoubleEntries)
                {
                    Console.WriteLine("   Student: " + entry.Key.Replace("_", " Activity: "));
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the student is in one group in each activity (no double groups)
        /// </summary>
        /// <param name="studentsFile">Student file input</param>
        /// <param name="debug">Display debug messages</param>
        /// <returns>true - all ok, false - error</returns>
        private static bool CheckForRequestsNotInInitialPlacement(IEnumerable<StudentsClass> studentsFile, 
            IEnumerable<RequestsClass> requestsFile, bool debug)
        {
            var studentsActivities = studentsFile.ToDictionary(row => row.student_id + "_" + row.activity_id, row => true);
            foreach(var request in requestsFile)
            {
                if (!studentsActivities.ContainsKey(request.student_id + "_" + request.activity_id))
                {
                    if (debug)
                    {
                        Console.WriteLine("Invalid student request. Activity in request is not listed in students.csv file\n" +
                            "Student: " + request.student_id + " activity: " + request.activity_id + " requested group: " + request.req_group_id);
                    }
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the student is in one group in each activity (no double groups)
        /// </summary>
        /// <param name="studentsContent">Student file input</param>
        /// <param name="limitsContent">Limits file input</param>
        /// <param name="awardActivity">Points awarded for number of activities completed for student</param>
        /// <param name="awardStudent">Points awarded for complete solution</param>
        /// <param name="minMaxPenalty">Points for below the wanted minumum and for abowe the wanted maximum</param>
        /// <param name="debug">Display debug messages</param>
        /// <param name="detailedCalculation">Detailed information about the values in calculation</param>
        /// <param name="groupSize">Display number of students in all groups after swap</param>
        /// <returns>0 - all ok</returns>
        private static long EvaluateSolution(IEnumerable<StudentsClass> studentsContent,
            List<LimitsClass> limitsContent, IEnumerable<RequestsClass> requestsContent, string awardActivity,
            long awardStudent, long minMaxPenalty, bool debug, bool detailedCalculation, bool groupSize)
        {
            long scoreA = 0;
            long scoreB = 0;
            long scoreC = 0;
            long scoreD = 0;
            long scoreE = 0;

            ulong lastExaminedId = 0;
            var swapedItemsForASingleStudent = 0;
            var totalItemsForASingleStudent = 0;
            long studentsWithAllSwaps = 0;

            // Make sure that the input file is ordered
            var studentsFileOrderd = studentsContent.OrderBy(row => row.student_id).ToList();

            // Awards for swap combo
            var awardActivityValues = awardActivity.Split(',').Select(row => long.Parse(row.Trim())).ToList();
            var maxAwardedActivityCombos = awardActivityValues.Count;

            if (detailedCalculation)
            {
                Console.WriteLine("\nDetailed scoring:");
            }

            // Calculate number of activities for each student for which the swap is requested
            var totalRequestedActivitiesForSwap = requestsContent.Select(row => new { row.student_id, row.activity_id })
                .Distinct()
                .GroupBy(row => row.student_id, (student_id, cnt) => new { sid = student_id, cnt = cnt.Count() })
                .ToDictionary(row => row.sid, row => row.cnt);

            // Add artifical last student at the end so that foreach gets the real last student
            studentsFileOrderd.Add(new StudentsClass{student_id = 0, activity_id = 0, new_group_id = 0, group_id = 0, swap_weight = 0 });
            // Check student solutions (scores A, B and C)
            foreach (var student in studentsFileOrderd)
            {
                // Next student
                if (student.student_id != lastExaminedId)
                {
                    // Swaps were made for previous student
                    if (swapedItemsForASingleStudent > 0)
                    {
                        // Take the combo value or greatest value available (eg. if combo values are 1,2,3 and there are 4 swaps made, then take the 3rd one)
                        var awardedScores = Math.Min(maxAwardedActivityCombos, swapedItemsForASingleStudent);
                        scoreB += awardActivityValues[awardedScores - 1];
                        if (detailedCalculation)
                        {
                            Console.WriteLine("  ScoreB += " + awardActivityValues[awardedScores - 1] +
                                              " (studentId: " + lastExaminedId + ")");
                        }

                        // All swaps were made for previous student 
                        // Note: all swaps are calculated as the number of different activities for which the request was made, not
                        //       as a total number of activities the student is enrolled into 
                        if (swapedItemsForASingleStudent == totalRequestedActivitiesForSwap[lastExaminedId])
                        {
                            studentsWithAllSwaps++;
                            if (detailedCalculation)
                            {
                                Console.WriteLine("  ScoreC (allswaps) += " + awardStudent + " (studentId: " +
                                                  lastExaminedId + ")");
                            }

                        }
                    }

                    totalItemsForASingleStudent = 0;
                    swapedItemsForASingleStudent = 0;
                    lastExaminedId = student.student_id;
                }

                totalItemsForASingleStudent++;

                if (student.new_group_id == 0 || student.new_group_id == student.group_id) continue;

                // Swap was made - award it
                scoreA += (long) student.swap_weight;
                if (detailedCalculation)
                {
                    Console.WriteLine("  ScoreA += " + student.swap_weight + " (studentId: " +
                                      student.student_id + " Swap " + student.group_id + "->" + student.new_group_id +
                                      ")");
                }
                swapedItemsForASingleStudent++;

            }

            scoreC += awardStudent * studentsWithAllSwaps;

            // Check group limits
            var groupLimits = GetGroupLimitsAfterSwap(studentsFileOrderd, limitsContent, debug);
            foreach (var group in groupLimits)
            {
                if (group.Value.students_cnt < group.Value.min_preferred)
                {
                    var val = ((long) group.Value.min_preferred - (long) group.Value.students_cnt) * minMaxPenalty;
                    scoreD += val;
                    if (detailedCalculation)
                    {
                        Console.WriteLine("  ScoreD += " + val + " (groupId: " +
                                          group.Value.group_id + " minPref: " + group.Value.min_preferred + " stud: " +
                                          group.Value.students_cnt + ")");
                    }
                }

                if (group.Value.students_cnt > group.Value.max_preferred)
                {
                    var val = ((long)group.Value.students_cnt - (long)group.Value.max_preferred) * minMaxPenalty;
                    scoreE += val;
                    if (detailedCalculation)
                    {
                        Console.WriteLine("  ScoreE += " +
                                          val + " (groupId: " +
                                          group.Value.group_id + " maxPref: " + group.Value.max_preferred + " stud: " +
                                          group.Value.students_cnt + ")");
                    }
                }
            }

            if (groupSize)
            {
                Console.WriteLine("\nGroup sizes:");
                foreach (var group in groupLimits)
                {
                    Console.WriteLine("  Group: " + group.Value.group_id + " cnt: " + group.Value.students_cnt);
                }
                Console.WriteLine("");
            }

            if (debug)
            {
                Console.Write("\nSolution values:\n" +
                              "ScoreA: " + scoreA + "\n" +
                              "ScoreB: " + scoreB + "\n" +
                              "ScoreC: " + scoreC + "\n" +
                              "ScoreD: " + scoreD + "\n" +
                              "ScoreE: " + scoreE + "\n" +
                              "\n" +
                              "Total = ScoreA + Score B + ScoreC - ScoreD - ScoreE =\n" + 
                              "Total = " + scoreA + " + " + scoreB + " + " + scoreC + " - " + scoreD + " - " + scoreE + "\n\n"
                );
            }

            return scoreA + scoreB + scoreC - scoreD - scoreE;
        }


        /// <summary>
        /// Checks if the student is in one group in each activity (no double groups)
        /// </summary>
        /// <param name="studentsContent">Student file input</param>
        /// <param name="awardActivity">Points awarded for number of activities completed for student</param>
        /// <param name="awardStudent">Points awarded for complete solution</param>
        /// <returns>0 - all ok</returns>
        private static long CalculateMaxSolutionValue(IEnumerable<StudentsClass> studentsContent, string awardActivity,
            long awardStudent)
        {
            long scoresA = 0;
            long scoresB = 0;
            long scoresC = 0;
            long scoresD = 0;
            long scoresE = 0;

            ulong lastExaminedId = 0;
            var swapedItemsForASingleStudent = 0;
            long studentsWithAllSwaps = 0;

            // Make sure that the input file is ordered
            var studentsFileOrderd = studentsContent.OrderBy(row => row.student_id).ToList();

            // Awards for swap combo
            var awardActivityValues = awardActivity.Split(',').Select(row => long.Parse(row.Trim())).ToList();
            var maxAwardedActivityCombos = awardActivityValues.Count;

            // Check student solutions (scores A, B and C)
            foreach (var student in studentsFileOrderd)
            {
                // Next student
                if (student.student_id != lastExaminedId)
                {
                    // Swaps were made for previous student
                    if (swapedItemsForASingleStudent > 0)
                    {
                        // Take the combo value or greatest value available (eg. if combo values are 1,2,3 and there are 4 swaps made, then take the 3rd one)
                        var awardedScores = Math.Min(maxAwardedActivityCombos, swapedItemsForASingleStudent);
                        scoresB += awardActivityValues[awardedScores - 1];

                        // All swaps were made for previous student
                        studentsWithAllSwaps++;
                    }

                    swapedItemsForASingleStudent = 0;
                    lastExaminedId = student.student_id;
                }

                // Swap was made - award it
                scoresA += (long) student.swap_weight;
                swapedItemsForASingleStudent++;
            }

            scoresC += awardStudent * studentsWithAllSwaps;

            return scoresA + scoresB + scoresC - scoresD - scoresE;
        }

    }
}
