This project is Java solution for problem defined in: Upute_zamjena_grupa.pdf.
Samples for the problem are in samples directory.
C# project for validation of the solution is in validator directory.

In short:

There is a problem of group replacements in some college. Students create requests for group replacements and those requests are heaped in the bundle.
There it has to be decided which requests has to be granted and which has to be dissregarded. Program gets its arguments from command line:

<program> –timeout 600 –award-activity "1,2,4" –award-student 1 –minmax-penalty 1
–students-file student.csv –requests-file requests.csv –overlaps-file overlaps.csv –limits-file limits.csv

Timeout is time in second which this program is allowed has to solve the problem.
–award-activity, –award-student, –minmax-penalty are used to determine the solution value: how good is the solution.
–students-file, –requests-file, –overlaps-file, –limits-file are paths to the files from which problem instance is defined.
All of the details of the problem are defined in: Upute_zamjena_grupa.pdf.