package hr.fer.diplomskirad.zamjenagrupa.data;

public interface GoodnessProvider {
	int getGoodness();
}
