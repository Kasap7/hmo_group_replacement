package hr.fer.diplomskirad.zamjenagrupa.data;

import java.util.HashMap;
import java.util.Map;

public class Constants {
	
	public static final String DIR = "samples";
	
	public static final String PRAVI1 = "Pravi1";
	public static final String PRAVI2 = "Pravi2";
	public static final String SAMPLE3 = "Sample3";
	public static final String SAMPLE4 = "Sample4";
	public static final String SAMPLE5 = "Sample5";
	public static final String SAMPLE6 = "Sample6";
	public static final String SAMPLE7 = "Sample7";
	
	public static final String PRAVI1_FULL;
	public static final String PRAVI2_FULL;
	public static final String SAMPLE3_FULL;
	public static final String SAMPLE4_FULL;
	public static final String SAMPLE5_FULL;
	public static final String SAMPLE6_FULL;
	public static final String SAMPLE7_FULL;
	
	public static final Map<String, String> SAMPLE_MAP;
	
	public static final long TIME1m  = 60*1000;
	public static final long TIME2m  = 2*TIME1m;
	public static final long TIME5m  = 5*TIME1m;
	public static final long TIME10m = 10*TIME1m;
	public static final long TIME15m = 15*TIME1m;
	public static final long TIME30m = 30*TIME1m;
	public static final long TIME60m = 60*TIME1m;
	public static final long TIME90m = 90*TIME1m;
	
	public static final int MIN_MAX_PENALTY = 1;
	public static final int AWARD_STUDENT = 1;
	public static final int[] AWARD_ACTIVITY = {1, 2, 4};
	
	static {
		SAMPLE_MAP = new HashMap<>();
		
		SAMPLE_MAP.put(PRAVI1, getFullPath(PRAVI1));
		SAMPLE_MAP.put(PRAVI2, getFullPath(PRAVI2));
		SAMPLE_MAP.put(SAMPLE3, getFullPath(SAMPLE3));
		SAMPLE_MAP.put(SAMPLE4, getFullPath(SAMPLE4));
		SAMPLE_MAP.put(SAMPLE5, getFullPath(SAMPLE5));
		SAMPLE_MAP.put(SAMPLE6, getFullPath(SAMPLE6));
		SAMPLE_MAP.put(SAMPLE7, getFullPath(SAMPLE7));
		
		PRAVI1_FULL = SAMPLE_MAP.get(PRAVI1);
		PRAVI2_FULL = SAMPLE_MAP.get(PRAVI2);
		SAMPLE3_FULL = SAMPLE_MAP.get(SAMPLE3);
		SAMPLE4_FULL = SAMPLE_MAP.get(SAMPLE4);
		SAMPLE5_FULL = SAMPLE_MAP.get(SAMPLE5);
		SAMPLE6_FULL = SAMPLE_MAP.get(SAMPLE6);
		SAMPLE7_FULL = SAMPLE_MAP.get(SAMPLE7);
	}


	private static String getFullPath(String sample) {
		return DIR + "/" + sample;
	}

}
