package hr.fer.diplomskirad.zamjenagrupa.data;

/**
 * This class represents one line from the -limits-file. Each limit is represented by:
 * <pre>
 * group_id: id of the group
 * students_cnt: number of students that are currently in this group
 * min: hard constraint for minimum students allowed for this group
 * max: hard constraint for maximum students allowed for this group
 * min-preferred: soft constraint for minimum students allowed for this group
 * max-preferred: soft constraint for maximum students allowed for this group
 * </pre>
 * @author Josip Kasap
 *
 */
public class GroupLimit {
	public final long group_id;
	public int students_cnt;
	public final int min, min_preferred, max, max_preferred;
	
	public GroupLimit(String[] arr) {
		super();
		group_id = Long.parseLong(arr[0]);
		students_cnt = Integer.parseInt(arr[1]);
		min = Integer.parseInt(arr[2]);
		min_preferred = Integer.parseInt(arr[3]);
		max = Integer.parseInt(arr[4]);
		max_preferred = Integer.parseInt(arr[5]);
	}
	
	public GroupLimit(GroupLimit other) {
		super();
		group_id = other.group_id;
		students_cnt = other.students_cnt;
		min = other.min;
		min_preferred = other.min_preferred;
		max = other.max;
		max_preferred = other.max_preferred;
	}
	
	/**
	 * Checks if this group is out of limits
	 * @return <code>true</code> if this group is out of limits, <code>false</code> otherwise
	 */
	public boolean isOutOfLimits() {
		return (students_cnt > max || students_cnt < min) ? true : false;
	}
	
	public boolean isOverMax() {
		return students_cnt > max;
	}
	
	public boolean isUnderMin() {
		return students_cnt < min;
	}
	
	/**
	 * Checks if this group has maximum or more number of students
	 * @return <code>true</code> if this group has maximum or more number of students, <code>false</code> otherwise
	 */
	public boolean isMax() {
		return (students_cnt >= max) ? true : false;
	}
	
	/**
	 * Checks if this group has minimum or less number of students
	 * @return <code>true</code> if this group has minimum or less number of students, <code>false</code> otherwise
	 */
	public boolean isMin() {
		return (students_cnt <= min) ? true : false;
	}
	
	/**
	 * Checks if this group has maximum-preferred or more number of students
	 * @return <code>true</code> if this group has maximum-preferred or more number of students, <code>false</code> otherwise
	 */
	public boolean isMaxPref() {
		return (students_cnt >= max_preferred) ? true : false;
	}
	
	/**
	 * Checks if this group has minimum-preferred or less number of students
	 * @return <code>true</code> if this group has minimum-preferred or less number of students, <code>false</code> otherwise
	 */
	public boolean isMinPref() {
		return (students_cnt <= min_preferred) ? true : false;
	}
	
	/**
	 * Clones this object and returns cloned object
	 * @return clone of this object
	 */
	public GroupLimit copy() {
		return new GroupLimit(this);
	}
	
	/**
	 * Increases number of students in this group by 1 and also returns how much the penalty points 
	 * have changed due to this student count change.
	 * @return the penalty points have changed due to this student count change
	 */
	public int increaseValue() {
		int value = 0;
		if (students_cnt < min_preferred) {
			value += -1;
		}
		if (students_cnt + 1 > max_preferred) {
			value +=  1;
		}
		
		students_cnt++;
		return value;
	}
	
	/**
	 * Decreases number of students in this group by 1 and also returns how much the penalty points 
	 * have changed due to this student count change.
	 * @return the penalty points have changed due to this student count change
	 */
	public int decreaseValue() {
		int value = 0;
		if (students_cnt - 1 < min_preferred) {
			value +=  1;
		}
		if (students_cnt > max_preferred) {
			value += -1;
		}
		
		students_cnt--;
		return value;
	}
	
	public void increaseValue(LimitsBoundsChecker checker, int minmax_penalty) {
		if (students_cnt < min_preferred) {
			checker.updateD(-minmax_penalty);
		}
		if (students_cnt + 1 > max_preferred) {
			checker.updateE(minmax_penalty);
		}
		
		students_cnt++;
	}
	
	public void decreaseValue(LimitsBoundsChecker checker, int minmax_penalty) {
		if (students_cnt - 1 < min_preferred) {
			checker.updateD(minmax_penalty);
		}
		if (students_cnt > max_preferred) {
			checker.updateE(-minmax_penalty);
		}
		
		students_cnt--;
	}
	
	/**
	 * Calculates the D + E penalty points for this group for the minmax-penalty parameter.
	 * @param minmax_penalty penalty for each student that is out of preferred scope
	 * @return D + E penalty points for this group for the minmax-penalty parameter
	 */
	public int getDE(int minmax_penalty) {
		int value = 0;
		if (students_cnt > max_preferred) {
			value += (students_cnt - max_preferred)*minmax_penalty;
		}
		if (students_cnt < min_preferred) {
			value += (min_preferred - students_cnt)*minmax_penalty;
		}
		return value;
	}
	
	/**
	 * Calculates penalty for exceeding the lower bound for preferred students count.
	 * @param minmax_penalty penalty for each student that is out of preferred scope
	 * @return D penalty points for this group for the minmax-penalty parameter
	 */
	public int getD(int minmax_penalty) {
		if (students_cnt < min_preferred) {
			return  (min_preferred - students_cnt)*(minmax_penalty);
		}
		return 0;
	}
	
	/**
	 * Calculates penalty for exceeding the upper bound for preferred students count.
	 * @param minmax_penalty penalty for each student that is out of preferred scope
	 * @return E penalty points for this group for the minmax-penalty parameter
	 */
	public int getE(int minmax_penalty) {
		if (students_cnt > max_preferred) {
			return  (students_cnt - max_preferred)*(minmax_penalty);
		}
		return 0;
	}
	
	public String getOutOfLimitsMessage() {
		if (students_cnt > max) {
			return String.format("group_id = %d; students_cnt = %d; max = %d", group_id, students_cnt, max);
		} else if (students_cnt < min) {
			return String.format("group_id = %d; students_cnt = %d; min = %d", group_id, students_cnt, min);
		}
		return "";
	}

	@Override
	public String toString() {
		return String.format("(%d,%d,%d,%d,%d,%d)", group_id, students_cnt, min, min_preferred, max, max_preferred);
	}
}
