package hr.fer.diplomskirad.zamjenagrupa.data;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.Algorithms;


/**
 * This class represents the state of the system. It holds all necessary information for this problem.
 * 
 * @author Josip Kasap
 *
 */
public class Data {
	
	public long timeout, currentTime, startTime;
	public int[] award_activity;
	public int award_student;
	public int minmax_penalty;
	public int initialGoodness;
	public String studentsFile;
	
	public int studentsCount, activityCount, requestsCount;
	
	/** Set of all groups */
	public Set<Long> groups = new HashSet<>();
	
	/** Maps student -> all groups that student attends; so that the groups student attends can't be overlapped */
	public Map<Long, Set<Long>> studentGroups = new HashMap<>();
	
	/** Limits the students in the group for each group */
	public Map<Long, GroupLimit> groupLimits = new HashMap<>();
	
	/** Maps group -> set of group; for each group, set of group that group overlaps with */
	public Map<Long, Set<Long>> overlapMap = new HashMap<>();
	
	/** Maps student & activity -> list of all requests */
	public Map<Long, Map<Long, List<Request>>> requestsMap = new HashMap<>();
	
	/** Maps group -> all requests that this group is source */
	public Map<Long, List<Request>> inputGroupRequests = new HashMap<>();
	
	/** Maps group -> all requests that want to go to this group */
	public Map<Long, List<Request>> outputGroupRequests = new HashMap<>();
	
	/** Maps group -> all requests that want to go to this group */
	public Map<Long, Long> groupToActivityMap = new HashMap<>();
	
	/** List of all requests */
	public List<Request> requestList = new ArrayList<>();
	
	/** List of all initially granted requests */
	public List<Request> initiallyGrantedRequests = new ArrayList<>();
    
	
	/**
	 * This constructor creates a Data that holds all of the important features of the system.
	 * @param students
	 * @param requests
	 * @param overlaps
	 * @param limits
	 * @param timeout
	 * @param startTime
	 * @param award_activity
	 * @param award_student
	 * @param minmax_penalty
	 * @throws IOException
	 */
	public Data(String students, String requests_, String overlaps, String limits, long timeout, long startTime,
			int[] award_activity, int award_student, int minmax_penalty) throws IOException {
		super();

		this.timeout = timeout;
		this.currentTime = startTime;
		this.startTime = startTime;
		this.award_activity = award_activity;
		this.award_student = award_student;
		this.minmax_penalty = minmax_penalty;
		this.studentsFile = students;
		
		List<String> studentsLines = Files.readAllLines(Paths.get(students));
		List<String> requestsLines = Files.readAllLines(Paths.get(requests_));
		List<String> overlapsLines = Files.readAllLines(Paths.get(overlaps));
		List<String> limitsLines = Files.readAllLines(Paths.get(limits));
		
		// Prijelazna mapa za request od studenata
		Map<Long, Map<Long, StudentRecord>> studentMap = new HashMap<>();
		
		Set<Long> studentsSet = new HashSet<>();
		Set<Long> activitySet = new HashSet<>();
		
		// Ucitavanje svih grupa i njihovih limita 
		for (int i=1; i<limitsLines.size(); i++) {
			String line = limitsLines.get(i);
			String[] arr = line.split(",");
			long group_id = Long.parseLong(arr[0]);
			
			groupLimits.put(group_id, new GroupLimit(arr));
		}
		
		// Ucitavanje students mape (inicijalno stanje studenata i njihovih grupa)
		for (int i=1; i<studentsLines.size(); i++) {
			String line = studentsLines.get(i);
			String[] arr = line.split(",");
			long student_id = Long.parseLong(arr[0]);
			long activity_id = Long.parseLong(arr[1]);
			//long swap_weight = Long.parseLong(arr[2]);
			long group_id = Long.parseLong(arr[3]);
			
			if (!groupLimits.containsKey(group_id))
				continue;
			
			Set<Long> studentSet = studentGroups.get(student_id);
			if (studentSet == null) {
				studentSet = new HashSet<>();
				studentGroups.put(student_id, studentSet);
			}
			studentSet.add(group_id);
			
			groups.add(group_id);
			
			Map<Long, StudentRecord> recordForActivity = studentMap.get(student_id);
			if (recordForActivity == null) {
				recordForActivity = new HashMap<>();
				studentMap.put(student_id, recordForActivity);
			}
			
			recordForActivity.put(activity_id, new StudentRecord(arr));
			
			if (!arr[4].equals("0")) {
				long wanted_group_id = Long.parseLong(arr[4]);
				long swap_weight = Long.parseLong(arr[2]);
				Request request = new Request(student_id, activity_id, group_id, swap_weight, wanted_group_id);
				initiallyGrantedRequests.add(request);
			}
			
			groupToActivityMap.put(group_id, activity_id);
			
			// This is strictly for statistical purposes
			studentsSet.add(student_id);
			activitySet.add(activity_id);
		}
		
		for (int i=1; i<requestsLines.size(); i++) {
			String line = requestsLines.get(i);
			String[] arr = line.split(",");
			long student_id = Long.parseLong(arr[0]);
			long activity_id = Long.parseLong(arr[1]);
			long new_group_id = Long.parseLong(arr[2]);
			
			if (!groupLimits.containsKey(new_group_id) || studentMap.get(student_id) == null ||
					studentMap.get(student_id).get(activity_id) == null)
				continue;
			
			groups.add(new_group_id);
			
			StudentRecord studentRecord = studentMap.get(student_id).get(activity_id);
			if (studentRecord == null)
				continue;
			
			Map<Long, List<Request>> requestsForActivity = requestsMap.get(student_id);
			if (requestsForActivity == null) {
				requestsForActivity = new HashMap<>();
				requestsMap.put(student_id, requestsForActivity);
			}
			//requestsForActivity.put(activity_id, new ArrayList<>()); STARO
			if (requestsForActivity.get(activity_id) == null) { 			// NOVO
				requestsForActivity.put(activity_id, new ArrayList<>());	// NOVO
			}																// NOVO
			
			List<Request> requests = requestsMap.get(student_id).get(activity_id);
			Request request = new Request(studentRecord, new_group_id);
			
			requests.add(request);
			requestList.add(request);
			
			// Added new functionality, all of the requests are sorted by their initial group and their wanted group
			List<Request> outgoingRequests = inputGroupRequests.get(studentRecord.group_id);
			if (outgoingRequests == null) {
				outgoingRequests = new ArrayList<>();
				inputGroupRequests.put(studentRecord.group_id, outgoingRequests);
			}
			outgoingRequests.add(request);
			
			List<Request> ingoingRequests = outputGroupRequests.get(new_group_id);
			if (ingoingRequests == null) {
				ingoingRequests = new ArrayList<>();
				outputGroupRequests.put(new_group_id, ingoingRequests);
			}
			ingoingRequests.add(request);
			
			activitySet.add(activity_id);
			
			groupToActivityMap.put(new_group_id, activity_id);
		}
		
		for (int i=1; i<overlapsLines.size(); i++) {
			String line = overlapsLines.get(i);
			String[] arr = line.split(",");
			long group_id1 = Long.parseLong(arr[0]);
			long group_id2 = Long.parseLong(arr[1]);
			
			if (! (groups.contains(group_id1) && groups.contains(group_id2))) {
				continue;
			}
			
			Set<Long> overlappedGroups1 = overlapMap.get(group_id1);
			if (overlappedGroups1 == null) {
				overlappedGroups1 = new HashSet<>();
				overlapMap.put(group_id1, overlappedGroups1);
			}
			overlappedGroups1.add(group_id2);
			
			Set<Long> overlappedGroups2 = overlapMap.get(group_id2);
			if (overlappedGroups2 == null) {
				overlappedGroups2 = new HashSet<>();
				overlapMap.put(group_id2, overlappedGroups2);
			}
			overlappedGroups2.add(group_id1);
		}
		
		for (long group_id : groups) {
			if (overlapMap.containsKey(group_id) == false) {
				overlapMap.put(group_id, new HashSet<>());
			}
		}
		
		requestList.sort(new Comparator<Request>() {
			@Override
			public int compare(Request r1, Request r2) {
				return -r1.compareTo(r2);
			}
		});
		
		requestsCount = requestList.size();
		studentsCount = studentsSet.size();
		activityCount = activitySet.size();
		
		Chromosome c = new Chromosome();
		initialGoodness = c.goodness;
	}
	
	
	/**
	 * This method optimizes requests by deleting all of the impossible requests. This is the list of all
	 * impossible requests:
	 * <br>
	 * 1. Student has a request for a full group, but nobody wants to get out of that group.
	 * <br>
	 * 2. Student has a request from a group with minimal student count, but there is no request to get into that group.
	 * <br>
	 * 3. Student has a request that would create an overlap, but student does not have any other request that would resolve
	 * 		that overlap.
	 */
	public void optimize() {
		System.out.println();
		System.out.printf(" Number of students   = %d\n Number of activities = %d\n Number of requests   = %d\n", 
				studentsCount, activityCount, requestsCount);
		System.out.printf(" Number of groups     = %d\n", groups.size());
		System.out.printf(" Avarage number of activities per student = %.3f\n", (1.0*activityCount) / studentsCount);
		System.out.printf(" Avarage number of requests per student = %.3f\n", (1.0*requestsCount) / studentsCount);
		System.out.println();
		
		//##############################################################################
									/* IMPOSIBLE REQUESTS */
		//##############################################################################
		List<Request> imposibleRequests1 = new ArrayList<>();
		List<Request> imposibleRequests2 = new ArrayList<>();
		List<Request> imposibleRequests3 = new ArrayList<>();
		for (Request r : requestList) {
			
			// 1) Requests for full groups
			if (groupLimits.get(r.wanted_group_id).isMax()) {
				List<Request> outgroupRequests = inputGroupRequests.get(r.wanted_group_id);
				if (outgroupRequests == null || outgroupRequests.isEmpty()) {
					imposibleRequests1.add(r);
					//System.out.printf("  1.) %s\n", r);
				}
			
			// 2) Requests from groups with minimal student count
			} else if (groupLimits.get(r.group_id).isMin()) {
				//System.out.printf("  #2.) %s\n", r);
				List<Request> ingroupRequests = outputGroupRequests.get(r.group_id);
				if (ingroupRequests == null || ingroupRequests.isEmpty()) {
					imposibleRequests2.add(r);
					//System.out.printf("  2.) %s\n", r);
				}
			} 
			
			// 3) Requests that create unsolvable conflicts (overlaps)
			Set<Long> studentGroupsChanged = new HashSet<>(studentGroups.get(r.student_id));
			List<Request> requestList = new ArrayList<>();
			requestList.add(r);
			List<List<Request>> requestListList = new ArrayList<>();
			requestListList.add(requestList);
			
			Set<Request> impossibleRequests = new HashSet<>(imposibleRequests1);
			impossibleRequests.addAll(imposibleRequests2);
			
			
			//System.out.println(r);
			//if (r.toString().equals("(17596,3542314,1,168826,168825)")) {
			//	System.out.println("ERROR happened here!");
			//}
			if (canRequestsBeAccepted(requestListList, studentGroupsChanged, new HashSet<>(), impossibleRequests) == false) {
				imposibleRequests3.add(r);
			}
		}
		
		int impossible1 = imposibleRequests1.size();
		int impossible2 = imposibleRequests2.size();
		int impossible3 = imposibleRequests3.size();
		
		System.out.println(" All of the impossible requests have been found.");
		System.out.printf(" Total number of impossible requests = %d\n", impossible1 + impossible2 + impossible3);
		System.out.printf("  1.) Requests for full groups = %d\n", impossible1);
		System.out.printf("  2.) Requests from groups with minimal student count = %d\n", impossible2);
		System.out.printf("  3.) Requests that create unsolvable conflicts (overlaps) = %d\n", impossible3);
		
		for (Request r : imposibleRequests1)
			requestList.remove(r);
		for (Request r : imposibleRequests2)
			requestList.remove(r);
		for (Request r : imposibleRequests3) {
			requestList.remove(r);
			//System.out.println(r);
		}
		
		System.out.printf(" Total number of leftover possible requests = %d\n", requestList.size());
		System.out.println();
	}
	
	/**
	 * This method checks if given requests can be accepted.
	 * @param impossibleRequests 
	 * @param request activityRequestMap Maps activity to list of requests that is to be checked if can be accepted
	 * @return <code>true</code> if requests can be accepted, <code>false</code> otherwise
	 */
	private boolean canRequestsBeAccepted(List<List<Request>> requestListList, Set<Long> studentGroupsChanged, 
			Set<Long> resolvedActivities, Set<Request> impossibleRequests) {
		if (requestListList.isEmpty()) 
			return false;
		
		long student_id = requestListList.get(0).get(0).student_id;
		boolean isAcceptable = false;
		boolean toBreak = false;
		int[] permutation = new int[requestListList.size()];
		for (int i=0; i<permutation.length; i++) {
			permutation[i] = 0;
		}
		
		while (true) {
			toBreak = true;
			isAcceptable = true;
			
			for (int i=0; i<permutation.length; i++) {
				
				if (requestListList.get(i) == null) System.out.println(requestListList);
				
				if (permutation[i] != requestListList.get(i).size()-1) {
					toBreak = false;
					break;
				}
			}
			
			// Virtually grant requests
			for (int i=0; i<permutation.length; i++) {
				Request request = requestListList.get(i).get(permutation[i]);
				
				if (!studentGroupsChanged.contains(request.group_id) || resolvedActivities.contains(request.activity_id) 
						|| impossibleRequests.contains(request)) {
					isAcceptable = false;
				}
				
				studentGroupsChanged.remove(request.group_id);
				studentGroupsChanged.add(request.wanted_group_id);
				resolvedActivities.add(request.activity_id);
			}
			
			if (isAcceptable) {
				Set<Long> ovelappedActivities = new HashSet<>();
				for (int i=0; i<permutation.length; i++) {
					Request request = requestListList.get(i).get(permutation[i]);
					
					Set<Long> overlapsWithWantedGroup = overlapMap.get(request.wanted_group_id);
					
					Set<Long> ovelappedGroups = Algorithms.intersection(studentGroupsChanged, overlapsWithWantedGroup);
					ovelappedGroups.remove(request.group_id);
					
					for (long group_id : ovelappedGroups) {
						long overlappedActivity_id = groupToActivityMap.get(group_id);
						ovelappedActivities.add(overlappedActivity_id);
					}
				}
				
				if (!ovelappedActivities.isEmpty()) {
					isAcceptable = false;
					boolean completelyUnAcceptable = false;
					List<List<Request>> requestListListNew = new ArrayList<>();
					for (long activity_id : ovelappedActivities) {
						if (requestsMap.get(student_id) == null || requestsMap.get(student_id).get(activity_id) == null
								|| resolvedActivities.contains(activity_id)) {
							completelyUnAcceptable = true;
							break;
						}
						requestListListNew.add(requestsMap.get(student_id).get(activity_id));
					}
					
					if (!completelyUnAcceptable)
						isAcceptable = canRequestsBeAccepted(requestListListNew, studentGroupsChanged, resolvedActivities, impossibleRequests);
				}
			}
			
			// Virtually rollback requests
			for (int i=0; i<permutation.length; i++) {
				Request request = requestListList.get(i).get(permutation[i]);
				studentGroupsChanged.remove(request.wanted_group_id);
				studentGroupsChanged.add(request.group_id);
				resolvedActivities.remove(request.activity_id);
			}
			
			if (toBreak || isAcceptable)
				break;
			
			for (int i = 0, overflow = 1; i < permutation.length && overflow > 0; i++) {
				permutation[i] += overflow;
				overflow = permutation[i] / requestListList.get(i).size();
				permutation[i] = permutation[i] % requestListList.get(i).size();
			}
		}
		
		return isAcceptable;
	}
	
	/**
	 * This method returns String output of the initial solution. If the initial solution is valid, it returns
	 * the goodness of solution with details. If the initial solution is invalid it returns detailed explanations
	 * why the solution is not valid.
	 * If solution is valid a new file is created and for each request it is explained why it is granted or not not granted
	 * @param dataValidationFile File to write an output of data validation
	 * @return String output of the initial solution that is either valid (goodness of solution) or invalid 
	 * 		(details for invalidness)
	 */
	public String validateSolution(String dataValidationFile) {
		Chromosome chrome = new Chromosome();
		boolean isValid = true;
		StringBuilder sb = new StringBuilder();
		int a = 0, b = 0, c = 0, d = 0, e = 0, goodness = 0;
		
		for (Request r : initiallyGrantedRequests) {
			boolean accepted = chrome.grantRequestForce(r);
			if (!accepted) {
				isValid = false;
				sb.append(String.format("Invalid request: %s, does not exist or could've been granted before\n", r));
			}
		}
		
		if (!isValid)
			return sb.toString();
		
		for (GroupLimit limit : chrome.groupLimitsC.values()) {
			if (limit.isOutOfLimits()) {
				if (isValid) {
					sb.append("Solution is not valid!\n");
				}
				sb.append(limit.getOutOfLimitsMessage() + "\n");
				isValid = false;
			}
			
			d += limit.getD(minmax_penalty);
			e += limit.getE(minmax_penalty);
		}
		
		for (long student_id : chrome.studentGroupsC.keySet()) {
			Set<Long> groups = chrome.studentGroupsC.get(student_id);
			Set<Long> initialGroups = studentGroups.get(student_id);
			for (long group_id : groups) {
				if (initialGroups.contains(group_id)) {
					continue;
				}
				
				Set<Long> overlapSet = overlapMap.get(group_id);
				Set<Long> overlapsWithGroups = Algorithms.intersection(overlapSet, groups);
				
				if (overlapsWithGroups.size() > 0) {
					if (isValid) {
						sb.append("Solution is not valid!\n");
					}
					for (long overlaped_group_id : groups) {
						sb.append(String.format("Overlap for student: %d, between groups: %d <-> %d\n", student_id, group_id, overlaped_group_id));
					}
					isValid = false;
				}
			}
		}
		
		if (!isValid)
			return sb.toString();
		
		for (long student_id : chrome.approvedRequests.keySet()) {
			Map<Long, Request> approvedRequestsForStudent = chrome.approvedRequests.get(student_id);
			for (long activity_id : approvedRequestsForStudent.keySet()) {
				Request request = approvedRequestsForStudent.get(activity_id);
				
				a += request.swap_weight;
			}
			
			int requestsGrantedForStudent = approvedRequestsForStudent.size();
			int requestsAskedForStudent = requestsMap.get(student_id).size();
			int index = requestsGrantedForStudent - 1;
			
			if (index >= 0 && index <= award_activity.length - 1) {
				b += award_activity[index];
			} else if (index > award_activity.length - 1) {
				b += award_activity[award_activity.length - 1];
			}
			
			if (requestsAskedForStudent == requestsGrantedForStudent) {
				c += award_student;
			}
		}
		
		goodness = a + b + c - d - e;
		
		List<List<Integer>> printList = new ArrayList<>();
		
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(dataValidationFile))) {
			for (long student_id : requestsMap.keySet()) {
				writer.write("student_id: " + student_id + "\n");
				
				List<Integer> requestsForActivity = new ArrayList<>();
				printList.add(requestsForActivity);
				
				for (long activity_id : requestsMap.get(student_id).keySet()) {
					writer.write("+  acitvity_id: " + activity_id + "\n");
					
					requestsForActivity.add(requestsMap.get(student_id).get(activity_id).size());
					
					for (Request r : requestsMap.get(student_id).get(activity_id)) {
						writer.write("++    " + r + ": " + chrome.getGrantRequestMessage(r) + "\n");
					}
				}
				writer.write("\n");
			}
			
			writer.write("\n\n############################### LIMITS CHANGED #####################################\n\n");
			writer.write(String.format("group_id,students_cnt,min,min_preferred,max,max_preferred\n"));
			
			Set<Long> groups = new TreeSet<>(chrome.groupLimitsC.keySet());
			for (long group_id : groups) {
				GroupLimit limit = chrome.groupLimitsC.get(group_id);
				writer.write(String.format("%d,%d,%d,%d,%d,%d\n", limit.group_id, limit.students_cnt, limit.min, limit.min_preferred,
						limit.max, limit.max_preferred));
			}
			
			writer.close();
		} catch (IOException e1) {
			
		} finally {
			
		}
		
		System.out.println();
		return String.format("Goodness = %d;\t %d = %d + %d + %d - %d - %d\n\n", 
				goodness - initialGoodness, goodness, a, b, c, d, e);
	}
	
	/**
	 * Makes greedy search for this problem by checking all of the requests in random order and trying to add
	 * each request until no more requests are able to be added.
	 * @return solution for the problem with greedy search
	 */
	public Chromosome greedySearch() {
		Chromosome current = new Chromosome();
		current.f();
		int goodness = current.goodness;
		int prevGoodness = goodness;
		int sum = 0;
		System.out.printf("Starting goodness = %d\n", goodness);
		
		while (currentTime < timeout) {
			int reqDone = 0;
			
			Collections.shuffle(requestList);
			for (Request request : requestList) {
				
				boolean acceptedRequest = current.grantRequest(request);
				if (!acceptedRequest) {
					continue;
				}
				
				current.f();
				goodness = current.goodness;
				
				if (goodness < prevGoodness) {
					current.rollbackPreviousRequest();
					//reqDone++;
					System.out.printf("Rollback done: goodness = %d, previousGoodnes = %d\n", goodness, prevGoodness);
				} else {
					reqDone++;
					sum++;
				}
				
				if (current.isValid == false) {
					//System.out.printf("%s\n", request);
				}
				
				prevGoodness = goodness;
				currentTime = System.currentTimeMillis();
				if (currentTime >= timeout) {
					return current;
				}
			}
			if (reqDone == 0)
				break;
		}
		
		printGoodnes(goodness);
		System.out.printf("Total requests done = %d\n", sum);
		current.checkValid();
		
		return current;
	}
	
	/**
	 * Prints goodness of current solution.
	 * @param goodness goodness of current solution
	 */
	private void printGoodnes(int goodness) {
		System.out.printf("Goodness = %d\n", goodness);
	}
	
	/**
	 * Makes greedy search for this problem by checking all of the requests in random order and trying to add
	 * each request until no more requests are able to be added.
	 * @param rand Random object used for shuffling list of all requests
	 * @return new random greedy solution
	 */
	public Chromosome greedySearch(Random rand) {
		Chromosome current = new Chromosome();
		current.greed(rand);
		current.f();
		return current;
	}
	
	/**
	 * Checks if the time assigned to this program has passed.
	 * @return <code>true</code> if time assigned to this program has passed, <code>false</code> otherwise
	 */
	public boolean isTimedOut() {
		currentTime = System.currentTimeMillis();
		if (currentTime < timeout)
			return false;
		return true;
	}
	
	/**
	 * Returns time passed in seconds.
	 * @return time passed in seconds
	 */
	public double getTimePassed() {
		currentTime = System.currentTimeMillis();
		return 1.0*(currentTime - startTime) / 1000.0;
	}
	
	/**
	 * Returns time passed in seconds in "%d minutes, %.1f seconds" format.
	 * @return time passed in seconds in "%d minutes, %.1f seconds" format
	 */
	public String stringTimePassed() {
		double timePassed = getTimePassed();
		if (timePassed < 60) {
			return timePassed + " seconds";
		}
		int minutes = ((int)timePassed) / 60;
		double seconds = timePassed - 60 * minutes;
		return String.format("%d minutes, %.1f seconds", minutes, seconds);
	}
	
	/**
	 * Prints goodness of the problem without any request been granted.
	 */
	public void printInitialGoodness() {
		Chromosome c = new Chromosome();
		//initialGoodness = c.goodness;
		System.out.printf(" Initial Goodness: (%d); A = %d, B = %d, C = %d, D = %d, E = %d.\n", c.goodness, c.a, c.b, c.c, -c.getD(), -c.getE());
	}
	
	public int getInitialGoodness() {
		Chromosome c = new Chromosome();
		return c.goodness;
	}
	
	/**
	 * This class represents one possible solution of the problem, after some of the requests have been granted.
	 * This class is a valid solution that abides the hard constraints of the problem in each of its states.
	 * <p>
	 * There are 2 ways of moving through the solution space:
	 * <pre>
	 * 1) Grant request: tries to grant request, request can be granted if the new chromosome 
	 * 		is a valid solution as well.
	 * 2) Roll-back request: tries to roll-back request, request can be roll-backed if the new chromosome
	 * 		is a valid solution as well.
	 * </pre>
	 * 
	 * @author Josip Kasap
	 *
	 */
	public class Chromosome {
		
		// All of the granted requests
		//public List<Request> grantedRequests = new ArrayList<>();
		
		Request prevRequest = null;
		boolean isValid = true;
		
		// Values used to determine goodness
		public int a, b, c, de;
		public int goodness = 0;
		
		// Set of all granted requests
		public Set<Request> grantedRequests = new HashSet<>();
		
		// Maps student -> all groups that student attends; so that the groups student attends can't be overlapped
		public Map<Long, Set<Long>> studentGroupsC = new HashMap<>();
		
		// Limits the students in the group for each group
		public Map<Long, GroupLimit> groupLimitsC = new HashMap<>();
		
		// Maps student & activity -> approved request
		public Map<Long, Map<Long, Request>> approvedRequests = new HashMap<>();
		
		/**
		 * Creates a chromosome from given chromosome.
		 * @param chrom chromosome
		 */
		public Chromosome(Chromosome chrom) {
			super();
			
			for (long group_id : chrom.groupLimitsC.keySet()) {
				groupLimitsC.put(group_id, chrom.groupLimitsC.get(group_id).copy());
			}
			
			for (long student_id : chrom.studentGroupsC.keySet()) {
				studentGroupsC.put(student_id, new HashSet<Long>(chrom.studentGroupsC.get(student_id)));
			}
			
			grantedRequests = new HashSet<>(chrom.grantedRequests);
			
			for (long student_id : chrom.approvedRequests.keySet()) {
				Map<Long, Request> mapForStudent = chrom.approvedRequests.get(student_id);
				approvedRequests.put(student_id, new HashMap<>(mapForStudent));
			}
			
			prevRequest = chrom.prevRequest;
			
			a = chrom.a;
			b = chrom.b;
			c = chrom.c;
			de = chrom.de;
			goodness = a + b + c - de;
		}
		
		/**
		 * Creates new (empty) chromosome.
		 */
		public Chromosome() {
			super();
			
			for (long group_id : groupLimits.keySet()) {
				groupLimitsC.put(group_id, groupLimits.get(group_id).copy());
			}
			
			for (long student_id : studentGroups.keySet()) {
				studentGroupsC.put(student_id, new HashSet<Long>(studentGroups.get(student_id)));
			}
			
			for (long group_id : groups) {
				GroupLimit limit = groupLimitsC.get(group_id);
				
				if (limit.isOutOfLimits()) {
					System.out.printf(">> Initial arguments are wrong. Group %l exceeds limits.\n", limit.group_id);
				} else if (limit.isMax()) {
					//maxGroups.add(group_id);
				} else if (limit.isMin()) {
					//minGroups.add(group_id);
				}
			}
			
			a = 0;
			b = 0;
			c = 0;
			de = getDE();
			
			goodness = a + b + c - de;
		}
		
		/**
		 * This method checks if this chromosome is valid. 
		 * As each chromosome has to be valid in each state, 
		 * if this class prints anything it means that I did an upsie in the code.
		 */
		public void checkValid() {
			for (Request r : grantedRequests) {
				Request r2 = null;
				try {
				 r2 = approvedRequests.get(r.student_id).get(r.activity_id);
				} catch (NullPointerException e) {
					System.out.printf("$$>>>> Error, invalid state: for student and activity requests don't match: %s \n", r);
				}
				if (!r.equals(r2)) {
					System.out.printf("$$>>>> Error, invalid state: for student and activity requests don't match: %s -%s\n", r, r2);
				}
			}
			
			groupLimitsC.values().stream().filter((limit) -> limit.isOutOfLimits()).forEach((limit) -> {
					System.out.printf("$$>>>> Error, invalid state: limit is out of bounds: %s\n", limit);
				}
			);
		}

		/**
		 * This method checks if given request would create an overlap for the student.
		 * @param request request that might create an overlap
		 * @return <code>true</code> if this request would create an overlap for the student, <code>false</code> otherwise
		 */
		private boolean checkForOverlaps(Request request) {
			Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
			Set<Long> overlapsForWantedGroup = overlapMap.get(request.wanted_group_id);
			
			return (Algorithms.isEmptyIntersectionIgnoreOne(groupsForStudent, overlapsForWantedGroup, request.group_id) == false);
		}
		
		/**
		 * This method checks if given request rollback would create an overlap for the student.
		 * @param request request whose rollback might create an overlap for student
		 * @return <code>true</code> if this request rollback would create an overlap for the student, 
		 * 		<code>false</code> otherwise
		 */
		private boolean checkForOverlapsReversed(Request request) {
			Set<Long> initialGroupsForStudent = studentGroups.get(request.student_id);
			Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
			Set<Long> overlapsForGroup = overlapMap.get(request.group_id);
			
			Set<Long> overlapsSet = Algorithms.intersection(groupsForStudent, overlapsForGroup);
			
			for (long group_id : overlapsSet) {
				if (!initialGroupsForStudent.contains(group_id) && group_id != request.wanted_group_id) {
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * This method returns status of granting the given request. Status of granting a request can be:
		 * <br> Can be granted
		 * <br> Already granted
		 * <br> Request activity resolved
		 * <br> Request from minimal student count group
		 * <br> Request to maximal student count group
		 * <br> Impossible request
		 * <br> Has overlaps
		 * @param request request for transmission
		 * @return <code>true</code> if this request has been granted, <code>false</code> otherwise
		 */
		public String getGrantRequestMessage(Request request) {
			// Secure that you are not granting same request twice
			if (grantedRequests.contains(request))
				return "+";
			
			// Secure that there is no approved request for the same student and activity
			Map<Long, Request> requestsForStudent = approvedRequests.get(request.student_id);
			if (requestsForStudent != null) {
				if (requestsForStudent.get(request.activity_id) != null) {
					return "-";
				}
			}
			
			if (!requestList.contains(request)) {
				if (checkForOverlaps(request)) {
					Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
					Set<Long> overlapsForWantedGroup = overlapMap.get(request.wanted_group_id);
					Set<Long> overlapedGroups = Algorithms.intersection(groupsForStudent, overlapsForWantedGroup);
					overlapedGroups.remove(request.group_id);
					return "IMPOSSIBLE: " + Algorithms.getSetAsString(overlapedGroups);
				}
				GroupLimit limit1 = groupLimitsC.get(request.group_id);
				GroupLimit limit2 = groupLimitsC.get(request.wanted_group_id);
				if (limit1.isMin()) {
					return "IMPOSSIBLE: From MINIMAL - " + "group: " + request.group_id + 
							"; student_cnt: " + limit1.students_cnt + "; min: " + limit1.min;
				} else if (limit2.isMax()) {
					return "IMPOSSIBLE: To MAXIMAL - " + "group: " + request.wanted_group_id + 
							"; student_cnt: " + limit2.students_cnt + "; max: " + limit2.max;
				}
			}
			
			// Check for overlaps
			if (checkForOverlaps(request)) {
				Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
				Set<Long> overlapsForWantedGroup = overlapMap.get(request.wanted_group_id);
				Set<Long> overlapedGroups = Algorithms.intersection(groupsForStudent, overlapsForWantedGroup);
				overlapedGroups.remove(request.group_id);
				return Algorithms.getSetAsString(overlapedGroups);
			}
			
			// Secure that granting this request will still secure the hard constraints of group limits
			GroupLimit limit1 = groupLimitsC.get(request.group_id);
			GroupLimit limit2 = groupLimitsC.get(request.wanted_group_id);
			if (limit1.isMin()) {
				return "From MINIMAL - " + "group: " + request.group_id + "; student_cnt: " + limit1.students_cnt + "; min: " + limit1.min;
			} else if (limit2.isMax()) {
				return "To MAXIMAL - " + "group: " + request.wanted_group_id + "; student_cnt: " + limit2.students_cnt + "; max: " + limit2.max;
			}
			
			Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
			groupsForStudent.remove(request.group_id);
			groupsForStudent.add(request.wanted_group_id);
			
			// Actually grant the request when proven that all of the hard constraints are good
			
			grantedRequests.add(request);
			
			Map<Long, Request> activityForRequest = approvedRequests.get(request.student_id);
			if (activityForRequest == null) {
				activityForRequest = new HashMap<>();
				approvedRequests.put(request.student_id, activityForRequest);
			}
			
			return "REQUEST CAN BE GRANTED";
		}
		
		/**
		 * This method grants request if this request is possible to grant. Meaning that if doing
		 * so would not harm the hard constraints.
		 * @param request request for transmission
		 * @return <code>true</code> if this request has been granted, <code>false</code> otherwise
		 */
		public boolean grantRequest(Request request) {
			// Secure that you are not granting same request twice
			if (grantedRequests.contains(request))
				return false;
			
			// Secure that there is no approved request for the same student and activity
			Map<Long, Request> requestsForStudent = approvedRequests.get(request.student_id);
			if (requestsForStudent != null) {
				if (requestsForStudent.get(request.activity_id) != null) {
					return false;
				}
			}
			
			// Secure that granting this request will still secure the hard constraints of group limits
			GroupLimit limit1 = groupLimitsC.get(request.group_id);
			GroupLimit limit2 = groupLimitsC.get(request.wanted_group_id);
			if (limit1.isMin() || limit2.isMax()) {
				return false;
			}
			
			// Chech for overlaps
			if (checkForOverlaps(request)) {
				return false;
			}
			
			Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
			groupsForStudent.remove(request.group_id);
			groupsForStudent.add(request.wanted_group_id);
			
			// Actually grant the request when proven that all of the hard constraints are good
			
			grantedRequests.add(request);
			
			Map<Long, Request> activityForRequest = approvedRequests.get(request.student_id);
			if (activityForRequest == null) {
				activityForRequest = new HashMap<>();
				approvedRequests.put(request.student_id, activityForRequest);
			}
			
			if (activityForRequest.get(request.activity_id) != null) {
				isValid = false;
				System.out.printf("$$>>>>>> REQUEST = %s; invalid machine state!\n", request);
			}
			activityForRequest.put(request.activity_id, request);
			
			// Update goodness parameters
			de += (limit1.decreaseValue() + limit2.increaseValue()) * minmax_penalty;
			
			a  += request.swap_weight;
			
			int totalRequestsGranted = activityForRequest.size();
			int index = totalRequestsGranted - 1;
			if (totalRequestsGranted == 1) {
				b += award_activity[0];
			} else if (index <= award_activity.length - 1) {
				b += ( award_activity[index] - award_activity[index - 1] );
			}
			if (totalRequestsGranted == requestsMap.get(request.student_id).size()) {
				c += award_student;
			}
			
			prevRequest = request;
			return true;
		}
		
		/**
		 * This method roll-backs request if this request is possible to roll-back. Meaning that if doing
		 * so would not harm the hard constraints.
		 * @param request request for transmission
		 * @return <code>true</code> if this request has been granted, <code>false</code> otherwise
		 */
		public boolean rollbackRequest(Request request) {
			
			// Make sure that request is granted before rollbacking it
			if (!grantedRequests.contains(request)) {
				return false;
			}
			
			GroupLimit limit1 = groupLimitsC.get(request.group_id);
			GroupLimit limit2 = groupLimitsC.get(request.wanted_group_id);
			
			if (limit1.isMax() || limit2.isMin()) {
				return false;
			}
			
			// Chech for overlaps
			if (checkForOverlapsReversed(request)) {
				return false;
			}
			
			Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
			groupsForStudent.remove(request.wanted_group_id);
			groupsForStudent.add(request.group_id);
			
			grantedRequests.remove(request);
			
			Map<Long, Request> activityForRequest = approvedRequests.get(request.student_id);
			if (activityForRequest != null) {
				activityForRequest.remove(request.activity_id);
				if (activityForRequest.isEmpty()) {
					approvedRequests.remove(request.student_id);
				}
			}
			
			de += (limit1.increaseValue() + limit2.decreaseValue()) * minmax_penalty;
			
			a  += -request.swap_weight;
			
			int totalRequestsGranted = activityForRequest.size();
			int index = totalRequestsGranted - 1;
			if (totalRequestsGranted == 0) {
				b += -award_activity[0];
			} else if (index <= award_activity.length - 2) {
				b += -( award_activity[index + 1] - award_activity[index] );
			}
			if (totalRequestsGranted + 1 == requestsMap.get(request.student_id).size()) {
				c += -award_student;
			}
			
			prevRequest = null;
			return true;
		}
		
		/**
		 * This method roll-backs previous request.
		 * @return <code>true</code>, because previous requests can always be roll-backed
		 */
		public boolean rollbackPreviousRequest() {
			if (prevRequest == null)
				return false;
			
			boolean retValue = rollbackRequest(prevRequest);
			
			prevRequest = null;
			return retValue;
		}
		
		public boolean grantRequestForce(Request request) {
			
			// Secure that you are not granting same request twice
			if (grantedRequests.contains(request))
				return false;
			
			// Secure that request exists
			if (requestList.contains(request) == false) {
				return false;
			}
			
			GroupLimit limit1 = groupLimitsC.get(request.group_id);
			GroupLimit limit2 = groupLimitsC.get(request.wanted_group_id);
			
			Set<Long> groupsForStudent = studentGroupsC.get(request.student_id);
			groupsForStudent.remove(request.group_id);
			groupsForStudent.add(request.wanted_group_id);
			
			// Actually grant the request when proven that all of the hard constraints are good
			
			grantedRequests.add(request);
			
			Map<Long, Request> activityForRequest = approvedRequests.get(request.student_id);
			if (activityForRequest == null) {
				activityForRequest = new HashMap<>();
				approvedRequests.put(request.student_id, activityForRequest);
			}
			
			if (activityForRequest.get(request.activity_id) != null) {
				isValid = false;
				System.out.printf("$$>>>>>> REQUEST = %s; invalid machine state!\n", request);
			}
			activityForRequest.put(request.activity_id, request);
			
			// Update goodness parameters
			de += (limit1.decreaseValue() + limit2.increaseValue()) * minmax_penalty;
			
			a  += request.swap_weight;
			
			int totalRequestsGranted = activityForRequest.size();
			int index = totalRequestsGranted - 1;
			if (totalRequestsGranted == 1) {
				b += award_activity[0];
			} else if (index <= award_activity.length - 1) {
				b += ( award_activity[index] - award_activity[index - 1] );
			}
			if (totalRequestsGranted == requestsMap.get(request.student_id).size()) {
				c += award_student;
			}
			
			prevRequest = request;
			return true;
		}

		/**
		 * Writes solution of the program to file.
		 * @param file file to write the solution of the program to
		 * @throws IOException if anything goes wrong
		 */
		public void writeToFile(String fileIn, String fileOut) throws IOException {
			List<String> studentsLines = Files.readAllLines(Paths.get(fileIn));
			List<String> newStudentsLines = new ArrayList<>();
			newStudentsLines.add(studentsLines.get(0));
			
			for (int i=1; i<studentsLines.size(); i++) {
				String line = studentsLines.get(i);
				String[] arr = line.split(",");
				long student_id = Long.parseLong(arr[0]);
				long activity_id = Long.parseLong(arr[1]);
				//long swap_weight = Long.parseLong(arr[2]);
				//long group_id = Long.parseLong(arr[3]);
				
				Request grantedRequest = null;
				Map<Long, Request> activityForRequest = approvedRequests.get(student_id);
				if (activityForRequest != null) {
					grantedRequest = activityForRequest.get(activity_id);
				}
				
				String s = line;
				if (grantedRequest != null) {
					arr[4] = "" + grantedRequest.wanted_group_id;
					s = String.join(",", arr);
				} 
				
				newStudentsLines.add(s);
			}
			
			Files.write(Paths.get(fileOut), newStudentsLines);
		}
		
		/**
		 * Calculates the goodness of the solution.
		 * @return <code>true<code>
		 */
		public boolean f() {
			
			goodness = a + b + c - de;
			return isValid;
		}
		
		/**
		 * Calculates D + E score.
		 * @return D + E score
		 */
		private int getDE() {
			int value = 0;
			
			for (long group_id : groupLimitsC.keySet()) {
				GroupLimit limit = groupLimitsC.get(group_id);
				value += limit.getDE(minmax_penalty);
			}
			
			return value;
		}
		
		/**
		 * Calculates D score.
		 * @return D score
		 */
		public int getD() {
			int value = 0;
			
			for (long group_id : groupLimitsC.keySet()) {
				GroupLimit limit = groupLimitsC.get(group_id);
				value += limit.getD(minmax_penalty);
			}
			
			return value;
		}
		
		/**
		 * Calculates E score.
		 * @return E score
		 */
		public int getE() {
			int value = 0;
			
			for (long group_id : groupLimitsC.keySet()) {
				GroupLimit limit = groupLimitsC.get(group_id);
				value += limit.getE(minmax_penalty);
			}
			
			return value;
		}

		public int getRequestsGranted() {
			return grantedRequests.size();
		}

		/**
		 * Makes greedy solution to the program from this point.
		 * @param rand Random for calculating greedy solution
		 */
		public void greed(Random rand) {
			while (true) {
				int reqDone = 0;
				
				Collections.shuffle(requestList, rand);
				for (Request request : requestList) {
					
					boolean acceptedRequest = grantRequest(request);
					if (!acceptedRequest) {
						continue;
					}
					
					reqDone++;
					
					f();
					
					if (isValid == false) {
						System.out.printf("Error, not valid machine state, for request = %s\n", request);
						
						System.out.printf("Limits initial, GROUP = \t%s -> %s\n", request.group_id, groupLimits.get(request.group_id));
						System.out.printf("Limits changed, GROUP = \t%s -> %s\n", request.group_id, groupLimitsC.get(request.group_id));
						
						System.out.printf("Limits initial, WANTED_GROUP = \t%s -> %s\n", request.wanted_group_id, 
								groupLimits.get(request.wanted_group_id));
						System.out.printf("Limits changed, WANTED_GROUP = \t%s -> %s\n", request.wanted_group_id, 
								groupLimitsC.get(request.wanted_group_id));
						System.exit(1);
					}
				}
				if (reqDone == 0)
					break;
			}
			checkValid();
		}

		public Chromosome copy() {
			return new Chromosome(this);
		}
	}
}