package hr.fer.diplomskirad.zamjenagrupa.data;

import java.util.*;

import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

public class ChromosomeAdvanced {
	
	public Map<Long, Student> studentMap = new HashMap<>();
	public List<Student> studentList = new ArrayList<>();
	public DataAdvanced data;
	public Map<Long, GroupLimit> groupLimits;
	public LimitsBoundsChecker limitsChecker;
	
	private int a=0, b=0, c=0;
	private int initialGoodness;
	
	public ChromosomeAdvanced(DataAdvanced data) {
		
		this.data = data;
		groupLimits = new HashMap<>();
		for (long group_id : data.groupLimits.keySet()) {
			GroupLimit limit = data.groupLimits.get(group_id);
			groupLimits.put(group_id, limit.copy());
		}
		limitsChecker = new LimitsBoundsChecker(groupLimits, data.minmax_penalty);
		initialGoodness = -limitsChecker.getD() -limitsChecker.getE();
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			Map<Long, List<Request>> activityRequestsMap = data.requestsMap.get(student_id);
			if (Utils.checkIfEmpty(activityRequestsMap))
				continue;
			
			Set<Long> groups = data.studentGroups.get(student_id);
			
			Student student = new Student(student_id, groups, activityRequestsMap, data.requestSet,
					data.overlapMap, data.minmax_penalty, data.award_activity, data.award_student, limitsChecker);
			
			studentMap.put(student_id, student);
			studentList.add(student);
		}
	}
	
	public ChromosomeAdvanced(ChromosomeAdvanced other) {
		this.data = other.data;
		this.a = other.a;
		this.b = other.b;
		this.c = other.c;
		this.initialGoodness = other.initialGoodness;
		
		groupLimits = new HashMap<>();
		for (long group_id : other.groupLimits.keySet()) {
			GroupLimit limit = other.groupLimits.get(group_id);
			groupLimits.put(group_id, limit.copy());
		}
		
		limitsChecker = new LimitsBoundsChecker(groupLimits, data.minmax_penalty);
		
		for (Student student : other.studentList) {
			Student copiedStudent = student.copy(limitsChecker);
			studentMap.put(copiedStudent.getStudentId(), copiedStudent);
			studentList.add(copiedStudent);
		}
	}

	public ChromosomeAdvanced(DataAdvanced data, Map<Long, Map<Long, Request>> randomlySelectedRequests) {
		this.data = data;
		groupLimits = new HashMap<>();
		for (long group_id : data.groupLimits.keySet()) {
			GroupLimit limit = data.groupLimits.get(group_id);
			groupLimits.put(group_id, limit.copy());
		}
		limitsChecker = new LimitsBoundsChecker(groupLimits, data.minmax_penalty);
		initialGoodness = -limitsChecker.getD() -limitsChecker.getE();
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			Map<Long, List<Request>> activityRequestsMap = data.requestsMap.get(student_id);
			if (Utils.checkIfEmpty(activityRequestsMap))
				continue;
			
			Set<Long> groups = data.studentGroups.get(student_id);
			
			Student student = new Student(student_id, groups, activityRequestsMap, data.requestSet,
					data.overlapMap, data.minmax_penalty, data.award_activity, data.award_student, limitsChecker);
			
			if (randomlySelectedRequests.containsKey(student_id)) {
				student.forceApprove(randomlySelectedRequests.get(student_id), groupLimits);
				a += student.getA();
				b += student.getB();
				c += student.getC();
			}
			
			studentMap.put(student_id, student);
			studentList.add(student);
		}
	}

	public ChromosomeAdvanced copy() {
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(this);
		return chrome;
	}
	
	public void grantRequestsGreedy(Random rand) {
		boolean hasGranted = true;
		while (hasGranted) {
			hasGranted = false;
			Collections.shuffle(studentList, rand);
			for (Student student : studentList) {
				
				int aPrev = student.getA();
				int bPrev = student.getB();
				int cPrev = student.getC();
				
				boolean granted = student.grantRequests(rand, groupLimits);
				if (granted) {
					hasGranted = true;
					
					int aNew = student.getA();
					int bNew = student.getB();
					int cNew = student.getC();
					
					a += aNew - aPrev;
					b += bNew - bPrev;
					c += cNew - cPrev;
				}
			}
		}
	}
	
	public boolean grantRequests(long student_id, Random rand, Collection<Request> requests) {
		boolean hasGranted = false;
		Student student = studentMap.get(student_id);
		int aPrev = student.getA();
		int bPrev = student.getB();
		int cPrev = student.getC();
		
		for (Request request : requests) {
			boolean granted = student.grantRequest(request, groupLimits, rand);
			hasGranted = hasGranted || granted;
		}
		
		int aNew = student.getA();
		int bNew = student.getB();
		int cNew = student.getC();
		
		a += aNew - aPrev;
		b += bNew - bPrev;
		c += cNew - cPrev;
		
		return hasGranted;
	}
	
	public int rollbackStudent(long student_id) {
		Student student = studentMap.get(student_id);
		int aPrev = student.getA();
		int bPrev = student.getB();
		int cPrev = student.getC();
		
		int ret = student.rollbackRequests(groupLimits);
		
		int aNew = student.getA();
		int bNew = student.getB();
		int cNew = student.getC();
		
		a += aNew - aPrev;
		b += bNew - bPrev;
		c += cNew - cPrev;
		
		return ret;
	}
	
	public int[] getGoodnessParameters() {
		int[] goonessParameters = new int[6];
		goonessParameters[0] = a;
		goonessParameters[1] = b;
		goonessParameters[2] = c;
		goonessParameters[3] = limitsChecker.getD();
		goonessParameters[4] = limitsChecker.getE();
		goonessParameters[5] = initialGoodness;
		
		return goonessParameters;
	}
	
	public int getGoodness() {
		return a + b + c - limitsChecker.getD() - limitsChecker.getE();
	}

	public int getRequestsGranted() {
		int requestsGranted = 0;
		for (Student student : studentList) {
			requestsGranted += student.getRequestsGranted();
		}
		return requestsGranted;
	}
	
	public static ChromosomeAdvanced getRandomChromosome(DataAdvanced data, Random rand) {
		return new ChromosomeAdvanced(data, getRandomlySelectedRequests(rand, data));
	}

	private static Map<Long, Map<Long, Request>> getRandomlySelectedRequests(Random rand, DataAdvanced data) {
		Map<Long, Map<Long, List<Request>>> randomRequests = new HashMap<>();
		
		double requestGrantedMin = 0.45;
		double requestGrantedMax = 0.9;
		double requestGrantedChance = rand.nextDouble() * (requestGrantedMax - requestGrantedMin) + requestGrantedMin;
		
		
		for (Request r : data.requestList) {
			Map<Long, List<Request>> requestsForStudent = null;
			if (!randomRequests.containsKey(r.student_id)) {
				requestsForStudent = new HashMap<>();
				//randomRequests.put(r.student_id, requestsForStudent);
			} else {
				requestsForStudent = randomRequests.get(r.student_id);
			}
			
			List<Request> requestsForActivity = null;
			
			if (!requestsForStudent.containsKey(r.activity_id)) {
				requestsForActivity = new ArrayList<>();
				//requestsForStudent.put(r.activity_id, requestsForActivity);
			} else {
				requestsForActivity = requestsForStudent.get(r.activity_id);
			}
			
			if (rand.nextDouble() < requestGrantedChance) {
				requestsForActivity.add(r);
				if (!requestsForStudent.containsKey(r.activity_id)) {
					requestsForStudent.put(r.activity_id, requestsForActivity);
				}
				if (!randomRequests.containsKey(r.student_id)) {
					randomRequests.put(r.student_id, requestsForStudent);
				}
			}
		}
		
		Map<Long, Map<Long, Request>> radomApprovedRequests = new HashMap<>();
		
		for (long student_id : randomRequests.keySet()) {
			Map<Long, Request> approvedForStudent = new HashMap<>();
			for (long activity_id : randomRequests.get(student_id).keySet()) {
				List<Request> randomlyApproved = randomRequests.get(student_id).get(activity_id);
				if (randomlyApproved.isEmpty())
					continue;
				
				Request approvedRequest = randomlyApproved.get(rand.nextInt(randomlyApproved.size()));
				approvedForStudent.put(activity_id, approvedRequest);
			}
			
			if (!approvedForStudent.isEmpty())
				radomApprovedRequests.put(student_id, approvedForStudent);
		}
		
		return radomApprovedRequests;
	}
	
	public void validateAndGreed(Random rand) {
		getValidSolution(rand);
		grantRequestsGreedy(rand);
	}

	public void getValidSolution(Random rand) {
		List<Long> students = data.students;
		boolean hasConflict = true;
		
		while (hasConflict) {
			Collections.shuffle(students, rand);
			hasConflict = false;
			
			for (long student_id : students) {
				Student student = studentMap.get(student_id);
				
				if (student == null || student.getApprovedRequests().isEmpty())
					continue;
				
				int aPrev = student.getA();
				int bPrev = student.getB();
				int cPrev = student.getC();
				
				if (student.removeInvalidRequests(groupLimits, data.overlapMap)) {
					hasConflict = true;
					
					int aNew = student.getA();
					int bNew = student.getB();
					int cNew = student.getC();
					
					a += aNew - aPrev;
					b += bNew - bPrev;
					c += cNew - cPrev;
				}
			}
		}
	}
	
	public void forceStudentRequests(Map<Long, Request> requestsForStudent, long student_id) {
		Student student = studentMap.get(student_id);
		
		int aPrev = student.getA();
		int bPrev = student.getB();
		int cPrev = student.getC();
		student.forceApprove(requestsForStudent, groupLimits);
		int aNew = student.getA();
		int bNew = student.getB();
		int cNew = student.getC();
		
		a += aNew - aPrev;
		b += bNew - bPrev;
		c += cNew - cPrev;
	}
	
	public void forceGrantRequest(Request r, long student_id) {
		Student student = studentMap.get(student_id);
		
		int aPrev = student.getA();
		int bPrev = student.getB();
		int cPrev = student.getC();
		student.forceGrantRequest(r, groupLimits);
		int aNew = student.getA();
		int bNew = student.getB();
		int cNew = student.getC();
		
		a += aNew - aPrev;
		b += bNew - bPrev;
		c += cNew - cPrev;
	}
}