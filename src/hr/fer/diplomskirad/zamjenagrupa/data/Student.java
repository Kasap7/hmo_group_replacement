package hr.fer.diplomskirad.zamjenagrupa.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.Algorithms;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

/**
 * This class represents one student and all of his requests, both pending and resolved ones.
 * 
 * @author Josip Kasap
 *
 */
public class Student {
	
	/** Unique identifier of a student */
	private long student_id;
	
	/** Set of current student groups (after all of the possible requests have been accepted) */
	private Set<Long> groups;
	
	/** Set of initial student groups */
	private Set<Long> initialGroups;
	
	/** 
	 * Map of all possible requests: Activity -> List of possible requests.
	 * This is after all of the impossible requests have been deleted.
	 * This map also does not contain requests that would compromise group limits.
	 */
	private Map<Long, List<Request>> requests;
	
	/** 
	 * Map of all possible requests: Activity -> List of possible requests.
	 * This is after all of the impossible requests have been deleted
	 */
	private Map<Long, List<Request>> requestsInit = new HashMap<>();
	
	/** Map of all initial requests: Activity -> List of all student requests */
	private Map<Long, List<Request>> requestsInitial;

	/** 
	 * Maps each of request with list of activities that has to be resolved in order for this request to have no conflicts:
	 * Request -> List of activities
	 */
	private Map<Request, List<Long>> activitiesToResolve = new HashMap<>();
	
	/** Maps Request -> List of impossible requests for student; because they create overlaps for 2 wanted groups. */
	private Map<Request, List<Request>> impossibleRequestsMap = new HashMap<>();
	
	/** List of all activities that have at least 1 request for this student */
	private List<Long> requestActivitiesList = new ArrayList<>();
	
	/** Maps activity with request that has been granted (if there is such for that activity) */
	private Map<Long, Request> approvedRequests = new HashMap<>();
	
	private LimitsBoundsChecker limitsChecker;
	
	// Parameters used to calculate the goodness of a solution
	private int a=0, b=0, c=0;
	
	private int minmax_penalty;
	private int[] award_activity;
	private int award_student;
	
	private Set<Request> impossibleRequests;

	/**
	 * This constructor creates new student with all of his requests been not accepted.
	 * @param student_id unique identifier of student
	 * @param initialGroups initial groups this student has
	 * @param requestsInitial map of all student requests
	 * @param requestList list of all possible requests
	 * @param overlapMap maps group with set of groups that overlap with given group
	 * @param limitsChecker 
	 */
	public Student(long student_id, Set<Long> initialGroups, Map<Long, List<Request>> requestsInitial, 
			Set<Request> requestSet, Map<Long, Set<Long>> overlapMap, int minmax_penalty, int[] award_activity, 
			int award_student, LimitsBoundsChecker limitsChecker) {
		
		this.limitsChecker = limitsChecker;
		this.student_id = student_id;
		this.initialGroups = initialGroups;
		this.setGroups(new HashSet<>(initialGroups));
		this.requestsInitial = requestsInitial;
		this.minmax_penalty = minmax_penalty;
		this.award_activity = award_activity;
		this.award_student = award_student;
		
		Set<Long> wantedGroups = new HashSet<>();
		Set<Long> groupsWithRequests = new HashSet<>();
		Set<Long> groupsWithoutRequests = new HashSet<>(initialGroups);
		
		Map<Long, Long> inputGroupToActivityMap = new HashMap<>();
		Map<Long, Request> wantedGroupMap = new HashMap<>();
		
		for (long activity_id : requestsInitial.keySet()) {
			List<Request> requestsForActivity = requestsInitial.get(activity_id);
			List<Request> possibleRequests = new ArrayList<>();
			
			for (Request r : requestsForActivity) {
				if (requestSet.contains(r)) {
					possibleRequests.add(r);
				}
				wantedGroups.add(r.wanted_group_id);
				groupsWithRequests.add(r.group_id);
				
				wantedGroupMap.put(r.wanted_group_id, r);
				inputGroupToActivityMap.put(r.group_id, r.activity_id);
			}
			
			if (!possibleRequests.isEmpty()) {
				requestsInit.put(activity_id, possibleRequests);
				requestActivitiesList.add(activity_id);
			}
		}
		
		groupsWithoutRequests.removeAll(groupsWithRequests);
		impossibleRequests = new HashSet<>();
		for (long activity_id : requestsInitial.keySet()) {
			
			List<Request> allRequests = requestsInitial.get(activity_id);
			if (!requestsInit.containsKey(activity_id)) {
				impossibleRequests.addAll(allRequests);
			}
			for (Request r : allRequests) {
				//if (r.toString().equals("(17596,3542314,1,168826,168825)")) {
				//	System.out.println("DELETE THIS LATER!!");
				//}
				
				if (requestsInit.containsKey(activity_id) && !requestsInit.get(activity_id).contains(r)) {
					impossibleRequests.add(r);
				}
				
				Set<Long> overlapsWithWantedGroup = overlapMap.get(r.wanted_group_id);
				
				Set<Long> intersectionWantedGroups = Algorithms.intersection(overlapsWithWantedGroup, wantedGroups);
				intersectionWantedGroups.remove(r.group_id);
				intersectionWantedGroups.remove(r.wanted_group_id);
				
				Set<Long> intersectionInputGroups = Algorithms.intersection(overlapsWithWantedGroup, groupsWithRequests);
				intersectionInputGroups.remove(r.group_id);
				intersectionInputGroups.remove(r.wanted_group_id);
				
				Set<Long> intersectionsImpossibleToSolve = Algorithms.intersection(overlapsWithWantedGroup, groupsWithoutRequests);
				intersectionsImpossibleToSolve.remove(r.group_id);
				intersectionsImpossibleToSolve.remove(r.wanted_group_id);
				
				if (intersectionsImpossibleToSolve.size() > 0) {
					impossibleRequests.add(r);
				}
				
				List<Request> impossibleRequestsList = new ArrayList<>();
				for (long group_id : intersectionWantedGroups) {
					Request impossibleRequest = wantedGroupMap.get(group_id);
					impossibleRequestsList.add(impossibleRequest);
				}
				impossibleRequestsMap.put(r, impossibleRequestsList);
				
				List<Long> activitiesList = new ArrayList<>();
				for (long group_id : intersectionInputGroups) {
					long activityToResolve = inputGroupToActivityMap.get(group_id);  // Long
					activitiesList.add(activityToResolve);
				}
				activitiesToResolve.put(r, activitiesList);
			}
		}
		
		removeImpossibleRequests();
	}
	
	private void removeImpossibleRequests() {
		for (long activity_id : requestsInit.keySet()) {
			List<Request> requests = requestsInit.get(activity_id);
			for (Request request : requests) {
				if (impossibleRequests.contains(request))
					continue;
				
				if (!canRequestBeGranted(request)) {
					impossibleRequests.add(request);
				}
				
				//if (!Utils.checkSetEquals(initialGroups, groups)) {
				//	System.err.println("ERROR HAPPENED HERE!!");
				//}
			}
		}
		
		Map<Long, List<Request>> newRequestsInit = new HashMap<>();
		for (long activity_id : requestsInit.keySet()) {
			List<Request> requests = requestsInit.get(activity_id);
			List<Request> newRequests = new ArrayList<>();
			for (Request request : requests) {
				if (!impossibleRequests.contains(request)) {
					newRequests.add(request);
				}
			}
			
			if (!newRequests.isEmpty()) {
				newRequestsInit.put(activity_id, newRequests);
			}
		}
		
		requestsInit = newRequestsInit;
	}

	private boolean canRequestBeGranted(Request request) {
		
		groupsGrantRequest(request);
		
		Map<Long, Request> granted = new HashMap<>();
		granted.put(request.activity_id, request);
		
		List<Long> activitiesToResolveList = activitiesToResolve.get(request);
		
		if (!activitiesToResolveList.isEmpty() && granted != null) {
			Set<Request> impossibleRequestsSet = getImpossibleRequestsSet(request);
			granted = canRequestBeGrantedRecursive(granted, new HashSet<>(activitiesToResolveList), impossibleRequestsSet);
		} 
		
		if (granted == null || granted.isEmpty()) {
			groupsRemoveRequest(request);
			return false;
		} else {
			for (Request r : granted.values()) {
				groupsRemoveRequest(r);
			}
			return true;
		}
	}

	private Map<Long, Request> canRequestBeGrantedRecursive(Map<Long, Request> grantedRequests, Set<Long> activitiesToResolveSet,
			Set<Request> impossibleRequestsSet) {

		List<Integer> perm = new ArrayList<>();
		List<List<Request>> requestListList = new ArrayList<>();
		
		for (long activity_id : activitiesToResolveSet) {
			if (requestsInit.get(activity_id) == null) {
				return null;
			}
			perm.add(requestsInit.get(activity_id).size());
			requestListList.add(requestsInit.get(activity_id));
		}
		
		List<int[]> permList = Algorithms.getAllPermutations(perm);
		
		boolean isValid = false;
		for (int[] permutation : permList) {
			boolean isGoodCombo = true;
			isValid = false;
			Set<Request> newImpossibleRequestsSet = new HashSet<>();
			Set<Long> newActivitiesToResolveSet = new HashSet<>();
			
			for (int i=0; i<permutation.length; i++) {
				Request r = requestListList.get(i).get(permutation[i]);
				if (impossibleRequestsSet.contains(r) || newImpossibleRequestsSet.contains(r)
						|| impossibleRequests.contains(r)) { // TODO ERROR IS HERE [&&] instead of [||]
					isGoodCombo = false;
				}
				
				grantedRequests.put(r.activity_id, r);
				groupsGrantRequest(r);
				newImpossibleRequestsSet.addAll(impossibleRequestsMap.get(r));
				newActivitiesToResolveSet.addAll(activitiesToResolve.get(r));
				
			}
			
			if (isGoodCombo) {
				isValid = true;
				newImpossibleRequestsSet.addAll(impossibleRequestsSet);
				Set<Long> newActivitiesToSet = new HashSet<>();
				for (long activity_id : newActivitiesToResolveSet) {
					if (grantedRequests.get(activity_id) == null) {
						newActivitiesToSet.add(activity_id);
					}
				}
				
				if (!newActivitiesToSet.isEmpty()) {
					Map<Long, Request> grantedRequests2 = 
							canRequestBeGrantedRecursive(grantedRequests, newActivitiesToSet, newImpossibleRequestsSet);
					if (grantedRequests2 != null && !grantedRequests2.isEmpty()) {
						grantedRequests = grantedRequests2;
					} else {
						isValid = false;
					}
				}
			} 
			
			if (isValid) {
				break;
			} else {
				for (int i=0; i<permutation.length; i++) {
					Request r = requestListList.get(i).get(permutation[i]);
					grantedRequests.remove(r.activity_id);
					groupsRemoveRequest(r); // TODO change to groupsRemoveRequest(r);
				}
			}
		}
		
		if (isValid)
			return grantedRequests;
		return null;
	}

	public Student(Student student, LimitsBoundsChecker limitsChecker) {
		this.limitsChecker = limitsChecker;
		this.student_id = student.student_id;
		this.initialGroups = student.initialGroups;
		setGroups(new HashSet<>(student.getGroups()));
		this.requestsInitial = student.requestsInitial;
		this.minmax_penalty = student.minmax_penalty;
		this.award_activity = student.award_activity;
		this.award_student = student.award_student;
		
		this.requestsInit = student.requestsInit;
		this.impossibleRequestsMap = student.impossibleRequestsMap;
		this.activitiesToResolve = student.activitiesToResolve;
		this.requestActivitiesList = student.requestActivitiesList;
		this.approvedRequests = new HashMap<>(student.approvedRequests);
		
		this.a = student.a;
		this.b = student.b;
		this.c = student.c;
	}

	public int rollbackRequests(Map<Long, GroupLimit> groupLimits) {
		if (approvedRequests.isEmpty()) {
			return 0;
		}
		
		a = b = c = 0;
		int count = approvedRequests.size();
		
		Map<Long, Request> requestsThatHaveToStayGranted = new HashMap<>();
		Set<Long> activitiesThatHaveToBeResolved = new HashSet<>();
		for (Request request : approvedRequests.values()) {
			
			//if (request == null || groupLimits == null || groupLimits.get(request.group_id) == null
			//		|| groupLimits.get(request.wanted_group_id) == null) {
			//	System.out.println("DELETE LATER!");
			//}
			
			if (groupLimits.get(request.group_id).isMax() || groupLimits.get(request.wanted_group_id).isMin()) {
				requestsThatHaveToStayGranted.put(request.activity_id, request);
				activitiesThatHaveToBeResolved.addAll(activitiesToResolve.get(request));
				a += request.swap_weight;
			}
		}
		
		if (requestsThatHaveToStayGranted.isEmpty()) {
			for (Request request : approvedRequests.values()) {
				limitsRemoveRequest(request, groupLimits);
				groupsRemoveRequest(request);
			}
			
			approvedRequests.clear();
		} else {
			while (!activitiesThatHaveToBeResolved.isEmpty()) {
				
				Set<Long> newActivitiesThatHaveToBeResolved = new HashSet<>();
				for (long activity_id : activitiesThatHaveToBeResolved) {
					if (requestsThatHaveToStayGranted.get(activity_id) != null)
						continue;
					
					Request request = approvedRequests.get(activity_id);
					requestsThatHaveToStayGranted.put(activity_id, request);
					
					//TODO Find out why this line causes null pointer exception, and delete later
					/*if (activitiesToResolve == null || request == null || activitiesToResolve.get(request) == null) {
						System.out.println(activity_id);
						System.out.println();
						System.out.println(request);
						System.out.println("   ...   ");
						System.out.println(Utils.printMap(activitiesToResolve));
						System.out.println("   ...   ");
						System.out.println(Utils.printApprovedRequests(approvedRequests));
						System.out.println("   ...   ");
					}*/
					
					newActivitiesThatHaveToBeResolved.addAll(activitiesToResolve.get(request));
					a += request.swap_weight;
				}
				
				activitiesThatHaveToBeResolved = newActivitiesThatHaveToBeResolved;
			}
			
			for (Request request : approvedRequests.values()) {
				if (requestsThatHaveToStayGranted.containsKey(request.activity_id))
					continue;
				limitsRemoveRequest(request, groupLimits);
				groupsRemoveRequest(request);
			}
			
			approvedRequests = requestsThatHaveToStayGranted;
			calculateBC();
		}
		
		return count - requestsThatHaveToStayGranted.size();
	}

	public boolean grantRequests(Random rand, Map<Long, GroupLimit> groupLimits) {
		boolean hasGranted = false;
		Collections.shuffle(requestActivitiesList, rand);
		
		for (long activity_id : requestActivitiesList) {
			if (approvedRequests.get(activity_id) != null) 
				continue;
			List<Request> requests = requestsInit.get(activity_id);
			Collections.shuffle(requests, rand);
			
			for (Request r : requests) {
				boolean gratned = grantRequest(r, groupLimits, rand);
				if (gratned) {
					hasGranted = true;
					break;
				}
			}
		}
		
		return hasGranted;
	}
	
	public boolean grantRequest(Request request, Map<Long, GroupLimit> groupLimits, Random rand) {
		
		// Secure that there is no approved request for the same student and activity
		if (approvedRequests.get(request.activity_id) != null) {
			return false;
		}
		
		// Secure that granting this request will still secure the hard constraints of group limits
		GroupLimit limit1 = groupLimits.get(request.group_id);
		GroupLimit limit2 = groupLimits.get(request.wanted_group_id);
		if (limit1.isMin() || limit2.isMax()) {
			return false;
		}
		
		groupsGrantRequest(request);
		
		Map<Long, Request> granted = new HashMap<>();
		granted.put(request.activity_id, request);
		
		List<Long> activitiesToResolveList = activitiesToResolve.get(request);
		if (checkForOverlapsWithGrantedRequests(request))
			granted = null;
		
		if (!activitiesToResolveList.isEmpty() && granted != null) {
			activitiesToResolveList = getActivitiesToResolveList(request);
			if (!activitiesToResolveList.isEmpty() ) {
				Set<Request> impossibleRequestsSet = getImpossibleRequestsSet(request);
				createRequests(groupLimits);
				granted = grantRequestsRecursive(granted, new HashSet<>(activitiesToResolveList), impossibleRequestsSet, rand);
			}
		} 
		
		if (granted == null || granted.isEmpty()) {
			groupsRemoveRequest(request);
			return false;
		}
		
		for (Request r : granted.values()) {
			approvedRequests.put(r.activity_id, r);
			
			a += r.swap_weight;
			limitsGrantRequest(r, groupLimits);
		}
		
		calculateBC();
		
		return true;
	}
	
	private Set<Request> getImpossibleRequestsSet(Request request) {
		Set<Request> impossibleRequests = new HashSet<>(impossibleRequestsMap.get(request));
		for (Request r : approvedRequests.values()) {
			impossibleRequests.addAll(impossibleRequestsMap.get(r));
		}
		return impossibleRequests;
	}

	private void calculateBC() {
		b = c = 0;
		int totalRequestsGranted = approvedRequests.size();
		int allRequests = requestsInitial.size();
		int index = totalRequestsGranted - 1;
		if (totalRequestsGranted >= award_activity.length) {
			b = award_activity[award_activity.length - 1];
		} else if (index >= 0) {
			b = award_activity[index];
		}
		if (totalRequestsGranted == allRequests) {
			c = award_student;
		}
	}

	private List<Long> getActivitiesToResolveList(Request request) {
		List<Long> activitiesToResolveList = new ArrayList<>();
		for (long activity_id : activitiesToResolve.get(request)) {
			if (approvedRequests.get(activity_id) == null) { // CORRECTION
				activitiesToResolveList.add(activity_id);
			}
		}
		return activitiesToResolveList;
	}

	private void createRequests(Map<Long, GroupLimit> groupLimits) {
		requests = new HashMap<>();
		for (long activity_id : requestsInit.keySet()) {
			List<Request> reqList = new ArrayList<>();
			for (Request r : requestsInit.get(activity_id)) {
				if (groupLimits.get(r.group_id).isMin()) {
					reqList = null;
					break;
				} else if (!groupLimits.get(r.wanted_group_id).isMax()) {
					reqList.add(r);
				}
			}
			
			if (reqList != null && !reqList.isEmpty()) {
				requests.put(activity_id, reqList);
			}
		}
	}

	private boolean checkForOverlapsWithGrantedRequests(Request request) {
		for (Request r : approvedRequests.values()) {
			if (impossibleRequestsMap.get(request).contains(r)) {
				return true;
			}
		}
		
		return false;
	}

	private Map<Long, Request> grantRequestsRecursive(Map<Long, Request> grantedRequests,
			Set<Long> activitiesToResolveSet, Set<Request> impossibleRequestsSet, Random rand) {
		
		List<Integer> perm = new ArrayList<>();
		List<List<Request>> requestListList = new ArrayList<>();
		
		for (long activity_id : activitiesToResolveSet) {
			if (requests.get(activity_id) == null) {
				return null;
			}
			perm.add(requests.get(activity_id).size());
			requestListList.add(requests.get(activity_id));
		}
		List<int[]> permList = Algorithms.getAllPermutations(perm);
		//if (permList == null) {
		//	System.out.println("DELETE LATER!");
		//}
		Collections.shuffle(permList, rand);
		
		boolean isValid = false;
		for (int[] permutation : permList) {
			boolean isGoodCombo = true;
			isValid = false;
			Set<Request> newImpossibleRequestsSet = new HashSet<>();
			Set<Long> newActivitiesToResolveSet = new HashSet<>();
			
			for (int i=0; i<permutation.length; i++) {
				Request r = requestListList.get(i).get(permutation[i]);
				if (impossibleRequestsSet.contains(r) || newImpossibleRequestsSet.contains(r)) { // TODO ERROR IS HERE [&&] instead of [||]
					isGoodCombo = false;
				}
				
				grantedRequests.put(r.activity_id, r);
				groupsGrantRequest(r);
				newImpossibleRequestsSet.addAll(impossibleRequestsMap.get(r));
				newActivitiesToResolveSet.addAll(activitiesToResolve.get(r));
				
			}
			
			if (isGoodCombo) {
				isValid = true;
				newImpossibleRequestsSet.addAll(impossibleRequestsSet);
				Set<Long> newActivitiesToSet = new HashSet<>();
				for (long activity_id : newActivitiesToResolveSet) {
					if (grantedRequests.get(activity_id) == null && approvedRequests.get(activity_id) == null) {
						newActivitiesToSet.add(activity_id);
					}
				}
				
				if (!newActivitiesToSet.isEmpty()) {
					Map<Long, Request> grantedRequests2 = 
							grantRequestsRecursive(grantedRequests, newActivitiesToSet, newImpossibleRequestsSet, rand);
					if (grantedRequests2 != null && !grantedRequests2.isEmpty()) {
						grantedRequests = grantedRequests2;
					} else {
						isValid = false;
					}
				}
			} 
			
			if (isValid) {
				break;
			} else {
				for (int i=0; i<permutation.length; i++) {
					Request r = requestListList.get(i).get(permutation[i]);
					grantedRequests.remove(r.activity_id);
					groupsRemoveRequest(r); // TODO change to groupsRollbackRequest(r);
					
				}
			}
		}
		
		if (isValid)
			return grantedRequests;
		
		return null;
	}
	
	public long getStudentId() {
		return student_id;
	}
	
	public int getA() {
		return a;
	}
	public int getB() {
		return b;
	}
	public int getC() {
		return c;
	}
	public Map<Long, Request> getApprovedRequests() {
		return approvedRequests;
	}

	public int getRequestsGranted() {
		return approvedRequests.size();
	}

	public Student copy(LimitsBoundsChecker limitsChecker) {
		Student stud = new Student(this, limitsChecker);
		return stud;
	}
	
	private void groupsRemoveRequest(Request r) {
		groups.remove(r.wanted_group_id);
		groups.add(r.group_id);
	}
	
	private void groupsGrantRequest(Request r) {
		groups.remove(r.group_id);
		groups.add(r.wanted_group_id);
	}
	
	private void limitsGrantRequest(Request r, Map<Long, GroupLimit> groupLimits) {
		groupLimits.get(r.group_id).decreaseValue(limitsChecker, minmax_penalty);
		groupLimits.get(r.wanted_group_id).increaseValue(limitsChecker, minmax_penalty);
	}
	
	private void limitsRemoveRequest(Request request, Map<Long, GroupLimit> groupLimits) {
		groupLimits.get(request.group_id).increaseValue(limitsChecker, minmax_penalty);
		groupLimits.get(request.wanted_group_id).decreaseValue(limitsChecker, minmax_penalty);
	}

	public Set<Long> getGroups() {
		return groups;
	}

	public void setGroups(Set<Long> groups) {
		this.groups = groups;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((approvedRequests == null) ? 0 : approvedRequests.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (approvedRequests == null) {
			if (other.approvedRequests != null)
				return false;
		} else if (!approvedRequests.equals(other.approvedRequests))
			return false;
		return true;
	}
	
	public Set<Request> getImpossibleRequests() {
		return impossibleRequests;
	}
	
	public Map<Long, List<Request>> getRequestsInitial() {
		return requestsInitial;
	}
	
	public void forceGrantRequest(Request r, Map<Long, GroupLimit> groupLimits) {
		if (r == null || r.student_id != student_id) 
			return;
		if (approvedRequests.containsKey(r.activity_id) && approvedRequests.get(r.activity_id).equals(r)) 
			return;
		
		if (approvedRequests.containsKey(r.activity_id)) {
			Request oldR = approvedRequests.get(r.activity_id);
			a -= oldR.swap_weight;
			
			limitsRemoveRequest(oldR, groupLimits);
			groupsRemoveRequest(oldR);
		}
		
		a += r.swap_weight;
		approvedRequests.put(r.activity_id, r);
		limitsGrantRequest(r, groupLimits);
		groupsGrantRequest(r);
		
		calculateBC();
	}

	public void forceApprove(Map<Long, Request> forcedRequests, Map<Long, GroupLimit> groupLimits) {
		a = b = c = 0;
		if (forcedRequests == null) 
			forcedRequests = new HashMap<>();
		
		for (Request r : approvedRequests.values()) {
			limitsRemoveRequest(r, groupLimits);
			groupsRemoveRequest(r);
		}
		
		for (Request r : forcedRequests.values()) {
			limitsGrantRequest(r, groupLimits);
			groupsGrantRequest(r);
			a += r.swap_weight;
		}
		
		approvedRequests = forcedRequests;
		calculateBC();
	}

	public boolean removeInvalidRequests(Map<Long, GroupLimit> groupLimits, Map<Long, Set<Long>> overlapMap) {
		boolean hasConflicts = false;
		
		List<Request> toRemove = null;
		for (Request r : approvedRequests.values()) {
			if ((isRequestOverLimits(r, groupLimits) || doesRequestCreateOverlaps(r, overlapMap))) {
				if (toRemove == null) {
					toRemove = new ArrayList<>();
				}
				hasConflicts = true;
				toRemove.add(r);
				removeRequestSoft(r, groupLimits);
			}
		}
		
		removeRequestsFromApproved(toRemove);
		
		calculateA();
		calculateBC();
		
		return hasConflicts;
	}
	
	private void calculateA() {
		a = 0;
		for (Request r : approvedRequests.values()) {
			a += r.swap_weight;
		}
	}

	private void removeRequestsFromApproved(List<Request> toRemove) {
		if (toRemove == null)
			return;
		for (Request request : toRemove) {
			approvedRequests.remove(request.activity_id);
		}
	}
	
	private void removeRequestSoft(Request request, Map<Long, GroupLimit> groupLimits) {
		limitsRemoveRequest(request, groupLimits);
		groupsRemoveRequest(request);
	}

	private boolean doesRequestCreateOverlaps(Request request, Map<Long, Set<Long>> overlapMap) {
		Set<Long> overlapsWithRequest = overlapMap.get(request.wanted_group_id);
		Set<Long> overlapsWithGroups = Algorithms.intersection(overlapsWithRequest, groups);
		overlapsWithGroups.remove(request.wanted_group_id);
		
		return !overlapsWithGroups.isEmpty();
	}

	private boolean isRequestOverLimits(Request request, Map<Long, GroupLimit> groupLimits) {
		GroupLimit limit1 = groupLimits.get(request.group_id);
		GroupLimit limit2 = groupLimits.get(request.wanted_group_id);
		
		if (limit1.isUnderMin() || limit2.isOverMax()) {
			return true;
		}
		
		return false;
	}
 }
