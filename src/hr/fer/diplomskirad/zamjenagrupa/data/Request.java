package hr.fer.diplomskirad.zamjenagrupa.data;

/**
 * This class represents one request from requests-file:
 * <pre>
 * student_id: id of student
 * activity_id: id for student activity (for instance Linear Algebra course)
 * group_id: id for gruop (for instance Linear Algebra course every Monday at 14:00)
 * swap_weight: points gained for granting this request
 * wanted_group_id: new group that this student wants to transfer
 * </pre>
 * 
 * @author Josip Kasap
 *
 */
public class Request implements Comparable<Request> {
	public final long student_id;
	public final long activity_id;
	public final long group_id;
	public final long swap_weight;
	public final long wanted_group_id;
	
	public Request(long student_id, long activity_id, long group_id, long swap_weight, long wanted_group_id) {
		super();
		this.student_id = student_id;
		this.activity_id = activity_id;
		this.group_id = group_id;
		this.swap_weight = swap_weight;
		this.wanted_group_id = wanted_group_id;
	}
	
	public Request(StudentRecord studentRecord, long wantedGroup) {
		super();
		this.student_id = studentRecord.student_id;
		this.activity_id = studentRecord.activity_id;
		this.group_id = studentRecord.group_id;
		this.swap_weight = studentRecord.swap_weight;
		this.wanted_group_id = wantedGroup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (activity_id ^ (activity_id >>> 32));
		result = prime * result + (int) (group_id ^ (group_id >>> 32));
		result = prime * result + (int) (student_id ^ (student_id >>> 32));
		result = prime * result + (int) (swap_weight ^ (swap_weight >>> 32));
		result = prime * result + (int) (wanted_group_id ^ (wanted_group_id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (activity_id != other.activity_id)
			return false;
		if (group_id != other.group_id)
			return false;
		if (student_id != other.student_id)
			return false;
		if (swap_weight != other.swap_weight)
			return false;
		if (wanted_group_id != other.wanted_group_id)
			return false;
		return true;
	}

	@Override
	public int compareTo(Request other) {
		int weightDifference = (int)(this.swap_weight - other.swap_weight);
		if (weightDifference != 0)
			return weightDifference;
		
		int studentDifference = (int)(this.student_id - other.student_id);
		if (studentDifference != 0)
			return studentDifference;
		
		int activitytDifference = (int)(this.activity_id - other.activity_id);
		if (activitytDifference != 0)
			return activitytDifference;
		
		int wantedGroupDifference = (int)(this.wanted_group_id - other.wanted_group_id);
		if (wantedGroupDifference != 0)
			return wantedGroupDifference;
		
		return 0;
	}
	
	@Override
	public String toString() {
		return String.format("(%d,%d,%d,%d,%d)", student_id, activity_id, swap_weight, group_id, wanted_group_id);
	}
}
