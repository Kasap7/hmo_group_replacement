package hr.fer.diplomskirad.zamjenagrupa.data;

import java.util.Map;

public class LimitsBoundsChecker {
	public int d = 0;
	public int e = 0;
	
	public LimitsBoundsChecker(Map<Long, GroupLimit> groupLimits, int minmax_penalty) {
		for (GroupLimit limit : groupLimits.values()) {
			d += limit.getD(minmax_penalty);
			e += limit.getE(minmax_penalty);
		}
	}
	
	public void updateD(int value) {
		d += value;
	}
	
	public void updateE(int value) {
		e += value;
	}
	
	public int getE() {
		return e;
	}
	
	public int getD() {
		return d;
	}
	
	public int getDE() {
		return d+e;
	}
	
	public void updateChecker(Map<Long, GroupLimit> groupLimits, int minmax_penalty) {
		d = e = 0;
		for (GroupLimit limit : groupLimits.values()) {
			d += limit.getD(minmax_penalty);
			e += limit.getE(minmax_penalty);
		}
	}
}
