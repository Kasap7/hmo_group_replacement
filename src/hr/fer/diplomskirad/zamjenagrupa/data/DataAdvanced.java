package hr.fer.diplomskirad.zamjenagrupa.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * This class represents the state of the system. It holds all necessary information for this problem.
 * 
 * @author Josip Kasap
 *
 */
public class DataAdvanced {
	
	public int[] award_activity;
	public int award_student;
	public int minmax_penalty;
	public int initialGoodness;
	
	public int studentsCount, activityCount, requestsCount;
	
	/** Maps student -> all groups that student attends; so that the groups student attends can't be overlapped */
	public Map<Long, Set<Long>> studentGroups = new HashMap<>();
	
	/** Limits the students in the group for each group */
	public Map<Long, GroupLimit> groupLimits = new HashMap<>();
	
	/** Maps group -> set of group; for each group, set of group that group overlaps with */
	public Map<Long, Set<Long>> overlapMap = new HashMap<>();
	
	/** Maps student & activity -> list of all requests */
	public Map<Long, Map<Long, List<Request>>> requestsMap = new HashMap<>();
	
	/** Maps student & activity -> list of possible requests */
	public Map<Long, Map<Long, List<Request>>> requestsMapPossible = new HashMap<>();
	
	/** Maps group -> all requests that want to go to this group */
	public Map<Long, Long> groupToActivityMap = new HashMap<>();
	
	/** List of all requests */
	public List<Request> requestList = new ArrayList<>();
	
	/** Set of all requests */
	public Set<Request> requestSet;
	
	public Map<Request, Integer> requestToListMapping;
	
	public Map<Long, List<Request>> inputGroupRequests = new HashMap<>();
	public Map<Long, List<Request>> outputGroupRequests = new HashMap<>();
	public List<Request> initiallyGrantedRequests = new ArrayList<>();
	public Set<Long> groups = new HashSet<>();
	public List<Long> students;
	
	public DataAdvanced(String students, String requests, String overlaps, String limits,
			int[] award_activity, int award_student, int minmax_penalty) throws IOException {
		
		this.award_activity = award_activity;
		this.award_student = award_student;
		this.minmax_penalty = minmax_penalty;
		
		initializeParameters(students, requests, overlaps, limits);
		optimize();
		
		requestSet = new HashSet<>(requestList);
		
		getPossibleRequestsMap();
		getRequestToListMapping();
		getStudentsWithRequest();
	}

	private void getStudentsWithRequest() {
		students = new ArrayList<>(requestsMapPossible.keySet());
	}

	private void getPossibleRequestsMap() {
		for (Request r : requestList) {
			Map<Long, List<Request>> requestsForStudent = null;
			if (!requestsMapPossible.containsKey(r.student_id)) {
				requestsForStudent = new HashMap<>();
				requestsMapPossible.put(r.student_id, requestsForStudent);
			} else {
				requestsForStudent = requestsMapPossible.get(r.student_id);
			}
			
			List<Request> requestsForActivity = null;
			
			if (!requestsForStudent.containsKey(r.activity_id)) {
				requestsForActivity = new ArrayList<>();
				requestsForStudent.put(r.activity_id, requestsForActivity);
			} else {
				requestsForActivity = requestsForStudent.get(r.activity_id);
			}
			
			requestsForActivity.add(r);
		}
	}

	private void initializeParameters(String students, String requests, String overlaps, String limits) throws IOException {
		List<String> studentsLines = Files.readAllLines(Paths.get(students));
		List<String> requestsLines = Files.readAllLines(Paths.get(requests));
		List<String> overlapsLines = Files.readAllLines(Paths.get(overlaps));
		List<String> limitsLines = Files.readAllLines(Paths.get(limits));
		
		// Prijelazna mapa za request od studenata
		Map<Long, Map<Long, StudentRecord>> studentMap = new HashMap<>();
		
		Set<Long> studentsSet = new HashSet<>();
		Set<Long> activitySet = new HashSet<>();
		
		// Ucitavanje svih grupa i njihovih limita 
		for (int i=1; i<limitsLines.size(); i++) {
			String line = limitsLines.get(i);
			String[] arr = line.split(",");
			long group_id = Long.parseLong(arr[0]);
			
			groupLimits.put(group_id, new GroupLimit(arr));
		}
		
		// Ucitavanje students mape (inicijalno stanje studenata i njihovih grupa)
		for (int i=1; i<studentsLines.size(); i++) {
			String line = studentsLines.get(i);
			String[] arr = line.split(",");
			long student_id = Long.parseLong(arr[0]);
			long activity_id = Long.parseLong(arr[1]);
			//long swap_weight = Long.parseLong(arr[2]);
			long group_id = Long.parseLong(arr[3]);
			
			if (!groupLimits.containsKey(group_id))
				continue;
			
			Set<Long> studentSet = studentGroups.get(student_id);
			if (studentSet == null) {
				studentSet = new HashSet<>();
				studentGroups.put(student_id, studentSet);
			}
			studentSet.add(group_id);
			
			groups.add(group_id);
			
			Map<Long, StudentRecord> recordForActivity = studentMap.get(student_id);
			if (recordForActivity == null) {
				recordForActivity = new HashMap<>();
				studentMap.put(student_id, recordForActivity);
			}
			
			recordForActivity.put(activity_id, new StudentRecord(arr));
			
			if (!arr[4].equals("0")) {
				long wanted_group_id = Long.parseLong(arr[4]);
				long swap_weight = Long.parseLong(arr[2]);
				Request request = new Request(student_id, activity_id, group_id, swap_weight, wanted_group_id);
				initiallyGrantedRequests.add(request);
			}
			
			groupToActivityMap.put(group_id, activity_id);
			
			// This is strictly for statistical purposes
			studentsSet.add(student_id);
			activitySet.add(activity_id);
		}
		
		for (int i=1; i<requestsLines.size(); i++) {
			String line = requestsLines.get(i);
			String[] arr = line.split(",");
			long student_id = Long.parseLong(arr[0]);
			long activity_id = Long.parseLong(arr[1]);
			long new_group_id = Long.parseLong(arr[2]);
			
			if (!groupLimits.containsKey(new_group_id) || studentMap.get(student_id) == null ||
					studentMap.get(student_id).get(activity_id) == null)
				continue;
			
			groups.add(new_group_id);
			
			StudentRecord studentRecord = studentMap.get(student_id).get(activity_id);
			if (studentRecord == null)
				continue;
			
			Map<Long, List<Request>> requestsForActivity = requestsMap.get(student_id);
			if (requestsForActivity == null) {
				requestsForActivity = new HashMap<>();
				requestsMap.put(student_id, requestsForActivity);
			}
			//requestsForActivity.put(activity_id, new ArrayList<>()); STARO
			if (requestsForActivity.get(activity_id) == null) { 			// NOVO
				requestsForActivity.put(activity_id, new ArrayList<>());	// NOVO
			}																// NOVO
			
			List<Request> requests_ = requestsMap.get(student_id).get(activity_id);
			Request request = new Request(studentRecord, new_group_id);
			
			requests_.add(request);
			requestList.add(request);
			
			// Added new functionality, all of the requests are sorted by their initial group and their wanted group
			List<Request> outgoingRequests = inputGroupRequests.get(studentRecord.group_id);
			if (outgoingRequests == null) {
				outgoingRequests = new ArrayList<>();
				inputGroupRequests.put(studentRecord.group_id, outgoingRequests);
			}
			outgoingRequests.add(request);
			
			List<Request> ingoingRequests = outputGroupRequests.get(new_group_id);
			if (ingoingRequests == null) {
				ingoingRequests = new ArrayList<>();
				outputGroupRequests.put(new_group_id, ingoingRequests);
			}
			ingoingRequests.add(request);
			
			activitySet.add(activity_id);
			
			groupToActivityMap.put(new_group_id, activity_id);
		}
		
		for (int i=1; i<overlapsLines.size(); i++) {
			String line = overlapsLines.get(i);
			String[] arr = line.split(",");
			long group_id1 = Long.parseLong(arr[0]);
			long group_id2 = Long.parseLong(arr[1]);
			
			if (! (groups.contains(group_id1) && groups.contains(group_id2))) {
				continue;
			}
			
			Set<Long> overlappedGroups1 = overlapMap.get(group_id1);
			if (overlappedGroups1 == null) {
				overlappedGroups1 = new HashSet<>();
				overlapMap.put(group_id1, overlappedGroups1);
			}
			overlappedGroups1.add(group_id2);
			
			Set<Long> overlappedGroups2 = overlapMap.get(group_id2);
			if (overlappedGroups2 == null) {
				overlappedGroups2 = new HashSet<>();
				overlapMap.put(group_id2, overlappedGroups2);
			}
			overlappedGroups2.add(group_id1);
		}
		
		for (long group_id : groups) {
			if (overlapMap.containsKey(group_id) == false) {
				overlapMap.put(group_id, new HashSet<>());
			}
		}
		
		requestList.sort(new Comparator<Request>() {
			@Override
			public int compare(Request r1, Request r2) {
				return -r1.compareTo(r2);
			}
		});
		
		requestsCount = requestList.size();
		studentsCount = studentsSet.size();
		activityCount = activitySet.size();
		
		getInitialGoodness();
	}

	private void getInitialGoodness() {
		initialGoodness = 0;
		for (GroupLimit limit : groupLimits.values()) {
			initialGoodness -= limit.getDE(minmax_penalty);
		}
	}
	
	/**
	 * This method optimizes requests by deleting all of the impossible requests. This is the list of all
	 * impossible requests:
	 * <br>
	 * 1. Student has a request for a full group, but nobody wants to get out of that group.
	 * <br>
	 * 2. Student has a request from a group with minimal student count, but there is no request to get into that group.
	 * <br>
	 * 3. Student has a request that would create an overlap, but student does not have any other request that would resolve
	 * 		that overlap.
	 */
	public void optimize() {
		System.out.println();
		System.out.printf(" Number of students   = %d\n Number of requests   = %d\n\n", studentsCount, requestsCount);
		System.out.printf(" Avarage number of activities per student = %.3f\n", (1.0*activityCount) / studentsCount);
		System.out.printf(" Avarage number of requests per student = %.3f\n", (1.0*requestsCount) / studentsCount);
		System.out.println();
		
		//##############################################################################
									/* IMPOSIBLE REQUESTS */
		//##############################################################################
		List<Request> imposibleRequests1 = new ArrayList<>();
		List<Request> imposibleRequests2 = new ArrayList<>();
		List<Request> imposibleRequests3;
		for (Request r : requestList) {
			
			// 1) Requests for full groups
			if (groupLimits.get(r.wanted_group_id).isMax()) {
				List<Request> outgroupRequests = inputGroupRequests.get(r.wanted_group_id);
				if (outgroupRequests == null || outgroupRequests.isEmpty()) {
					imposibleRequests1.add(r);
					//System.out.printf("  1.) %s\n", r);
				}
			}
			
			// 2) Requests from groups with minimal student count
			if (groupLimits.get(r.group_id).isMin()) {
				//System.out.printf("  #2.) %s\n", r);
				List<Request> ingroupRequests = outputGroupRequests.get(r.group_id);
				if (ingroupRequests == null || ingroupRequests.isEmpty()) {
					imposibleRequests2.add(r);
					//System.out.printf("  2.) %s\n", r);
				}
			}
		}
		
		Set<Request> impossibleRequests = new HashSet<>(imposibleRequests1);
		impossibleRequests.addAll(imposibleRequests2);
		
		// 3) Requests that create unsolvable conflicts
		imposibleRequests3 = getImpossibleOverlapsRequests(impossibleRequests);
		
		int impossible1 = imposibleRequests1.size();
		int impossible2 = imposibleRequests2.size();
		int impossible3 = imposibleRequests3.size() - impossibleRequests.size();
		
		System.out.println(" All of the impossible requests have been found.");
		System.out.printf(" Total number of impossible requests = %d\n", impossible1 + impossible2 + impossible3);
		System.out.printf("  1.) Requests for full groups = %d\n", impossible1);
		System.out.printf("  2.) Requests from groups with minimal student count = %d\n", impossible2);
		System.out.printf("  3.) Requests that create unsolvable conflicts (overlaps) = %d\n", impossible3);
		
		for (Request r : imposibleRequests1)
			requestList.remove(r);
		for (Request r : imposibleRequests2)
			requestList.remove(r);
		for (Request r : imposibleRequests3) {
			requestList.remove(r);
			//System.out.println(r);
		}
		
		System.out.printf(" Total number of leftover possible requests = %d\n", requestList.size());
		System.out.println();
	}

	private List<Request> getImpossibleOverlapsRequests(Set<Request> impossibleRequests) {
		List<Request> impossibleOverlapsRequests = new ArrayList<>();
		
		Set<Request> requestsSet = new HashSet<>(requestList);
		requestsSet.removeAll(impossibleRequests);
		
		for (long student_id : requestsMap.keySet()) {
			Map<Long, List<Request>> requestsForStudent = requestsMap.get(student_id);
			Set<Long> initialStudentGroups = studentGroups.get(student_id);
			
			Student student = new Student(student_id, initialStudentGroups, requestsForStudent, requestsSet,
					overlapMap, minmax_penalty, award_activity, award_student, null);
			
			impossibleOverlapsRequests.addAll(student.getImpossibleRequests());
		}
		
		return impossibleOverlapsRequests;
	}
	
	public void getRequestToListMapping() {
		requestToListMapping = new HashMap<>();
		for (Map<Long, List<Request>> studentRequests : requestsMapPossible.values()) {
			for (List<Request> requests : studentRequests.values()) {
				for (int i=0; i<requests.size(); i++) {
					requestToListMapping.put(requests.get(i), i);
				}
			}
		}
	}
}