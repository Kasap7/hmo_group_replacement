package hr.fer.diplomskirad.zamjenagrupa.data;

/**
 * This class represents one request from student-file:
 * <pre>
 * student_id: id of student
 * activity_id: id for student activity (for instance Linear Algebra course)
 * group_id: id for gruop (for instance Linear Algebra course every Monday at 14:00)
 * swap_weight: points gained for granting this request
 * </pre>
 * 
 * @author Josip Kasap
 *
 */
public class StudentRecord {
	public final long student_id;
	public final long activity_id;
	public final long group_id;
	public final long swap_weight;
	
	public StudentRecord(String[] arr) {
		student_id = Long.parseLong(arr[0]);
		activity_id = Long.parseLong(arr[1]);
		swap_weight = Long.parseLong(arr[2]);
		group_id = Long.parseLong(arr[3]);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (activity_id ^ (activity_id >>> 32));
		result = prime * result + (int) (group_id ^ (group_id >>> 32));
		result = prime * result + (int) (student_id ^ (student_id >>> 32));
		result = prime * result + (int) (swap_weight ^ (swap_weight >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentRecord other = (StudentRecord) obj;
		if (activity_id != other.activity_id)
			return false;
		if (group_id != other.group_id)
			return false;
		if (student_id != other.student_id)
			return false;
		if (swap_weight != other.swap_weight)
			return false;
		return true;
	}
	
}
