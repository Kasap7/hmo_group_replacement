package hr.fer.diplomskirad.zamjenagrupa;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.text.SimpleAttributeSet;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.EliminationGa;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.Ga;
import hr.fer.diplomskirad.zamjenagrupa.data.Data;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.SimpleTimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

/**
 * This program solves optimization problem defined in file: Upute_zamjena_grupa.pdf.
 * In this optimization problem students want to transfer groups and they created a bundle of requests.
 * Without breaking hard constraints defined in the Upute_zamjena_grupa.pdf you have to grant as many
 * of the requests as possible, more specifically you have to gain the best possible score for granting those requests.
 * This program uses genetic algorithm to find the best solution: the best possible requests that have to be granted 
 * from the bundle to ensure the highest score.
 * @author Josip Kasap
 *
 */
public class Demo {
	
	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static ITimeProvider timeProvider;
	
	// Environment of this program
	static Data data;
	
	public static void main(String[] args) throws IOException {
		startTime = System.currentTimeMillis();
		initialiseParameters(args);
		timeProvider = new SimpleTimeProvider(timeout);
		
		printStatment("Preprocessing started on directory: " + dir + ".");
		
		preprocessing();
		
		printStatment("Preprocessing done!");
		
		solveWithGA();
		printStatment("GA is done!");
		
	}
	
	private static void solveWithGA() throws IOException {
		int popSize = 500;
		int maxEval = 10000000;
		double pm = 0.02;
		String fileOut = dir + File.separatorChar + "out.csv";
		
		//Ga ga = new Ga(data, popSize, maxEval, pm);
		EliminationGa ga = new EliminationGa(data, timeProvider, popSize, maxEval, pm);
		ga.doYourJob();
		
		ga.getBest().writeToFile(students, fileOut);
	}

	
	private static void printStatment(String s) {
		double timePassed = 1.0*(System.currentTimeMillis() - startTime) / 1000.0;
		s += "\tTime passed = %.3f seconds\n";
		System.out.printf(s, timePassed);
	}
	
	/**
	 * Initializes program parameters from command line arguments:<p>
	 * <pre>
	 * "-timeout": how long will the program run (in seconds)
	 * "-award-activity": award points for each of student request for activity that is granted
	 * "-award-student": award points for student that has all of his requests for activity granted
	 * "-minmax-penalty": penalty for exceeding the prefered count of students in classroom
	 * "-students-file": position of students-file
	 * "-requests-file": position of requests-file
	 * "-overlaps-file": position of overlaps-file
	 * "-limits-file": position of limits-file
	 * </pre>
	 * @param args command line arguments
	 */
	private static void initialiseParameters(String[] args) {
		for (int i=0; i<args.length; i++) {
			if (args[i].equals("-timeout") && args.length > i+1) {
				timeout = Utils.parseInteger(args[i+1], "-timeout")*1000;
			} else if (args[i].equals("-award-activity") && args.length > i+1) { 
				String[] arr = args[i+1].substring(0, args[i+1].length()).split(",");
				award_activity = new int[arr.length];
				for (int k=0; k < arr.length; k++) {
					award_activity[k] = Utils.parseInteger(args[i+1], "-award-activity");
				}
			} else if (args[i].equals("-award-student") && args.length > i+1) { 
				award_student = Utils.parseInteger(args[i+1], "-award-student");
			} else if (args[i].equals("-minmax-penalty") && args.length > i+1) { 
				minmax_penalty = Utils.parseInteger(args[i+1], "-minmax-penalty");
			} else if (args[i].equals("-dir") && args.length > i+1) { 
				dir = args[i+1];
			}
		}
		
		timeout += startTime;
		students = dir + File.separatorChar + "students.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
		
		Utils.checkExists(students, dir, "students.csv");
		Utils.checkExists(requests, dir, "requests.csv");
		Utils.checkExists(limits, dir, "limits.csv");
		Utils.checkExists(overlaps, dir, "overlaps.csv");
	}

	private static void preprocessing() throws IOException {
		data = new Data(students, requests, overlaps, limits, timeout, startTime, award_activity, award_student, minmax_penalty);
		data.optimize();
	}

}
