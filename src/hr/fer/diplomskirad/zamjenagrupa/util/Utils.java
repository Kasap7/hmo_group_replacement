package hr.fer.diplomskirad.zamjenagrupa.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.Algorithms;
import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.data.Student;

public class Utils {
	
	public static class Pair<T1, T2> {
		T1 first;
		T2 second;
		private Pair(T1 first, T2 second) {
			this.first = first;
			this.second = second;
		}
		
		public static <T1, T2> Pair<T1, T2> of(T1 first, T2 second) {
			return new Pair<T1, T2>(first, second);
		}
		
		public T1 first() {
			return first;
		}
		
		public T2 second() {
			return second;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((first == null) ? 0 : first.hashCode());
			result = prime * result + ((second == null) ? 0 : second.hashCode());
			return result;
		}

		@SuppressWarnings("rawtypes")
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pair other = (Pair) obj;
			if (first == null) {
				if (other.first != null)
					return false;
			} else if (!first.equals(other.first))
				return false;
			if (second == null) {
				if (other.second != null)
					return false;
			} else if (!second.equals(other.second))
				return false;
			return true;
		}
	}

	public static void checkExists(String path, String directory, String file) {
		if (Files.notExists(Paths.get(path))) {
			System.out.println("Illegal Arguments: Given path: " + directory + ", does not contain file: " + file);
			System.exit(0);
		}
	}
	
	public static int parseInteger(String number, String argument) {
		try {
			return Integer.parseInt(number);
		} catch (Exception e) {
			System.out.println("Illegal arguments, " + argument + " was expected to get integer value, but: " + number + " was provided.");
			System.exit(0);
			
			return 0;
		}
	}
	
	public static <A, B> boolean checkIfEmpty(Map<A, List<B>> map) {
		if (map.isEmpty())
			return true;
		
		for (List<B> list : map.values()) {
			if (!list.isEmpty()) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkForNullValues(List<Student> studentList) {
		boolean ret = false;
		for (Student student : studentList) {
			for (Request r : student.getApprovedRequests().values()) {
				if (r == null) {
					System.out.println("Null value found for student: " + student.getStudentId() + ".");
					ret = true;
				}
			}
		}
		
		return ret;
	}
	
	public static boolean checkForNullValues(Map<Long, Request> approvedRequests) {
		for (Request r : approvedRequests.values()) {
			if (r == null)
				return true;
		}
		
		return false;
	}
	
	public static void writeToFile(String fileIn, String fileOut, Map<Long, Map<Long, Request>> approvedRequests) throws IOException {
		List<String> studentsLines = Files.readAllLines(Paths.get(fileIn));
		List<String> newStudentsLines = new ArrayList<>();
		newStudentsLines.add(studentsLines.get(0));
		
		for (int i=1; i<studentsLines.size(); i++) {
			String line = studentsLines.get(i);
			String[] arr = line.split(",");
			long student_id = Long.parseLong(arr[0]);
			long activity_id = Long.parseLong(arr[1]);
			
			Request grantedRequest = null;
			Map<Long, Request> activityForRequest = approvedRequests.get(student_id);
			if (activityForRequest != null) {
				grantedRequest = activityForRequest.get(activity_id);
			}
			
			String s = line;
			if (grantedRequest != null) {
				arr[4] = "" + grantedRequest.wanted_group_id;
				s = String.join(",", arr);
			} 
			
			newStudentsLines.add(s);
		}
		
		Files.write(Paths.get(fileOut), newStudentsLines);
	}
	
	public static void writeToFile(String fileIn, String fileOut, List<Student> studentsList) throws IOException {
		Map<Long, Map<Long, Request>> approvedRequests = new HashMap<>();
		for (Student s : studentsList) {
			approvedRequests.put(s.getStudentId(), s.getApprovedRequests());
		}
		
		writeToFile(fileIn, fileOut, approvedRequests);
	}
	
	public static void printGoodness(int[] goodnessParameters) {
		int a = goodnessParameters[0];
		int b = goodnessParameters[1];
		int c = goodnessParameters[2];
		int d = goodnessParameters[3];
		int e = goodnessParameters[4];
		int initialGoodness = goodnessParameters[5];
		int goodness = (a + b + c - d - e) - initialGoodness;
		System.out.printf("Goodness = %d |\t %d = %d + %d + %d - %d - %d\n",
				goodness, (a + b + c - d - e), a, b, c, d, e);
	}
	
	public static void printGoodnessReduced(int[] goodnessParameters) {
		int a = goodnessParameters[0];
		int b = goodnessParameters[1];
		int c = goodnessParameters[2];
		int de = goodnessParameters[3];
		int initialGoodness = goodnessParameters[4];
		int goodness = (a + b + c - de) - initialGoodness;
		System.out.printf("Goodness = %d |\t %d = %d + %d + %d - %d\n",
				goodness, (a + b + c - de), a, b, c, de);
	}
	
	public static void printStudent(DataAdvanced data, long student_id) {
		Set<Long> groups = data.studentGroups.get(student_id);
		Map<Long, List<Request>> requestMap = data.requestsMap.get(student_id);
		Set<Request> requestSet = new HashSet<>(data.requestList);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		System.out.println("Student_id: " + student_id);
		
		Set<Long> groupsWithRequests = new HashSet<>();
		Set<Long> groupsWithRequestsImpossible = new HashSet<>();
		Set<Long> wantedGroups = new HashSet<>();
		Set<Long> wantedGroupsImpossible = new HashSet<>();
		
		for (List<Request> requests : requestMap.values()) {
			for (Request r : requests) {
				groupsWithRequests.add(r.group_id);
				wantedGroups.add(r.wanted_group_id);
				if (!requestSet.contains(r)) {
					groupsWithRequestsImpossible.add(r.group_id);
					wantedGroupsImpossible.add(r.wanted_group_id);
				}
			}
		}
		
		Set<Long> groupsWithoutRequests = new HashSet<>(groups);
		groupsWithoutRequests.removeAll(groupsWithRequests);
		
		System.out.println();
		System.out.println("  Groups: " + Algorithms.getSetAsString(groups));
		System.out.println("  Groups with requests: " + Algorithms.getSetAsString(groupsWithRequests));
		System.out.println("  Groups with impossible requests: " + Algorithms.getSetAsString(groupsWithRequestsImpossible));
		
		System.out.println();
		System.out.println("  Requests: ");
		System.out.println();
		
		for (long activity_id : requestMap.keySet()) {
			System.out.println("  " + activity_id + ":");
			
			for (Request r : requestMap.get(activity_id)) {
				if (!groupsWithRequestsImpossible.contains(r.group_id)) {
					System.out.println(r.group_id + " -> " + r.wanted_group_id);
				} else {
					System.out.println(r.group_id + " -> " + r.wanted_group_id +"  -  IMPOSSIBLE");
				}
			}
		}
		
		
		System.out.println();
		
		for (long activity_id : requestMap.keySet()) {
			for (Request r : requestMap.get(activity_id)) {
				Set<Long> overlapsWithWantedGroup = overlapMap.get(r.wanted_group_id);
				overlapsWithWantedGroup.remove(r.group_id);
				overlapsWithWantedGroup.remove(r.wanted_group_id);
				Set<Long> overlapsGroupsWithRequests = Algorithms.intersection(overlapsWithWantedGroup, groupsWithRequests);
				Set<Long> overlapsWantedGroups = Algorithms.intersection(overlapsWithWantedGroup, wantedGroups);
				Set<Long> overlapsGroupsWithoutRequests = Algorithms.intersection(overlapsWithWantedGroup, groupsWithoutRequests);
				
				System.out.println("    " + r.wanted_group_id +":   " + Algorithms.getSetAsString(overlapsGroupsWithRequests) + " - "
						+ Algorithms.getSetAsString(overlapsWantedGroups)  + " \t\t- " + Algorithms.getSetAsString(overlapsGroupsWithoutRequests));
			}
		}
	}
	
	public static void printHeader() {
		System.out.printf("\n ITERATION | GOODNESS | REQ |      A |     B |     C |     D |     E |    SUM | Time Passed\n");
		System.out.printf(  " --------------------------------------------------------------------------------------------------\n");
	}
	
	public static void printHeaderSA() {
		System.out.printf("\n ITERATION | GOODNESS | REQ |      A |     B |     C |     D |     E |    SUM |   Temp.  | Time Passed\n");
		System.out.printf(  " ------------------------------------------------------------------------------------------------------------\n");
	}
	
	public static void printHeaderReduced() {
		System.out.printf("\n ITERATION | GOODNESS | REQ |      A |     B |     C |    DE |    SUM | Time Passed\n");
		System.out.printf(  " ------------------------------------------------------------------------------------------\n");
	}
	
	public static void printIteration(int[] goodnessParameters, int t, ITimeProvider timeprovider, int requestsGranted) {
		int a = goodnessParameters[0];
		int b = goodnessParameters[1];
		int c = goodnessParameters[2];
		int d = goodnessParameters[3];
		int e = goodnessParameters[4];
		int initialGoodness = goodnessParameters[5];
		int goodness = a + b + c - d - e;
		
		System.out.printf(" [%7d]: %7d %7d | %7d %7d %7d %7d %7d %7d | %s \n", 
				t, goodness - initialGoodness, requestsGranted,
				a, b, c, d, e, goodness, timeprovider.stringTimePassed());
	}
	
	public static void printIterationReduced(int[] goodnessParameters, int t, ITimeProvider timeprovider, int requestsGranted) {
		int a = goodnessParameters[0];
		int b = goodnessParameters[1];
		int c = goodnessParameters[2];
		int de = goodnessParameters[3];
		int initialGoodness = goodnessParameters[4];
		int goodness = a + b + c - de;
		System.out.printf(" [%7d]: %7d %7d | %7d %7d %7d %7d %7d | %s \n", 
				t, goodness - initialGoodness, requestsGranted,
				a, b, c, de, goodness, timeprovider.stringTimePassed());
	}

	public static boolean hasOverlaps(Set<Long> groups, Map<Long, Set<Long>> overlapMap) {
		for (long group_id : groups) {
			Set<Long> overlapSet = overlapMap.get(group_id);
			if (Algorithms.isEmptyIntersection(groups, overlapSet) == false) {
				System.out.println(group_id + " -> " + Algorithms.getSetAsString(Algorithms.intersection(groups, overlapSet)));
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasOverlaps(Set<Long> groups, Map<Long, Set<Long>> overlapMap, Map<Long, List<Request>> requests) {
		Set<Long> wantedGroups = new HashSet<>();
		for (List<Request> requestList : requests.values()) {
			for (Request r : requestList) {
				wantedGroups.add(r.wanted_group_id);
			}
		}
		
		Set<Long> grantedRequestGroups = Algorithms.intersection(wantedGroups, groups);
				
		for (long group_id : grantedRequestGroups) {
			Set<Long> overlapSet = overlapMap.get(group_id);
			if (Algorithms.isEmptyIntersection(groups, overlapSet) == false) {
				System.out.println(group_id + " -> " + Algorithms.getSetAsString(Algorithms.intersection(groups, overlapSet)));
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasOverlaps(ChromosomeAdvanced chrome) {
		for (Student student : chrome.studentList) {
			Set<Long> groups = student.getGroups();
			if (hasOverlaps(groups, chrome.data.overlapMap, student.getRequestsInitial())) {
				System.out.println(" -> Overlap detected for student: " + student.getStudentId());
				return true;
			}
		}
		
		return false;
	}
	
	public static <T> boolean checkSetEquals(Set<T> set1, Set<T> set2) {
		if (set1.size() != set2.size()) {
			System.err.println("Set sizes does not match!!!");
			return false;
		} else {
			for (T e : set1) {
				if (!set2.contains(e)) {
					System.err.printf("Set1 contains element %s, but Set2 does not.\n", e);
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static String printApprovedRequests(Map<Long, Request> approvedRequests) {
		if (approvedRequests == null) {
			return "null";
		}
		
		StringBuilder sb = new StringBuilder();
		for (long activity_id : approvedRequests.keySet()) {
			Request r = approvedRequests.get(activity_id);
			sb.append(String.format("%s: %s\n", String.valueOf(activity_id),  r));
		}
		
		return sb.toString();
	}

	public static String printMap(Map<Request, List<Long>> activitiesToResolve) {
		if (activitiesToResolve == null) {
			return "null";
		}
		
		StringBuilder sb = new StringBuilder();
		for (Request r : activitiesToResolve.keySet()) {
			List<Long> list = activitiesToResolve.get(r);
			sb.append(String.format("%s: %s\n", r,  printList(list)));
		}
		
		return sb.toString();
	}

	private static String printList(List<Long> list) {
		if (list == null)
			return "null";
		
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i=0; i<list.size(); i++) {
			long l = list.get(i);
			sb.append(l);
			if (i != list.size() - 1)
				sb.append(",");
		}
		sb.append("]");
		return sb.toString();
	}

	public static void printIteration(int[] goodnessParameters, int t, ITimeProvider timeProvider, int requestsGranted,
			double T) {
		int a = goodnessParameters[0];
		int b = goodnessParameters[1];
		int c = goodnessParameters[2];
		int d = goodnessParameters[3];
		int e = goodnessParameters[4];
		int initialGoodness = goodnessParameters[5];
		int goodness = a + b + c - d - e;
		
		System.out.printf(" [%7d]: %7d %7d | %7d %7d %7d %7d %7d %7d | %.6f | %s \n", 
				t, goodness - initialGoodness, requestsGranted,
				a, b, c, d, e, goodness, T, timeProvider.stringTimePassed());
	}

	public static void writeToFile(String fileOut, String log) throws IOException {
		File file = Paths.get(fileOut).toFile();
		if (!file.exists())
			file.createNewFile();
		Files.write(Paths.get(fileOut), log.getBytes());
	}
}
