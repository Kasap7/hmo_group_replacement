package hr.fer.diplomskirad.zamjenagrupa.util;

public class SimpleTimeProvider implements ITimeProvider {
	private long timeout;
	private long startTime;
	
	public SimpleTimeProvider(long timeout) {
		startTime = System.currentTimeMillis();
		this.timeout = timeout;
	}

	@Override
	public boolean isTimedOut() {
		long currentTime = System.currentTimeMillis();
		if (currentTime < timeout)
			return false;
		return true;
	}

	@Override
	public String stringTimePassed() {
		double timePassed = getTimePassed();
		if (timePassed < 60) {
			return timePassed + " seconds";
		}
		int minutes = ((int)timePassed) / 60;
		double seconds = timePassed - 60 * minutes;
		return String.format("%d minutes, %.1f seconds", minutes, seconds);
	}
	
	private double getTimePassed() {
		long currentTime = System.currentTimeMillis();
		return 1.0*(currentTime - startTime) / 1000.0;
	}

	@Override
	public double getTimePercentagePassed() {
		long totalTime = timeout - startTime;
		long currentTime = System.currentTimeMillis() - startTime;
		return ((1.0*currentTime) / totalTime);
	}

	@Override
	public long getTimePassedMilliseconds() {
		long currentTime = System.currentTimeMillis() - startTime;
		return currentTime;
	}

}
