package hr.fer.diplomskirad.zamjenagrupa.util;

import java.io.File;
import java.io.IOException;
import java.util.*;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.Algorithms;
import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.data.Student;

public class Test {

	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "samples/Pravi1";
	static ITimeProvider timeProvider;
	
	// Environment of this program
	static DataAdvanced data;
	
	public static void main(String[] args) throws IOException {
		startTime = System.currentTimeMillis();
		timeProvider = new SimpleTimeProvider(timeout);
		
		initialiseParameters();

		data = new DataAdvanced(students, requests, overlaps, limits, award_activity, award_student, minmax_penalty);
		
		
		test1(1);
		test2(2);
		test3(3);
		test4(4);
		test5(5);
		test6(6);
		test7(7);
		test8(8);
		
		test9(9);
	}

	private static void initialiseParameters() {
		timeout += startTime;
		students = dir + File.separatorChar + "students.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
	}

	private static void test1(int i) {		
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3550773, 3551205, 3543265, 3544745};

		
		long student_id = 17614;
		long activitiy_id = 3550773;
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		Request request = studentRequests.get(activitiy_id).get(0);
		
		printRequest(request);
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequest(request, chrome.groupLimits, new Random());
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		checkOverlaps(groups, overlapMap);
		
		
	}

	private static void test2(int i) {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3550773, 3551205, 3543265, 3544745};

		
		long student_id = 17614;
		long activitiy_id = 3550773;
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		Request request = studentRequests.get(activitiy_id).get(0);
		
		System.out.println("Granting all requests randomly.");
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequests(new Random(), chrome.groupLimits);
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		checkOverlaps(groups, overlapMap);
		
	}
	
	private static void test3(int i) {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3550773, 3551205, 3543265, 3544745};

		
		long student_id = 17411;
		//long activitiy_id = 3550773;
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		//Request request = studentRequests.get(activitiy_id).get(0);
		
		System.out.println("Granting all requests randomly.");
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequests(new Random(), chrome.groupLimits);
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		checkOverlaps(groups, overlapMap);
		
	}
	
	private static void test4(int i) {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3550773, 3551205, 3543265, 3544745};

		
		long student_id = 17474;
		//long activitiy_id = 3550773;
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		//Request request = studentRequests.get(activitiy_id).get(0);
		
		System.out.println("Granting all requests randomly.");
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequests(new Random(), chrome.groupLimits);
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		//groups.add(170726L);
		
		checkOverlaps(groups, overlapMap);
		
	}
	
	private static void test5(int i) {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3551205, 3543745, 3549185, 3549233, 3542314, 3549209};

		
		long student_id = 17474;
		long activitiy_id = requestActivities[2];
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		Request request = studentRequests.get(activitiy_id).get(0);
		
		System.out.println("Granting request: " + request);
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequest(request, chrome.groupLimits, new Random());
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		//groups.add(170726L);
		
		checkOverlaps(groups, overlapMap);
		
	}
	
	private static void test6(int i) {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3541396, 3550773, 3551205, 3543745, 3549185, 3541450, 3549209, 3549257};

		
		long student_id = 17787;
		long activitiy_id1 = requestActivities[2];
		long activitiy_id2 = requestActivities[7];
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		Request request1 = studentRequests.get(activitiy_id1).get(1);
		Request request2 = studentRequests.get(activitiy_id2).get(1);
		
		System.out.println("Granting all requests.");
		System.out.println();
		
		Utils.printStudent(data, student_id);
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequests(new Random(), chrome.groupLimits);
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		//groups.add(170726L);
		
		checkOverlaps(groups, overlapMap);
		
	}
	
	private static void test7(int i) {
		Map<Long, Request> map1 = new HashMap<>();
		Map<Long, Request> map2 = new HashMap<>();
		Request r1 = new Request(5, 6, 7, 8, 9);
		Request r1_ = new Request(5, 6, 7, 8, 9);
		Request r2 = new Request(5, 111, 7, 8, 9);
		Request r3 = new Request(9, 6, 7, 8, 9);
		Request r3_ = new Request(9, 6, 7, 8, 9);
		
		map1.put(12L, r1);
		map1.put(13L, r2);
		map1.put(14L, r3);
		
		map2.put(12L, r1_);
		map2.put(13L, r2);
		map2.put(14L, r3_);
		
		System.out.println(map1);
		System.out.println(map2);
		
		System.out.println(map1.equals(map2));
	}
	
	private static void test8(int i) {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		int[] requestActivities = {3541396, 3550773, 3551205, 3543745, 3549185, 3541450, 3549209, 3549257};

		
		long student_id = 17787;
		long activitiy_id1 = requestActivities[2];
		long activitiy_id2 = requestActivities[7];
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		Request request1 = studentRequests.get(activitiy_id1).get(1);
		Request request2 = studentRequests.get(activitiy_id2).get(1);
		
		System.out.println("Granting all requests.");
		System.out.println();
		
		Utils.printStudent(data, student_id);
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		student.grantRequests(new Random(), chrome.groupLimits);
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		checkOverlaps(groups, overlapMap);
		
		////
		
		System.out.println();
		System.out.println("Rollbacking all requests!");
		student.rollbackRequests(chrome.groupLimits);
		printRequestsGranted(student);
		checkOverlaps(student.getGroups(), overlapMap);
		
	}
	
	private static void test9(int i) throws IOException {
		System.out.println();
		System.out.println();
		System.out.println("############################################");
		
		String students2 = "samples/Pravi2/students.csv";
		String requests2 = "samples/Pravi2/requests.csv";
		String overlaps2 = "samples/Pravi2/overlaps.csv";
		String limits2 = "samples/Pravi2/limits.csv";
		DataAdvanced data2 = new DataAdvanced(students2, requests2, overlaps2, limits2, award_activity, award_student, minmax_penalty);
		
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data2);
		
		long student_id = 17677;
		long activity_id = 3551205;
		
		printTest(student_id, i);
		System.out.println();
		
		Map<Long, List<Request>> studentRequests = chrome.data.requestsMap.get(student_id);
		Map<Long, Set<Long>> overlapMap = data2.overlapMap;
		
		Student student = chrome.studentMap.get(student_id);
		Request request = studentRequests.get(activity_id).get(1);
		
		System.out.println("Granting request: " + request);
		System.out.println();
		
		Utils.printStudent(data2, student_id);
		System.out.println();
		
		printGroups(student.getGroups(), studentRequests);
		
		//student.grantRequests(new Random(), chrome.groupLimits);
		student.grantRequest(request, chrome.groupLimits, new Random());
		
		Set<Long> groups = student.getGroups();
		
		printGroups(student.getGroups(), studentRequests);
		
		System.out.println();
		
		printRequestsGranted(student);
		
		System.out.println();
		
		checkOverlaps(groups, overlapMap);
		
		////
		
		System.out.println();
		System.out.println("Rollbacking all requests!");
		student.rollbackRequests(chrome.groupLimits);
		printRequestsGranted(student);
		checkOverlaps(student.getGroups(), overlapMap);
		
	}
	
	private static void printTest(long student_id, int i) {
		System.out.println("> Test" + i + ", student: " + student_id);
	}

	private static void printRequestsGranted(Student student) {
		System.out.println("Requests granted: " + student.getRequestsGranted());
	}

	private static void printRequest(Request request) {
		System.out.println("Request: " + request);
	}

	private static void printGroups(Set<Long> groups, Map<Long, List<Request>> studentRequests) {
		System.out.println(Algorithms.getSetAsString(getRelevantGroups(studentRequests, groups)));	
	}

	private static void checkOverlaps(Set<Long> groups, Map<Long, Set<Long>> overlapMap) {
		boolean hasOverlaps = Utils.hasOverlaps(groups, overlapMap);
		
		if (hasOverlaps) {
			System.err.println("######>   Error, student has Overlaps!!");
		} else {
			System.out.println("Done! No overlaps found :)");
		}
	}

	private static Set<Long> getRelevantGroups(Map<Long, List<Request>> studentRequests, Set<Long> groups) {

		Set<Long> relevantGroups = new LinkedHashSet<>();
		
		for (List<Request> requests : studentRequests.values()) {
			for (Request r : requests) {
				if (groups.contains(r.group_id)) {
					relevantGroups.add(r.group_id);
				} else if (groups.contains(r.wanted_group_id)) {
					relevantGroups.add(r.wanted_group_id);
					
				}
			}
		}
		
		return relevantGroups;
	}
}
