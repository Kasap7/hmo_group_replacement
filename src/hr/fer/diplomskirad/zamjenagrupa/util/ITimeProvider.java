package hr.fer.diplomskirad.zamjenagrupa.util;

public interface ITimeProvider {

	boolean isTimedOut();
	String stringTimePassed();
	double getTimePercentagePassed();
	long getTimePassedMilliseconds();
}
