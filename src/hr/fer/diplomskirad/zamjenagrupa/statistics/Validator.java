package hr.fer.diplomskirad.zamjenagrupa.statistics;

import java.io.File;
import java.io.IOException;

import hr.fer.diplomskirad.zamjenagrupa.data.Data;

public class Validator {
	
	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static String dataValidationFile = "validate.txt";
	
	// Environment of this program
	static Data data;
	
	public static void main(String[] args) throws IOException {
		
		startTime = System.currentTimeMillis();
		dir = "samples\\Pravi1";
		
		initialiseParameters();
		
		data = new Data(students, requests, overlaps, limits, timeout, startTime, award_activity, award_student, minmax_penalty);
		data.optimize();
		String result = data.validateSolution(dataValidationFile);
		
		System.out.println(result);
	}
	
	private static void initialiseParameters() {
		
		timeout += startTime;
		students = dir + File.separatorChar + "out_improved_popsize_500_10m_1.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
	}
}
