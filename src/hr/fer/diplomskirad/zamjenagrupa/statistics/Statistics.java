package hr.fer.diplomskirad.zamjenagrupa.statistics;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.Ga;
import hr.fer.diplomskirad.zamjenagrupa.data.Data;

/**
 * This program solves optimization problem defined in file: Upute_zamjena_grupa.pdf.
 * In this optimization problem students want to transfer groups and they created a bundle of requests.
 * Without breaking hard constraints defined in the Upute_zamjena_grupa.pdf you have to grant as many
 * of the requests as possible, more specifically you have to gain the best possible score for granting those requests.
 * This program uses genetic algorithm to find the best solution: the best possible requests that have to be granted 
 * from the bundle to ensure the highest score.
 * @author Josip Kasap
 *
 */
public class Statistics {
	
	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static String[] dirs = {"samples\\Pravi1", "samples\\Pravi2", "samples\\Sample3", "samples\\Sample4",
							"samples\\Sample5", "samples\\Sample6", "samples\\Sample7"};
	static StringBuilder log = new StringBuilder();
	
	// Environment of this program
	static Data data;
	
	public static void main(String[] args) throws IOException {
		
		log.append("file,goodness,requestsGranted,iterations,A,B,C,DE,SUM\n");
		
		for (int i=1; i<=3; i++) {
			for (String s : dirs) {
				startTime = System.currentTimeMillis();
				
				initialiseParameters(s);
				
				printStatment("Preprocessing started on directory: " + dir + ".");
				
				preprocessing();
				
				printStatment("Preprocessing done!");
				
				solveWithGA(i);
				printStatment("GA is done!");
				
				System.out.printf("\n%s\n", log.toString());
				
				System.out.printf("##################################################################################################\n");
			}
		}
		
		Files.write(Paths.get("statistics_popsize_500_10m.csv"), log.toString().getBytes());
	}
	
	private static void solveWithGA(int i) throws IOException {
		//int popSize = 250;
		int popSize = 500;
		int maxEval = 1000000;
		double pm = 0.02;
		
		Ga ga = new Ga(data, popSize, maxEval, pm);
		ga.doYourJob();
		
		String fileOut = dir + File.separatorChar + "out_improved_popsize_500_10m_" + i + ".csv";
		ga.getBest().writeToFile(students, fileOut);
		
		log.append(String.format("%s,%d,%d,%d,%d,%d,%d,%d,%d\n",
				dir.split("\\\\")[1], ga.getBest().goodness - data.initialGoodness, ga.getBest().grantedRequests.size(),
				ga.getIterationCount(), ga.getBest().a, ga.getBest().b, ga.getBest().c, -ga.getBest().de, ga.getBest().goodness));
	}

	
	private static void printStatment(String s) {
		double timePassed = 1.0*(System.currentTimeMillis() - startTime) / 1000.0;
		s += "\tTime passed = %.3f seconds\n";
		System.out.printf(s, timePassed);
	}
	
	/**
	 * Initializes program parameters from command line arguments:<p>
	 * <pre>
	 * "-timeout": how long will the program run (in seconds)
	 * "-award-activity": award points for each of student request for activity that is granted
	 * "-award-student": award points for student that has all of his requests for activity granted
	 * "-minmax-penalty": penalty for exceeding the preferred count of students in classroom
	 * "-students-file": position of students-file
	 * "-requests-file": position of requests-file
	 * "-overlaps-file": position of overlaps-file
	 * "-limits-file": position of limits-file
	 * </pre>
	 * @param args command line arguments
	 */
	private static void initialiseParameters(String s) {
		
		dir = s;
		timeout = 600*1000 + startTime;
		
		students = dir + File.separatorChar + "students.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
	}

	private static void preprocessing() throws IOException {
		data = new Data(students, requests, overlaps, limits, timeout, startTime, award_activity, award_student, minmax_penalty);
		data.optimize();
	}

}
