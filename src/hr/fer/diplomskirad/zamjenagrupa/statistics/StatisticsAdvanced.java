package hr.fer.diplomskirad.zamjenagrupa.statistics;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedGa;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedGeneticAlgorithm;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedSimmulatedAnnealing;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.Ga;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.utility.UtilityChromosome;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.utility.UtilitySimmulatedAnnealing;
import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Data;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.SimpleTimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

/**
 * This program solves optimization problem defined in file: Upute_zamjena_grupa.pdf.
 * In this optimization problem students want to transfer groups and they created a bundle of requests.
 * Without breaking hard constraints defined in the Upute_zamjena_grupa.pdf you have to grant as many
 * of the requests as possible, more specifically you have to gain the best possible score for granting those requests.
 * This program uses genetic algorithm to find the best solution: the best possible requests that have to be granted 
 * from the bundle to ensure the highest score.
 * @author Josip Kasap
 *
 */
public class StatisticsAdvanced {
	
	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static String[] dirs = {"samples\\Pravi1", "samples\\Pravi2", "samples\\Sample3", "samples\\Sample4",
							"samples\\Sample5", "samples\\Sample6", "samples\\Sample7"};
	static StringBuilder log = new StringBuilder();
	static ITimeProvider timeProvider;
	
	// Environment of this program
	static DataAdvanced data;
	
	public static void main(String[] args) throws IOException {
		
		log.append("file,goodness,requestsGranted,iterations,A,B,C,DE,SUM\n");
		
		for (int i=1; i<=3; i++) {
			for (String s : dirs) {
				startTime = System.currentTimeMillis();
				
				initialiseParameters(s);
				timeProvider = new SimpleTimeProvider(timeout);
				
				printStatment("Preprocessing started on directory: " + dir + ".");
				
				preprocessing();
				
				printStatment("Preprocessing done!");
				
				//solveWithGA(i);
				//solveWithSA(i);
				//solveWithGApp(i);
				solveWithSApp(i);
				printStatment("SA is done!");
				
				System.out.printf("\n%s\n", log.toString());
				
				System.out.printf("##################################################################################################\n");
			}
		}
		
		Files.write(Paths.get("statistics_SA_improved_10m.csv"), log.toString().getBytes());
	}
	
	private static void solveWithSA(int i) throws IOException {
		int maxEval = 10000000;
		boolean allowGreedy = true;
		
		UtilitySimmulatedAnnealing sa = new UtilitySimmulatedAnnealing(data, timeProvider, maxEval);
		sa.setAllowGreedy(allowGreedy);
		sa.doYourJob();
		
		String fileOut = dir + File.separatorChar + "out_SA_10m_" + i + ".csv";
		try {
			UtilityChromosome best = sa.getBest();
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(students, fileOut, best.approvedRequests);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
		
		int[] goodnessParams = sa.getBest().getGoodnessParameters();
		int goodness = goodnessParams[0] + goodnessParams[1] + goodnessParams[2] - goodnessParams[3] - goodnessParams[4];
		log.append(String.format("%s,%d,%d,%d,%d,%d,%d,%d,%d\n",
				dir.split("\\\\")[1], goodness - data.initialGoodness, sa.getBest().getRequestsGranted(),
				sa.getIterationCount(), goodnessParams[0], goodnessParams[1], goodnessParams[2], 
				goodnessParams[3] + goodnessParams[4], goodness));
	}
	
	private static void solveWithGA(int i) throws IOException {
		int popSize = 400;
		int maxEval = 1000000;
		double pm = 0.02;
		
		AdvancedGeneticAlgorithm ga = new AdvancedGeneticAlgorithm(data, timeProvider, popSize, maxEval, pm);
		ga.doYourJob();
		
		String fileOut = dir + File.separatorChar + "out_student_improved_400_10m_" + i + ".csv";
		try {
			ChromosomeAdvanced best = ga.getBest();
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(students, fileOut, best.studentList);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
		
		int[] goodnessParams = ga.getBest().getGoodnessParameters();
		int goodness = goodnessParams[0] + goodnessParams[1] + goodnessParams[2] - goodnessParams[3] - goodnessParams[4];
		log.append(String.format("%s,%d,%d,%d,%d,%d,%d,%d,%d\n",
				dir.split("\\\\")[1], goodness - data.initialGoodness, ga.getBest().getRequestsGranted(),
				ga.getIterationCount(), goodnessParams[0], goodnessParams[1], goodnessParams[2], 
				goodnessParams[3] + goodnessParams[4], goodness));
	}
	
	private static void solveWithGApp(int i) throws IOException {
		int popSize = 400;
		int maxEval = 1000000;
		double pm = 0.02;
		
		AdvancedGa ga = new AdvancedGa(data, timeProvider, popSize, maxEval, pm);
		ga.doYourJob();
		
		String fileOut = dir + File.separatorChar + "out_student_extra_improved_400_10m_" + i + ".csv";
		try {
			ChromosomeAdvanced best = ga.getBest();
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(students, fileOut, best.studentList);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
		
		int[] goodnessParams = ga.getBest().getGoodnessParameters();
		int goodness = goodnessParams[0] + goodnessParams[1] + goodnessParams[2] - goodnessParams[3] - goodnessParams[4];
		log.append(String.format("%s,%d,%d,%d,%d,%d,%d,%d,%d\n",
				dir.split("\\\\")[1], goodness - data.initialGoodness, ga.getBest().getRequestsGranted(),
				ga.getIterationCount(), goodnessParams[0], goodnessParams[1], goodnessParams[2], 
				goodnessParams[3] + goodnessParams[4], goodness));
	}

	private static void solveWithSApp(int i) throws IOException {
		int maxEval = 10000000;
		
		AdvancedSimmulatedAnnealing sa = new AdvancedSimmulatedAnnealing(data, timeProvider, maxEval);
		sa.doYourJob();
		
		String fileOut = dir + File.separatorChar + "out_SA_improved_10m_" + i + ".csv";
		try {
			ChromosomeAdvanced best = sa.getBest();
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(students, fileOut, best.studentList);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
		
		int[] goodnessParams = sa.getBest().getGoodnessParameters();
		int goodness = goodnessParams[0] + goodnessParams[1] + goodnessParams[2] - goodnessParams[3] - goodnessParams[4];
		log.append(String.format("%s,%d,%d,%d,%d,%d,%d,%d,%d\n",
				dir.split("\\\\")[1], goodness - data.initialGoodness, sa.getBest().getRequestsGranted(),
				sa.getIterationCount(), goodnessParams[0], goodnessParams[1], goodnessParams[2], 
				goodnessParams[3] + goodnessParams[4], goodness));
	}

	
	private static void printStatment(String s) {
		double timePassed = 1.0*(System.currentTimeMillis() - startTime) / 1000.0;
		s += "\tTime passed = %.3f seconds\n";
		System.out.printf(s, timePassed);
	}
	
	/**
	 * Initializes program parameters from command line arguments:<p>
	 * <pre>
	 * "-timeout": how long will the program run (in seconds)
	 * "-award-activity": award points for each of student request for activity that is granted
	 * "-award-student": award points for student that has all of his requests for activity granted
	 * "-minmax-penalty": penalty for exceeding the preferred count of students in classroom
	 * "-students-file": position of students-file
	 * "-requests-file": position of requests-file
	 * "-overlaps-file": position of overlaps-file
	 * "-limits-file": position of limits-file
	 * </pre>
	 * @param args command line arguments
	 */
	private static void initialiseParameters(String s) {
		
		dir = s;
		timeout = 600*1000 + startTime;
		
		students = dir + File.separatorChar + "students.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
	}

	private static void preprocessing() throws IOException {
		data = new DataAdvanced(students, requests, overlaps, limits, award_activity, award_student, minmax_penalty);
	}

}
