package hr.fer.diplomskirad.zamjenagrupa;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedGa;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedGeneticAlgorithm;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedSimmulatedAnnealing;
import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.SimpleTimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

/**
 * This program solves optimization problem defined in file: Upute_zamjena_grupa.pdf.
 * In this optimization problem students want to transfer groups and they created a bundle of requests.
 * Without breaking hard constraints defined in the Upute_zamjena_grupa.pdf you have to grant as many
 * of the requests as possible, more specifically you have to gain the best possible score for granting those requests.
 * This program uses genetic algorithm to find the best solution: the best possible requests that have to be granted 
 * from the bundle to ensure the highest score.
 * @author Josip Kasap
 *
 */
public class DemoAdvanced {
	
	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static ITimeProvider timeProvider;
	static String out = "out.csv";
	
	// Environment of this program
	static DataAdvanced data;
	
	public static void main(String[] args) throws IOException {
		startTime = System.currentTimeMillis();
		initialiseParameters(args);
		timeProvider = new SimpleTimeProvider(timeout);
		
		printStatment("Preprocessing started on directory: " + dir + ".");
		
		preprocessing();
		
		printStatment("Preprocessing done!");
		
		//greedy();
		
		//solveWithGa();
		//solveWithGaAdv();
		solveWithSa();
		
		printStatment("Genetic Algorithm is done!");
		
	}
	
	private static void solveWithGaAdv() {
		int popSize = 400;
		int maxEval = 10000000;
		double pm = 0.02;
		String fileOut = dir + File.separatorChar + out;
		
		//Ga ga = new Ga(data, popSize, maxEval, pm);
		AdvancedGa ga = new AdvancedGa(data, timeProvider, popSize, maxEval, pm);
		ga.doYourJob();
		
		try {
			ChromosomeAdvanced best = ga.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.studentList);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}
	
	private static void solveWithSa() {
		int maxEval = 100000000;
		String fileOut = dir + File.separatorChar + out;
		
		//Ga ga = new Ga(data, popSize, maxEval, pm);
		AdvancedSimmulatedAnnealing sa = new AdvancedSimmulatedAnnealing(data, timeProvider, maxEval);
		sa.doYourJob();
		
		try {
			ChromosomeAdvanced best = sa.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.studentList);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}
	
	private static void solveWithGa() {
		int popSize = 400;
		int maxEval = 10000000;
		double pm = 0.02;
		String fileOut = dir + File.separatorChar + out;
		
		//Ga ga = new Ga(data, popSize, maxEval, pm);
		AdvancedGeneticAlgorithm ga = new AdvancedGeneticAlgorithm(data, timeProvider, popSize, maxEval, pm);
		ga.doYourJob();
		
		try {
			ChromosomeAdvanced best = ga.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.studentList);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}


	private static void greedy() {
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		Random rand = new Random();
		
		Utils.printGoodness(chrome.getGoodnessParameters());
		
		//Utils.printStudent(data, 17837);
		//System.exit(0);
		
		chrome.grantRequestsGreedy(rand);
		
		Utils.printGoodness(chrome.getGoodnessParameters());
		
		String fileOut = dir + File.separatorChar + "out_test.csv";
		
		try {
			Utils.writeToFile(students, fileOut, chrome.studentList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void printStatment(String s) {
		s += "\tTime passed = %s.\n";
		System.out.printf(s, timeProvider.stringTimePassed());
	}
	
	/**
	 * Initializes program parameters from command line arguments:<p>
	 * <pre>
	 * "-timeout": how long will the program run (in seconds)
	 * "-award-activity": award points for each of student request for activity that is granted
	 * "-award-student": award points for student that has all of his requests for activity granted
	 * "-minmax-penalty": penalty for exceeding the prefered count of students in classroom
	 * "-students-file": position of students-file
	 * "-requests-file": position of requests-file
	 * "-overlaps-file": position of overlaps-file
	 * "-limits-file": position of limits-file
	 * </pre>
	 * @param args command line arguments
	 */
	private static void initialiseParameters(String[] args) {
		for (int i=0; i<args.length; i++) {
			if (args[i].equals("-timeout") && args.length > i+1) {
				timeout = Utils.parseInteger(args[i+1], "-timeout")*1000;
			} else if (args[i].equals("-award-activity") && args.length > i+1) { 
				String[] arr = args[i+1].substring(0, args[i+1].length()).split(",");
				award_activity = new int[arr.length];
				for (int k=0; k < arr.length; k++) {
					award_activity[k] = Utils.parseInteger(args[i+1], "-award-activity");
				}
			} else if (args[i].equals("-award-student") && args.length > i+1) { 
				award_student = Utils.parseInteger(args[i+1], "-award-student");
			} else if (args[i].equals("-minmax-penalty") && args.length > i+1) { 
				minmax_penalty = Utils.parseInteger(args[i+1], "-minmax-penalty");
			} else if (args[i].equals("-dir") && args.length > i+1) { 
				dir = args[i+1];
			}
		}
		
		timeout += startTime;
		students = dir + File.separatorChar + "students.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
		
		Utils.checkExists(students, dir, "students.csv");
		Utils.checkExists(requests, dir, "requests.csv");
		Utils.checkExists(limits, dir, "limits.csv");
		Utils.checkExists(overlaps, dir, "overlaps.csv");
	}

	private static void preprocessing() throws IOException {
		data = new DataAdvanced(students, requests, overlaps, limits, award_activity, award_student, minmax_penalty);
	}

}
