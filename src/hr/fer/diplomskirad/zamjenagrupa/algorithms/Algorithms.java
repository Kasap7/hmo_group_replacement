package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Algorithms {

	/**
	 * This method returns intersection between 2 sets.
	 * @param set1 first set
	 * @param set2 second set
	 * @return intersection between 2 sets.
	 */
	public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
		Set<T> intersectionSet = new HashSet<>();
		
		if (set1.size() <= set2.size()) {
			for (T e : set1) {
				if (set2.contains(e)) {
					intersectionSet.add(e);
				}
			}
		} else {
			for (T e : set2) {
				if (set1.contains(e)) {
					intersectionSet.add(e);
				}
			}
		}
		
		return intersectionSet;
	}
	
	/**
	 * This method checks if intersection between 2 sets is empty.
	 * @param set1 First set
	 * @param set2 Second set
	 * @return <code>true</code> if intersection between 2 sets is empty, <code>false</code> otherwise.
	 */
	public static <T> boolean isEmptyIntersection(Set<T> set1, Set<T> set2) {

		if (set1.size() <= set2.size()) {
			for (T e : set1) {
				if (set2.contains(e)) {
					return false;
				}
			}
		} else {
			for (T e : set2) {
				if (set1.contains(e)) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * This method checks if intersection between 2 sets is empty, with exeption of one elemet which is to be ignored.
	 * @param set1 First set
	 * @param set2 Second set
	 * @param ignored Element which is to be ignored from intersection
	 * @return <code>true</code> if intersection between 2 sets is empty, <code>false</code> otherwise.
	 */
	public static <T> boolean isEmptyIntersectionIgnoreOne(Set<T> set1, Set<T> set2, T ignored) {

		if (set1.size() <= set2.size()) {
			for (T e : set1) {
				if (!e.equals(ignored) && set2.contains(e)) {
					return false;
				}
			}
		} else {
			for (T e : set2) {
				if (!e.equals(ignored) && set1.contains(e)) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static List<int[]> getAllPermutations(List<Integer> perm) {
		if (perm.isEmpty())
			return null;
		List<int[]> permList = new ArrayList<>();
		int[] permutation = new int[perm.size()];
		for (int i=0; i<permutation.length; i++) {
			permutation[i] = 0;
		}
		
		boolean toBreak = true;
		
		while (true) {
			toBreak = true;
			
			permList.add(permutation.clone());
			
			for (int i=0; i<permutation.length; i++) {
				
				if (permutation[i] != perm.get(i) - 1) {
					toBreak = false;
					break;
				}
			}
			
			for (int i = 0, overflow = 1; i < permutation.length && overflow > 0; i++) {
				permutation[i] += overflow;
				overflow = permutation[i] / perm.get(i);
				permutation[i] = permutation[i] % perm.get(i);
			}
			
			if (toBreak)
				break;
		}
		return permList;
	}
	
	public static void main(String[] args) {
		List<Integer> perm = new ArrayList<>();
		perm.add(2);
		perm.add(3);
		perm.add(4);
		
		List<int[]> permList = getAllPermutations(perm);
		for (int[] permutation : permList) {
			System.out.println(getAsString(permutation));
		}
	}
	
	private static String getAsString(int[] data) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int x : data) {
			sb.append(x + " ");
		}
		
		return sb.toString().substring(0, sb.toString().length()-1) + "]";
	}
	
	public static <T> String getSetAsString(Set<T> set) {
		if (set == null || set.isEmpty()) {
			return "[]";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (T t : set) {
			sb.append(t + ",");
		}
		return sb.toString().substring(0, sb.toString().length() - 1) + "]";
	}
}
