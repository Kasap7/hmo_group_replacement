package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Student;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

public class AdvancedGeneticAlgorithm extends GeneticAlgorithm<ChromosomeAdvanced> {
	private DataAdvanced data;

	public AdvancedGeneticAlgorithm(DataAdvanced data, ITimeProvider timeProvider, int popSize, int maxEval, double pm) {
		super(timeProvider, popSize, maxEval, pm);
		
		this.data = data;
	}

	@Override
	public void getInitialPopulation() {
		population = new ArrayList<>();
		
		for (int i=0; i<popSize; i++) {
			ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
			chrome.grantRequestsGreedy(rand);
			population.add(chrome);
			
			if (best == null || extractGoodness(chrome) > extractGoodness(best)) {
				best = chrome;
			}
			
			//if (Utils.hasOverlaps(chrome)) {
			//	System.out.println("ERROR, overlap detected in initial population!");
			//}
			t++;
		}
	}

	@Override
	public ChromosomeAdvanced crossover(ChromosomeAdvanced p1, ChromosomeAdvanced p2) {
		ChromosomeAdvanced child = crossover1(p1, p2);
		//if (Utils.hasOverlaps(child)) {
		//	System.out.println("ERROR, overlap detected AFTER CROSSOVER!");
		//}
		return child;
	}

	@Override
	public void mutate(ChromosomeAdvanced child) {
		if (rand.nextDouble() < pm)
			mutateSimple(child);
	}
	
	private ChromosomeAdvanced crossover1(ChromosomeAdvanced p1, ChromosomeAdvanced p2) {
		ChromosomeAdvanced child = p1.copy();
		
		// PICK STUDENTS TO CHANGE
		List<Long> studentsToChange = new ArrayList<>();
		for (long student_id : child.studentMap.keySet()) {
			Student s1 = child.studentMap.get(student_id);
			Student s2 = p2.studentMap.get(student_id);
			
			if (rand.nextBoolean() && !s1.equals(s2)) {
				studentsToChange.add(s1.getStudentId());
			}
		}
		
		// ROLLBACK ALL OF THEIR REQUESTS
		while (true) {
			int requestsDeleted = 0;
			for (long student_id : studentsToChange) {
				requestsDeleted += child.rollbackStudent(student_id);
			}
			
			if (requestsDeleted == 0)
				break;
		}
		
		// ACCEPT ALL REQUESTS FROM OTHER PARENT
		boolean hasGrantedAny = true;
		while (hasGrantedAny) {
			hasGrantedAny = false;
			for (long student_id : studentsToChange) {
				Student s2 = p2.studentMap.get(student_id);
				hasGrantedAny = hasGrantedAny || child.grantRequests(student_id, rand, s2.getApprovedRequests().values());
			}
		}
		
		// ACCEPT ALL REQUESTS
		child.grantRequestsGreedy(rand);
		
		return child;
	}

	private void mutateSimple(ChromosomeAdvanced child) {
		int requestsGranted = child.getRequestsGranted();
		int toBeDeleted = (int)(requestsGranted * cm);
		int deleted = 0;
		
		// Delete certain amount of requests from some students
		while (true) {
			Collections.shuffle(child.studentList, rand);
			for (Student student : child.studentList) {
				deleted += child.rollbackStudent(student.getStudentId());
				if (deleted >= toBeDeleted)
					break;
			}
			
			if (deleted >= toBeDeleted)
				break;
		}
		
		child.grantRequestsGreedy(rand);
	}

	@Override
	protected int extractGoodness(ChromosomeAdvanced chrome) {
		return chrome.getGoodness();
	}

	@Override
	public void doYourJob() {
		System.out.println();
		printInitialGoodness();
		
		getInitialPopulation();
		Utils.printHeader();
		Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
		
		int prevGoodness = extractGoodness(best);
		
		int iterationWithoutProgress = 0;
		long currentTime = System.currentTimeMillis();
		
		while (t < maxEval && timeProvider.isTimedOut() == false) {
			t++;
			iterationWithoutProgress++;
			
			List<ChromosomeAdvanced> turnResults = getTurnResults();
			population.remove(turnResults.get(0));
			ChromosomeAdvanced c = crossover(turnResults.get(1), turnResults.get(2));
			mutate(c);
			population.add(c);
			
			if (extractGoodness(c) > extractGoodness(best)) {
				best = c;
				iterationWithoutProgress = 0;
				currentTime = System.currentTimeMillis();
			}			
			
			if (t % popSize == 0 && extractGoodness(best) > prevGoodness) {
				Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
				prevGoodness = extractGoodness(best);
				//System.out.println(pm);
			} else if (t % (4*popSize) == 0 && (System.currentTimeMillis() - currentTime) > 30*1000) {
				Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
				currentTime = System.currentTimeMillis();
			}
			
			if (iterationWithoutProgress >= TERMINATION_THR) {
				System.out.println(" >>> No progress for a long time, terminating the algorithm...");
				break;
			}
			
			regulateMutation();
		}
		
		Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
	}

	private void printInitialGoodness() {
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		Utils.printGoodness(chrome.getGoodnessParameters());
	}
}
