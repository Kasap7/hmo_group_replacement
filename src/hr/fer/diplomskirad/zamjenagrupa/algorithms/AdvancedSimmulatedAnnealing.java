package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

public class AdvancedSimmulatedAnnealing {
	
	private DataAdvanced data;
	private ITimeProvider timeProvider;
	private ChromosomeAdvanced best;
	private ChromosomeAdvanced current;
	private Random rand;
	private int t = 0;
	private int maxEval;
	private int bestIter = 1;
	private int bestGoodness = Integer.MIN_VALUE;
	private static final int TERMINATION_THR = 10000000;
	private StringBuilder log;
	
	private int K;
	private double kPercentage = 0.01;
	private static final int K_MIN = 5;
	private static final int K_MAX = 8;
	private final int HEAT_UP_STEP_INIT;
	
	private double T;
	private double T0;
	private double bestT;
	private double Tend;
	private double alpha;
	private final int n;
	private int heatUpStep;
	
	private boolean logActive = false;
	private boolean allowGreedy = true;
	private boolean terminateInactivity = false;
	private boolean printTemperure = false;
	
	public AdvancedSimmulatedAnnealing(DataAdvanced data, ITimeProvider timeProvider, int maxEval) {
		this.data = data;
		this.timeProvider = timeProvider;
		this.maxEval = maxEval;
		this.rand = new Random();
		n = 2000;
		HEAT_UP_STEP_INIT = n/10;
		
		initializeSAParams();
	}

	private void initializeSAParams() {
		K = calculateK();
		initializeHeatUpStep();
	}

	private void initializeTemperature() {
		bestT = T = T0 = 7.5;
		Tend = 0.2;
		
		initGeoTempParams();
	}

	private void initGeoTempParams() {
		alpha = Math.pow((Tend/T0), (1.0/n));
	}

	private int calculateK() {
		int studentsCnt = data.students.size();
		int k = (int)(kPercentage*studentsCnt);
		k = Math.max(Math.min(K_MIN, studentsCnt), k);
		k = Math.min(k, K_MAX);
		return k;
	}

	public void doYourJob() {
		appendLogHeader();
		current = getInitialSolution();
		best = current;
		bestGoodness = extractGoodness(best);
		t++;
		initializeTemperature();
		
		int previouslyBestGoodness = extractGoodness(best);
		long currentTime = System.currentTimeMillis();
		int iterationWithoutProgress = 0;
		
		System.out.println();
		printInitialGoodness();
		Utils.printHeaderSA();
		
		printIteration();
		
		logIteration();
		while (t < maxEval && timeProvider.isTimedOut() == false) {
			initT();
			current = best;
			
			if (bestGoodness > previouslyBestGoodness) {
				printIteration();
				previouslyBestGoodness = extractGoodness(best);
			} else if ((System.currentTimeMillis() - currentTime) > 30*1000) {
				magnifyHeatUp();
				initT();
				printIteration();
				currentTime = System.currentTimeMillis();
			}
			
			if (terminateInactivity && iterationWithoutProgress >= TERMINATION_THR) {
				System.out.println(" >>> No progress for a long time, terminating the algorithm...");
				break;
			}
		
			while (t < maxEval && timeProvider.isTimedOut() == false && T >= Tend) {
				t++;
				iterationWithoutProgress++;
				
				ChromosomeAdvanced currentNext = getANeighbour();
				updateCurrent(currentNext);
				
				if (extractGoodness(current) > bestGoodness) {
					best = current;
					bestGoodness = extractGoodness(best);
					bestIter = t;
					bestT = T;
					
					logIteration();
					initializeHeatUpStep();
					currentTime = System.currentTimeMillis();
					iterationWithoutProgress = 0;
				}
				
				updateTemp();
			}
		}
		
		printBest();
		logIteration();
	}

	private void magnifyHeatUp() {
		heatUpStep = heatUpStep + HEAT_UP_STEP_INIT;
	}

	private void initializeHeatUpStep() {
		heatUpStep = HEAT_UP_STEP_INIT;
	}

	private void initT() {
		T = bestT;
		heatUp();
	}

	private void heatUp() {
		T = T*Math.pow((1/alpha), heatUpStep);
		T = Math.min(T0*0.5, T);
	}

	private void updateCurrent(ChromosomeAdvanced currentNext) {
		int nextGoodness = extractGoodness(currentNext);
		int currentGoodness = extractGoodness(current);
		int df = nextGoodness - currentGoodness;
		
		if (df > 0) {
			current = currentNext;
		} else {
			double p = getProbability(df);
			double rnd = rand.nextDouble();
			
			if (rnd < p) 
				current = currentNext;
		}
	}

	private double getProbability(int df) {
		// {[f(next) - f(current)]^2}*exp(1/T)
		return Math.exp(df/T);
	}

	private void updateTemp() {
		updateGeoTemp();
	}

	private void updateGeoTemp() {
		T = alpha*T;
	}

	private void printIteration() {
		printCurrent();
		if (printTemperure)
			System.out.printf("   > T = %f   \n", T);
	}

	private void printCurrent() {
		Utils.printIteration(current.getGoodnessParameters(), t, timeProvider, current.getRequestsGranted(), T);
	}
	
	private void printBest() {
		Utils.printIteration(best.getGoodnessParameters(), bestIter, timeProvider, best.getRequestsGranted(), T);
	}

	private int extractGoodness(ChromosomeAdvanced chrome) {
		if (chrome != null)
			return chrome.getGoodness();
		return Integer.MIN_VALUE;
	}

	private ChromosomeAdvanced getANeighbour() {
		long[] student_ids = getStudentIDs();
		ChromosomeAdvanced chrome = current.copy();
		
		for (long student_id : student_ids) {
			Map<Long, Request> approved = new HashMap<>();
			
			for (List<Request> reqList : data.requestsMapPossible.get(student_id).values()) {
				int index = getIndex(reqList);
				if (index >= 0) {
					Request r = reqList.get(index);
					approved.put(r.activity_id, r);
				}
			}
			
			chrome.forceStudentRequests(approved, student_id);
		}
		
		chrome.getValidSolution(rand);
		if (allowGreedy) 
			chrome.grantRequestsGreedy(rand);
		
		return chrome;
	}

	private long[] getStudentIDs() {
		Set<Integer> studentIndexes = new HashSet<>();
		long[] student_ids = new long[K];
		int studentsCnt = data.students.size();
		
		for (int i=0; i<K; i++) {
			int index = rand.nextInt(studentsCnt);
			while (studentIndexes.contains(index)) {
				index = rand.nextInt(studentsCnt);
			}
			studentIndexes.add(index);
			student_ids[i] = data.students.get(index);
		}
		
		return student_ids;
	}

	private int getIndex(List<Request> reqList) {
		if (reqList == null || reqList.isEmpty())
			return -1;
		return rand.nextInt(reqList.size() + 1) - 1;
	}

	private ChromosomeAdvanced getInitialSolution() {
		ChromosomeAdvanced chrome = ChromosomeAdvanced.getRandomChromosome(data, rand);
		chrome.getValidSolution(rand);
		if (allowGreedy)
			chrome.grantRequestsGreedy(rand);
		return chrome;
	}
	
	public ChromosomeAdvanced getBest() {
		return best;
	}
	
	private void printInitialGoodness() {
		Utils.printGoodnessReduced(new int[] {0, 0, 0, -data.initialGoodness, data.initialGoodness});
	}
	
	public void setAllowGreedy(boolean b) {
		allowGreedy = b;
	}

	
	public int getIterationCount() {
		return t;
	}

	public void setLogActive(boolean b) {
		logActive = b;
	}

	public String getAlgorithmLog() {
		return log.toString();
	}
	
	private void appendLogHeader() {
		if (!logActive)
			return;
		log = new StringBuilder();
		log.append(String.format("iteration,time,goodness,requestsGranted\n"));
	}
	
	private void logIteration() {
		if (!logActive)
			return;
		log.append(String.format("%d,%d,%d,%d\n", t, timeProvider.getTimePassedMilliseconds(), 
				extractGoodness(best) - data.initialGoodness, best.getRequestsGranted()));
	}
}
