package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import hr.fer.diplomskirad.zamjenagrupa.data.ChromosomeAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.data.Student;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

public class AdvancedGa extends GeneticAlgorithm<ChromosomeAdvanced> {
	private DataAdvanced data;
	private double chance1 = 0.5;
	
	private StringBuilder log;
	
	private boolean logActive = false;
	private boolean allowGreedy = true;
	private boolean terminateInactivity = false;

	public AdvancedGa(DataAdvanced data, ITimeProvider timeProvider, int popSize, int maxEval, double pm) {
		super(timeProvider, popSize, maxEval, pm);
		
		this.data = data;
	}

	@Override
	public void getInitialPopulation() {
		population = new ArrayList<>(popSize);
		
		for (int i=0; i<popSize; i++) {
			t++;
			ChromosomeAdvanced chrome = ChromosomeAdvanced.getRandomChromosome(data, rand);
			chrome.getValidSolution(rand);
			if (allowGreedy)
				chrome.grantRequestsGreedy(rand);
			population.add(chrome);
			
			if (extractGoodness(chrome) > extractGoodness(best)) {
				best = chrome;
				logIteration();
			}
		}
	}

	@Override
	public ChromosomeAdvanced crossover(ChromosomeAdvanced p1, ChromosomeAdvanced p2) {
		if (rand.nextDouble() < chance1)
			return crossover1(p1, p2);
		else
			return crossover2(p1, p2);
	}

	@Override
	public void mutate(ChromosomeAdvanced child) {
		if (rand.nextDouble() < chance1)
			mutate1(child);
		else 
			mutate2(child);
	}

	private ChromosomeAdvanced crossover1(ChromosomeAdvanced p1, ChromosomeAdvanced p2) {
		ChromosomeAdvanced child = p1.copy();
		
		// PICK STUDENTS TO CHANGE
		List<Long> studentsToChange = new ArrayList<>();
		for (long student_id : child.studentMap.keySet()) {
			Student s1 = child.studentMap.get(student_id);
			Student s2 = p2.studentMap.get(student_id);
			
			if (rand.nextBoolean() && !s1.equals(s2)) {
				studentsToChange.add(s1.getStudentId());
			}
		}
		
		// ROLLBACK ALL OF THEIR REQUESTS
		while (true) {
			int requestsDeleted = 0;
			for (long student_id : studentsToChange) {
				requestsDeleted += child.rollbackStudent(student_id);
			}
			
			if (requestsDeleted == 0)
				break;
		}
		
		// ACCEPT ALL REQUESTS FROM OTHER PARENT
		boolean hasGrantedAny = true;
		while (hasGrantedAny) {
			hasGrantedAny = false;
			for (long student_id : studentsToChange) {
				Student s2 = p2.studentMap.get(student_id);
				hasGrantedAny = hasGrantedAny || child.grantRequests(student_id, rand, s2.getApprovedRequests().values());
			}
		}
		
		// GRANT ALL REQUESTS
		if (allowGreedy)
			child.grantRequestsGreedy(rand);
		
		return child;
	}

	private void mutate1(ChromosomeAdvanced child) {
		if (!shouldMutate())
			return;
		
		int requestsGranted = child.getRequestsGranted();
		int toBeDeleted = (int)(requestsGranted * cm);
		int deleted = 0;
		
		// Delete certain amount of requests from some students
		while (true) {
			Collections.shuffle(child.studentList, rand);
			for (Student student : child.studentList) {
				deleted += child.rollbackStudent(student.getStudentId());
				if (deleted >= toBeDeleted)
					break;
			}
			
			if (deleted >= toBeDeleted)
				break;
		}
		
		if (allowGreedy)
			child.grantRequestsGreedy(rand);
	}
	
	private boolean shouldMutate() {
		if (rand.nextDouble() < pm)
			return true;
		return false;
	}

	private ChromosomeAdvanced crossover2(ChromosomeAdvanced p1, ChromosomeAdvanced p2) {
		//Map<Long, Map<Long, Request>> crossoverApprovedRequests = crossoverApprovedRequests(p1, p2);
		ChromosomeAdvanced child = p1.copy();
		
		double firstChromeProbability = 0.75;
		double crossReferenceProbability = firstChromeProbability * 0.1;
		
		for (long student_id : child.studentMap.keySet()) {
			boolean isFirst = rand.nextDouble() < firstChromeProbability;
			Request crossreferenceRequest = null;
			
			if (rand.nextDouble() < crossReferenceProbability) {
				ChromosomeAdvanced second = isFirst ? p2 : p1;
				
				List<Long> randomActivityList = new ArrayList<>(data.requestsMapPossible.get(student_id).keySet());
				if (randomActivityList.isEmpty())
					continue;
				
				long randomActivity_id = randomActivityList.get(rand.nextInt(randomActivityList.size()));
				
				crossreferenceRequest = second.studentMap.get(student_id).getApprovedRequests().get(randomActivity_id);
			}
			
			if (!isFirst) {
				Student otherStudent = p2.studentMap.get(student_id);
				Map<Long, Request> secondApproved = new HashMap<>(otherStudent.getApprovedRequests());
				if (crossreferenceRequest != null) {
					secondApproved.put(crossreferenceRequest.activity_id, crossreferenceRequest);
				}
				child.forceStudentRequests(secondApproved, student_id);
			} else if (crossreferenceRequest != null) {
				child.forceGrantRequest(crossreferenceRequest, student_id);
			}
		}
		
		child.getValidSolution(rand);
		//if (Utils.hasOverlaps(child)) {
		//	System.out.println("ERROR, overlap detected after crossover2!");
		//}
		
		if (allowGreedy)
			child.grantRequestsGreedy(rand);
		
		return child;
	}
	
	private void mutate2(ChromosomeAdvanced child) {
		if (!shouldMutate())
			return;
		
		double cM = 0.25;
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			if (rand.nextDouble() >= cM)
				continue;
			
			Map<Long, Request> requestsForStudent = new HashMap<>();
			
			for (List<Request> requests : data.requestsMapPossible.get(student_id).values()) {
				int indexToGrant = rand.nextInt(requests.size() + 1) - 1;
				if (indexToGrant >= 0) {
					Request r = requests.get(indexToGrant);
					requestsForStudent.put(r.activity_id, r);
				}
			}
			
			child.forceStudentRequests(requestsForStudent, student_id);
		}
		
		child.getValidSolution(rand);
		//if (Utils.hasOverlaps(child)) {
		//	System.out.println("ERROR, overlap detected after mutation2!");
		//}
		if (allowGreedy)
			child.grantRequestsGreedy(rand);
	}

	@Override
	protected int extractGoodness(ChromosomeAdvanced chrome) {
		if (chrome == null)
			return Integer.MIN_VALUE;
		return chrome.getGoodness();
	}

	@Override
	public void doYourJob() {
		appendLogHeader();
		System.out.println();
		printInitialGoodness();
		
		getInitialPopulation();
		Utils.printHeader();
		Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
		
		int prevGoodness = extractGoodness(best);
		
		int iterationWithoutProgress = 0;
		long currentTime = System.currentTimeMillis();
		
		while (t < maxEval && timeProvider.isTimedOut() == false) {
			t++;
			iterationWithoutProgress++;
			
			List<ChromosomeAdvanced> turnResults = getTurnResults();
			population.remove(turnResults.get(0));
			ChromosomeAdvanced c = crossover(turnResults.get(1), turnResults.get(2));
			mutate(c);
			population.add(c);
			
			if (extractGoodness(c) > extractGoodness(best)) {
				best = c;
				logIteration();
				iterationWithoutProgress = 0;
				currentTime = System.currentTimeMillis();
			}			
			
			if (t % popSize == 0 && extractGoodness(best) > prevGoodness) {
				Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
				prevGoodness = extractGoodness(best);
				//System.out.println(pm);
			} else if (t % (4*popSize) == 0 && (System.currentTimeMillis() - currentTime) > 30*1000) {
				Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
				currentTime = System.currentTimeMillis();
			}
			
			if (terminateInactivity && iterationWithoutProgress >= TERMINATION_THR) {
				System.out.println(" >>> No progress for a long time, terminating the algorithm...");
				break;
			}
			
			regulateMutation();
		}
		
		Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
		logIteration();
	}

	private void printInitialGoodness() {
		ChromosomeAdvanced chrome = new ChromosomeAdvanced(data);
		Utils.printGoodness(chrome.getGoodnessParameters());
	}
	
	public void setLogActive(boolean b) {
		logActive = b;
	}
	
	public String getAlgorithmLog() {
		if (logActive)
			return log.toString();
		return "";
	}
	
	private void appendLogHeader() {
		if (!logActive)
			return;
		log = new StringBuilder();
		log.append(String.format("iteration,time,goodness,requestsGranted\n"));
	}
	
	private void logIteration() {
		if (!logActive)
			return;
		log.append(String.format("%d,%d,%d,%d\n", t, timeProvider.getTimePassedMilliseconds(), 
				extractGoodness(best) - data.initialGoodness, best.getRequestsGranted()));
	}

}
