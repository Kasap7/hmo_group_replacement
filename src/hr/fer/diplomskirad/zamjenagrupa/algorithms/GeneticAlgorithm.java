package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.ArrayList;
import java.util.List;

import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;

public abstract class GeneticAlgorithm<T> extends Algorithm {
	
	protected List<T> population;
	protected T best;
	protected int maxEval, popSize;
	protected double pm, pmInit;
	protected double cm = 0.35, cs = 0.2;
	protected int t = 0;
	protected static final int TERMINATION_THR = 100000;
	protected double pmMax = 0.2;

	public GeneticAlgorithm(ITimeProvider timeProvider, int popSize, int maxEval, double pm) {
		super(timeProvider);
		this.popSize = popSize;
		this.maxEval = maxEval;
		this.pm = pm;
		this.pmInit = pm;
		population = new ArrayList<>(popSize);
	}
	
	public abstract void getInitialPopulation();
	public abstract T crossover(T p1, T p2);
	public abstract void mutate(T child);
	
	public T getBest() {
		return best;
	}
	
	public int getIterationCount() {
		return t;
	}
	
	protected List<T> getTurnResults() {
		int x = rand.nextInt(popSize), y = rand.nextInt(popSize), z = rand.nextInt(popSize);
		while (x == y) {
			y = rand.nextInt(popSize);
		}
		while (y == z) {
			z = rand.nextInt(popSize);
		}
		
		T cx = population.get(x);
		T cy = population.get(y);
		T cz = population.get(z);
		List<T> turnResults = new ArrayList<>();
		
		if (extractGoodness(cx) < extractGoodness(cy) && extractGoodness(cx) < extractGoodness(cz)) {
			turnResults.add(cx);
			turnResults.add(cy);
			turnResults.add(cz);
		} else if (extractGoodness(cy) < extractGoodness(cx) && extractGoodness(cy) < extractGoodness(cz)) {
			turnResults.add(cy);
			turnResults.add(cx);
			turnResults.add(cz);
		} else {
			turnResults.add(cz);
			turnResults.add(cx);
			turnResults.add(cy);
		}
		
		return turnResults;
	}
	
	protected abstract int extractGoodness(T t);
	
	protected void regulateMutation() {
		pm = pmInit + (pmMax-pmInit) * timeProvider.getTimePercentagePassed();
	}
}
