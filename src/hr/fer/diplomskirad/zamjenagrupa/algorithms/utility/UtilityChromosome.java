package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.Algorithms;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.GroupLimit;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;

public class UtilityChromosome {
	
	public Map<Long, Map<Long, Request>> approvedRequests;
	private Map<Long, GroupLimit> groupLimits;
	private Map<Long, Set<Long>> studentGroups;
	private DataAdvanced data;
	private boolean hasGoodness = false;
	private int goodness = 0;
	private int a, b, c, d, e;
	
	public UtilityChromosome(DataAdvanced data, Map<Long, Map<Long, Request>> approvedRequests) {
		super();
		this.data = data;
		this.approvedRequests = approvedRequests;
	}
	
	public UtilityChromosome(DataAdvanced data, UtilityChromosome chrome) {
		super();
		this.data = data;
		this.approvedRequests = chrome.copyApproved();
	}
	
	public UtilityChromosome(DataAdvanced data) {
		super();
		this.data = data;
		this.approvedRequests = new HashMap<>();
		
		getInitialGroupLimits();
		getStudentGroups();
	}
	
	public void validateAndGreed(Random rand) {
		getValidSolution(rand);
		greed(rand);
	}
	
	public void getValidSolution(Random rand) {
		hasGoodness = false;
		
		getInitialGroupLimits();
		getCurrentGroupLimits();
		getStudentGroups();
		
		List<Long> students = new ArrayList<>(approvedRequests.keySet());
		boolean hasConflict = true;
		
		while (hasConflict) {
			Collections.shuffle(students, rand);
			hasConflict = false;
			
			for (long student_id : students) {
				Map<Long, Request> approvedRequestsForStudent = approvedRequests.get(student_id);
				List<Request> removeList = null;
				
				if (approvedRequestsForStudent == null || approvedRequestsForStudent.isEmpty())
					continue;
				for (Request request : new ArrayList<>(approvedRequestsForStudent.values())) {
					if ((isRequestOverLimits(request) || doesRequestCreateOverlaps(request))) {
						removeRequestSoft(request);
						hasConflict = true;
						if (removeList == null)
							removeList = new ArrayList<>();
						removeList.add(request);
					}
				}
				
				removeRequestsFromApproved(removeList);
			}
		}
	}
	
	public void greed(Random rand) {
		hasGoodness = false;
		boolean grantedAny = true;
		
		while (grantedAny) {
			grantedAny = false;
			Collections.shuffle(data.requestList, rand);
			
			for (Request r : data.requestList) {
				grantedAny = grantedAny || grantRequest(r);
			}
		}
	}
	
	public boolean removeRequest(Request r) {
		if (!approvedRequests.containsKey(r.student_id) || !approvedRequests.get(r.student_id).containsKey(r.activity_id)
				|| !approvedRequests.get(r.student_id).get(r.activity_id).equals(r))
			return false;
		
		GroupLimit limit1 = groupLimits.get(r.group_id);
		GroupLimit limit2 = groupLimits.get(r.wanted_group_id);
		if (limit1.isMax() || limit2.isMinPref())
			return false;
		
		if (wouldRemoveOverlap(r))
			return false;
		
		removeRequestHard(r);
		
		return true;
	}

	private void removeRequestHard(Request r) {
		removeRequestSoft(r);
		approvedRequests.get(r.student_id).remove(r.activity_id);
		if (approvedRequests.get(r.student_id).isEmpty())
			approvedRequests.remove(r.student_id);
	}

	private boolean wouldRemoveOverlap(Request r) {
		Set<Long> overlapsWithInitialGroup = data.overlapMap.get(r.group_id);
		Set<Long> currentGroups = studentGroups.get(r.student_id);
		Set<Long> intersection = Algorithms.intersection(currentGroups, overlapsWithInitialGroup);
		intersection.remove(r.wanted_group_id);
		return !intersection.isEmpty();
	}

	public boolean grantRequest(Request r) {
		if (approvedRequests.containsKey(r.student_id) && approvedRequests.get(r.student_id).get(r.activity_id) != null) {
			return false;
		}
		
		GroupLimit limit1 = groupLimits.get(r.group_id);
		GroupLimit limit2 = groupLimits.get(r.wanted_group_id);
		
		if (limit1.isMin() || limit2.isMax()) {
			return false;
		}
		
		if (wouldOverlap(r)) {
			return false;
		}
		
		setRequest(r);
		
		return true;
	}

	private boolean wouldOverlap(Request r) {
		Set<Long> overlapsWithWantedGroup = data.overlapMap.get(r.wanted_group_id);
		Set<Long> currentGroups = studentGroups.get(r.student_id);
		Set<Long> intersection = Algorithms.intersection(currentGroups, overlapsWithWantedGroup);
		
		return !intersection.isEmpty();
	}

	private void setRequest(Request r) {
		if (!approvedRequests.containsKey(r.student_id)) {
			approvedRequests.put(r.student_id, new HashMap<>());
		}
		
		approvedRequests.get(r.student_id).put(r.activity_id, r);
		
		studentGroups.get(r.student_id).remove(r.group_id);
		studentGroups.get(r.student_id).add(r.wanted_group_id);
		
		groupLimits.get(r.group_id).decreaseValue();
		groupLimits.get(r.wanted_group_id).increaseValue();
	}

	private void removeRequestSoft(Request request) {
		GroupLimit limit1 = groupLimits.get(request.group_id);
		GroupLimit limit2 = groupLimits.get(request.wanted_group_id);
		
		Set<Long> groups = studentGroups.get(request.student_id);
		groups.remove(request.wanted_group_id);
		groups.add(request.group_id);
		
		limit1.increaseValue();
		limit2.decreaseValue();
	}
	
	private void removeRequestsFromApproved(List<Request> requests) {
		if (requests == null)
			return;
		for (Request request : requests) {
			approvedRequests.get(request.student_id).remove(request.activity_id);
			if (approvedRequests.get(request.student_id).isEmpty()) {
				approvedRequests.remove(request.student_id);
			}
		}
	}

	private boolean doesRequestCreateOverlaps(Request request) {
		Set<Long> overlapsWithRequest = data.overlapMap.get(request.wanted_group_id);
		Set<Long> groups = studentGroups.get(request.student_id);
		Set<Long> overlapsWithGroups = Algorithms.intersection(overlapsWithRequest, groups);
		
		overlapsWithGroups.remove(request.wanted_group_id);
		
		return !overlapsWithGroups.isEmpty();
	}

	private boolean isRequestOverLimits(Request request) {
		GroupLimit limit1 = groupLimits.get(request.group_id);
		GroupLimit limit2 = groupLimits.get(request.wanted_group_id);
		
		if (limit1.isUnderMin() || limit2.isOverMax()) {
			return true;
		}
		
		return false;
	}

	private void getStudentGroups() {
		Map<Long, Set<Long>> sGroups = getInitialStudentGroups();
		
		for (long student_id : approvedRequests.keySet()) {
			Map<Long, Request> approvedRequestsForStudent = approvedRequests.get(student_id);
			
			if (approvedRequestsForStudent == null || approvedRequestsForStudent.isEmpty())
				continue;
			
			Set<Long> groupsForStudent = sGroups.get(student_id);
			for (Request request : approvedRequestsForStudent.values()) {
				groupsForStudent.remove(request.group_id);
				groupsForStudent.add(request.wanted_group_id);
			}
			
		}
		 
		studentGroups = sGroups;
	}

	private Map<Long, Set<Long>> getInitialStudentGroups() {
		Map<Long, Set<Long>> studentGroups = new HashMap<>();
		 
		for (long student_id : data.studentGroups.keySet()) {
			studentGroups.put(student_id, new HashSet<>(data.studentGroups.get(student_id)));
		}
		return studentGroups;
	}

	private void getCurrentGroupLimits() {
		for (long student_id : approvedRequests.keySet()) {
			Map<Long, Request> approvedRequestsForStudent = approvedRequests.get(student_id);
			
			if (approvedRequestsForStudent == null || approvedRequestsForStudent.isEmpty())
				continue;
			for (Request request : approvedRequestsForStudent.values()) {
				GroupLimit limit1 = groupLimits.get(request.group_id);
				GroupLimit limit2 = groupLimits.get(request.wanted_group_id);
				
				limit1.decreaseValue();
				limit2.increaseValue();
			}
		}
	}

	public int getGoodness() {
		if (hasGoodness)
			return goodness;
		if (groupLimits == null) 
			populateGroupLimits();
		
		calculateGoodness();
		
		hasGoodness = true;
		return goodness;
	}

	private void populateGroupLimits() {
		getInitialGroupLimits();
		for (long student_id : approvedRequests.keySet()) {
			Map<Long, Request> approvedRequestsForStudent = approvedRequests.get(student_id);
			
			if (approvedRequestsForStudent == null || approvedRequestsForStudent.isEmpty())
				continue;
			
			for (Request request : approvedRequestsForStudent.values()) {
				GroupLimit limit1 = groupLimits.get(request.group_id);
				GroupLimit limit2 = groupLimits.get(request.wanted_group_id);
				limit1.decreaseValue(); 
				limit2.increaseValue(); 
			}
		}
	}
	
	private Map<Long, Map<Long, Request>> copyApproved() {
		Map<Long, Map<Long, Request>> copied = new HashMap<>();
		for (long student_id : approvedRequests.keySet()) {
			Map<Long, Request> approved = approvedRequests.get(student_id);
			Map<Long, Request> approvedCopy = new HashMap<>();
			
			for (Request r : approved.values()) {
				approvedCopy.put(r.activity_id, r);
			}
			
			if (!approved.isEmpty()) {
				copied.put(student_id, approvedCopy);
			}
		}
		return copied;
	}

	private void calculateGoodness() {
		calculateDE();
		
		for (long student_id : approvedRequests.keySet()) {
			Map<Long, Request> approvedRequestsForStudent = approvedRequests.get(student_id);
			
			if (approvedRequestsForStudent == null || approvedRequestsForStudent.isEmpty())
				continue;
			
			for (Request request : approvedRequestsForStudent.values()) {
				a += request.swap_weight;												// A
			}
			
			int requestsGranted = approvedRequestsForStudent.size();
			int totalRequestsForStudent = data.requestsMap.get(student_id).size();
			
			int index = requestsGranted <= data.award_activity.length ? requestsGranted - 1 : data.award_activity.length - 1;
			
			b += index < 0 ? 0 : data.award_activity[index];											// B
			c += requestsGranted == totalRequestsForStudent ? data.award_student : 0;	// C
		}
		
		goodness = a + b + c - d - e;
	}
	
	private void calculateDE() {
		for (GroupLimit limit : groupLimits.values()) {
			d += limit.getD(data.minmax_penalty);
			e += limit.getE(data.minmax_penalty);
		}
	}

	public int[] getGoodnessParameters() {
		return new int[] {a, b, c, d, e, data.initialGoodness};
	}

	private void getInitialGroupLimits() {
		groupLimits = new HashMap<>();
		for (long group_id : data.groupLimits.keySet()) {
			GroupLimit limit = data.groupLimits.get(group_id);
			groupLimits.put(group_id, limit.copy());
		}
	}
	
	public int getRequestsGranted() {
		int granted = 0;
		for (Map<Long, Request> grantedRequestsForStudent : approvedRequests.values()) {
			granted += grantedRequestsForStudent.size();
		}
		return granted;
	}
	
	public static UtilityChromosome getRandomChromosome(DataAdvanced data, Random rand) {
		return new UtilityChromosome(data, getRandomlySelectedRequests(rand, data));
	}
	
	private static Map<Long, Map<Long, Request>> getRandomlySelectedRequests(Random rand, DataAdvanced data) {
		Map<Long, Map<Long, List<Request>>> randomRequests = new HashMap<>();
		
		double requestGrantedMin = 0.45;
		double requestGrantedMax = 0.9;
		double requestGrantedChance = rand.nextDouble() * (requestGrantedMax - requestGrantedMin) + requestGrantedMin;
		
		
		for (Request r : data.requestList) {
			Map<Long, List<Request>> requestsForStudent = null;
			if (!randomRequests.containsKey(r.student_id)) {
				requestsForStudent = new HashMap<>();
				//randomRequests.put(r.student_id, requestsForStudent);
			} else {
				requestsForStudent = randomRequests.get(r.student_id);
			}
			
			List<Request> requestsForActivity = null;
			
			if (!requestsForStudent.containsKey(r.activity_id)) {
				requestsForActivity = new ArrayList<>();
				//requestsForStudent.put(r.activity_id, requestsForActivity);
			} else {
				requestsForActivity = requestsForStudent.get(r.activity_id);
			}
			
			if (rand.nextDouble() < requestGrantedChance) {
				requestsForActivity.add(r);
				if (!requestsForStudent.containsKey(r.activity_id)) {
					requestsForStudent.put(r.activity_id, requestsForActivity);
				}
				if (!randomRequests.containsKey(r.student_id)) {
					randomRequests.put(r.student_id, requestsForStudent);
				}
			}
		}
		
		Map<Long, Map<Long, Request>> radomApprovedRequests = new HashMap<>();
		
		for (long student_id : randomRequests.keySet()) {
			Map<Long, Request> approvedForStudent = new HashMap<>();
			for (long activity_id : randomRequests.get(student_id).keySet()) {
				List<Request> randomlyApproved = randomRequests.get(student_id).get(activity_id);
				if (randomlyApproved.isEmpty())
					continue;
				
				Request approvedRequest = randomlyApproved.get(rand.nextInt(randomlyApproved.size()));
				approvedForStudent.put(activity_id, approvedRequest);
			}
			
			if (!approvedForStudent.isEmpty())
				radomApprovedRequests.put(student_id, approvedForStudent);
		}
		
		return radomApprovedRequests;
	}
}
