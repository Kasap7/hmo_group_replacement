package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;

public class UtilityPSOVelocity {

	public Map<Long, Map<Long, Integer>> velocity;
	private DataAdvanced data;
	private static final int vMAX =  20;
	private static final int vMIN = -20;
	
	public UtilityPSOVelocity(Map<Long, Map<Long, Integer>> velocity, DataAdvanced data) {
		this.data = data;
		this.velocity = velocity;
	}
	
	public static UtilityPSOVelocity getRandomVelocity(Random rand, DataAdvanced data) {
		Map<Long, Map<Long, Integer>> velocity = new HashMap<>();
		
		Map<Long, Map<Long, List<Request>>> requestsPossibleMap = data.requestsMapPossible;
		
		for (long student_id : requestsPossibleMap.keySet()) {
			Map<Long, List<Request>> requestsForStudent = requestsPossibleMap.get(student_id);
			
			Map<Long, Integer> velocityStudent = new HashMap<>();
			
			for (long activity_id : requestsForStudent.keySet()) {
				List<Request> requests = requestsForStudent.get(activity_id);
				int requestCnt = requests.size();
				
				int indexRandom = rand.nextInt(requestCnt + 1) - requestCnt/2;
				indexRandom = validate(indexRandom);
				
				velocityStudent.put(activity_id, indexRandom);
			}
			
			velocity.put(student_id, velocityStudent);
		}
		
		return new UtilityPSOVelocity(velocity, data);
	}
	
	public static UtilityPSOVelocity add(DataAdvanced data, UtilityPSOVelocity v1, UtilityPSOVelocity v2, UtilityPSOVelocity v3,
			double w, double c1, double c2, Random rand) {
		Map<Long, Map<Long, Integer>> velocity = new HashMap<>();
		
		double s1 = w;
		double s2 = c1 * rand.nextDouble();
		double s3 = c2 * rand.nextDouble();
		
		for (long student_id : v1.velocity.keySet()) {
			Map<Long, Integer> velocityStudent = new HashMap<>();
			
			Map<Long, Integer> v1VelocityStudent = v1.velocity.get(student_id);
			Map<Long, Integer> v2VelocityStudent = v2.velocity.get(student_id);
			Map<Long, Integer> v3VelocityStudent = v3.velocity.get(student_id);
			
			int inx = getIndex(s1, s2, s3, rand);
			Map<Long, Integer> vVelocityStudent = null;
			
			if (inx == 1) {
				vVelocityStudent = v1VelocityStudent;
			} else if (inx == 2) {
				vVelocityStudent = v2VelocityStudent;
			} else if (inx == 3) {
				vVelocityStudent = v3VelocityStudent;
			}
			
			for (long activity_id : v1VelocityStudent.keySet()) {
				int index = vVelocityStudent == null ? 0 : vVelocityStudent.get(activity_id);
				velocityStudent.put(activity_id, index);
			}
			
			velocity.put(student_id, velocityStudent);
		}
		
		return new UtilityPSOVelocity(velocity, data);
	}
	
	private static int getIndex(double s1, double s2, double s3, Random rand) {
		int index = 0;
		double max = 0;
		
		double x1 = rand.nextDouble();
		if (x1*s1 > max) {
			max = x1*s1;
			index = 1;
		}
		
		double x2 = rand.nextDouble();
		if (x2*s2 > max) {
			max = x2*s2;
			index = 2;
		}
		
		double x3 = rand.nextDouble();
		if (x3*s3 > max) {
			max = x3*s3;
			index = 3;
		}
		
		return index;
	}

	private static int getIndex(int i1, double s1, int i2, double s2, int i3, double s3, Random rand) {
		int index = 0;
		
		if (rand.nextDouble() < s1)
			index += i1;
		if (rand.nextDouble() < s2)
			index += i2;
		if (rand.nextDouble() < s3)
			index += i3;
		
		return validate(index);
	}

	public UtilityChromosome addVelocityToPosition(UtilityChromosome position, Random rand) {
		Map<Long, Map<Long, Request>> apporvedRequests = new HashMap<>();
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			Map<Long, List<Request>> studentPossibleRequests = data.requestsMapPossible.get(student_id);
			Map<Long, Request> studentApprovedRequests = new HashMap<>();
			Map<Long, Request> positionApprovedRequests = position.approvedRequests.getOrDefault(student_id, new HashMap<>());
			
			for (long activity_id : studentPossibleRequests.keySet()) {
				int velocityIndex = velocity.get(student_id).get(activity_id);
				Request positionApproved = positionApprovedRequests.get(activity_id);
				int positionIndex = positionApproved == null ? -1 : data.requestToListMapping.get(positionApproved);
				List<Request> requests = studentPossibleRequests.get(activity_id);
				int requestsCnt = requests.size();
				
				int newIndex = getNewIndex(positionIndex, velocityIndex, requestsCnt);
				if (newIndex == -1) 
					continue;
				
				Request request = requests.get(newIndex);
				studentApprovedRequests.put(activity_id, request);
			}
			
			if (!studentApprovedRequests.isEmpty())
				apporvedRequests.put(student_id, studentApprovedRequests);
		}
		
		UtilityChromosome chrome = new UtilityChromosome(data, apporvedRequests);
		//chrome.validateAndGreed(rand);
		return chrome;
	}

	public static UtilityPSOVelocity substractPositions(UtilityChromosome p1, UtilityChromosome p2, DataAdvanced data) {
		Map<Long, Map<Long, Integer>> velocity = new HashMap<>();
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			Map<Long, Integer> velocityStudent = new HashMap<>();
			
			Map<Long, Request> p1ApprovedForStudent = p1.approvedRequests.getOrDefault(student_id, new HashMap<>());
			Map<Long, Request> p2ApprovedForStudent = p2.approvedRequests.getOrDefault(student_id, new HashMap<>());
			Map<Long, List<Request>> possibleRequestsForStudent = data.requestsMapPossible.get(student_id);
			
			for (long activity_id : possibleRequestsForStudent.keySet()) {
				
				Request r1 = p1ApprovedForStudent.get(activity_id);
				Request r2 = p2ApprovedForStudent.get(activity_id);
				
				int i1 = r1 == null ? -1 : data.requestToListMapping.get(r1);
				int i2 = r2 == null ? -1 : data.requestToListMapping.get(r2);
				
				int index = i1 - i2;
				index = validate(index);
				
				velocityStudent.put(activity_id, index);
			}
			
			velocity.put(student_id, velocityStudent);
		}
		
		return new UtilityPSOVelocity(velocity, data);
	}
	
	private int getNewIndex(int x, int v, int N) {
		return (( ((x+1) + v) + vMAX*(N+1) ) % (N+1)) - 1;
	}

	public static int validate(int indexRandom) {
		if (indexRandom > vMAX)
			return vMAX;
		if (indexRandom < vMIN)
			return vMIN;
		return indexRandom;
	}
}
