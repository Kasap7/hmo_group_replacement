package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils.Pair;

public class UtilityTabooList {
	
	private List<Pair<Long, Long>> tabooList;
	Map<Long, Map<Long, Request>> tabooMap;
	private int C = 0;
	
	public UtilityTabooList(int C) {
		tabooList = new LinkedList<>();
		tabooMap = new HashMap<>();
		this.C = C;
	}
	
	public void add(Pair<Long, Long> e, Request r) {
		addToTaboo(e, r);
	}
	
	public void add(Request r, long student_id, long activity_id) {
		add(Pair.of(student_id, activity_id), r);
	}

	private void addToTaboo(Pair<Long, Long> e, Request r) {
		tabooList.add(0, e);
		addToTabooMap(e, r);
		validate();
	}

	private void validate() {
		while (tabooList.size() > C) {
			Pair<Long, Long> e = tabooList.get(tabooList.size() - 1);
			tabooList.remove(tabooList.size() - 1);
			removeFromTabooMap(e);
		}
	}
	
	private void addToTabooMap(Pair<Long, Long> e, Request r) {
		long student_id = e.first(), activity_id = e.second();
		
		if (!tabooMap.containsKey(student_id)) {
			tabooMap.put(student_id, new HashMap<>());
		}
		
		tabooMap.get(student_id).put(activity_id, r);
	}
	
	private void removeFromTabooMap(Pair<Long, Long> e) {
		long student_id = e.first(), activity_id = e.second();
		if (!tabooMap.containsKey(student_id))
			return;
		
		Map<Long, Request> tabooActivitesForStudent = tabooMap.get(student_id);
		tabooActivitesForStudent.remove(activity_id);
		if (tabooActivitesForStudent.isEmpty())
			tabooMap.remove(student_id);
		
	}

	public Pair<Long, Long> get(int i) {
		return tabooList.get(i);
	}
	
	public List<Long> getAllStudents() {
		return tabooList.stream().map(pair -> pair.first()).distinct().collect(Collectors.toList());
	}
	
	public int getTabooIndex(long student_id, long activity_id, List<Request> reqList) {
		int index = -2;
		if (tabooMap.containsKey(student_id) && tabooMap.get(student_id).containsKey(activity_id)) {
			Request r = tabooMap.get(student_id).get(activity_id);
			if (r == null) {
				index = -1;
			} else {
				index = reqList.indexOf(r);
			}
		}
		
		return index;
	}
	
	public boolean isStudentTaboo(Map<Long, Map<Long, Request>> approvedRequests) {
		for (long student_id : tabooMap.keySet()) {
			Map<Long, Request> tabooActivities = tabooMap.get(student_id);
			Map<Long, Request> approved = approvedRequests.getOrDefault(student_id, new HashMap<>());
			
			for (long activity_id : tabooActivities.keySet()) {
				Request r = tabooActivities.get(activity_id);
				if (r == null) {
					if (approved.containsKey(activity_id))
						return true;
				} else {
					if (!approved.containsKey(activity_id) || !approved.get(activity_id).equals(r)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public Set<Long> getTabooActivities(long student_id) {
		return tabooMap.get(student_id).keySet();
	}
}
