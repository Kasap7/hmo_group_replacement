package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.GeneticAlgorithm;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

public class UtilityGeneticAlgorithm extends GeneticAlgorithm<UtilityChromosome> implements UtilityAlgorithm {
	
	private DataAdvanced data;
	private StringBuilder log;
	
	private boolean logActive = false;
	private boolean terminateInactivity = false;
	private boolean allowGreedy = false;

	public UtilityGeneticAlgorithm(DataAdvanced data, ITimeProvider timeProvider, int popSize, int maxEval, double pm) {
		super(timeProvider, popSize, maxEval, pm);
		this.data = data;
		
		cm = 0.25;
	}

	@Override
	public void getInitialPopulation() {
		population = new ArrayList<>();
		
		for (int i=0; i<popSize; i++) {
			t++;
			
			UtilityChromosome chrome = UtilityChromosome.getRandomChromosome(data, rand);
			chrome.getValidSolution(rand);
			if (allowGreedy)
				chrome.greed(rand);
			
			population.add(chrome);
			
			if (best == null || extractGoodness(chrome) > extractGoodness(best)) {
				best = chrome;
				logIteration();
			}
		}
	}

	@Override
	public void doYourJob() {
		appendLogHeader();
		System.out.println();
		printInitialGoodness();
		
		getInitialPopulation();
		Utils.printHeader();
		Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
		
		int prevGoodness = extractGoodness(best);
		
		int iterationWithoutProgress = 0;
		long currentTime = System.currentTimeMillis();
		
		while (t < maxEval && timeProvider.isTimedOut() == false) {
			t++;
			iterationWithoutProgress++;
			
			List<UtilityChromosome> turnResults = getTurnResults();
			population.remove(turnResults.get(0));
			UtilityChromosome c = crossover(turnResults.get(1), turnResults.get(2));
			mutate(c);
			population.add(c);
			
			if (extractGoodness(c) > extractGoodness(best)) {
				best = c;
				logIteration();
				iterationWithoutProgress = 0;
				currentTime = System.currentTimeMillis();
			}			
			
			if (t % popSize == 0 && extractGoodness(best) > prevGoodness) {
				Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
				prevGoodness = extractGoodness(best);
				//System.out.println(pm);
			} else if (t % (4*popSize) == 0 && (System.currentTimeMillis() - currentTime) > 30*1000) {
				Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
				currentTime = System.currentTimeMillis();
			}
			
			if (terminateInactivity && iterationWithoutProgress >= TERMINATION_THR) {
				System.out.println(" >>> No progress for a long time, terminating the algorithm...");
				break;
			}
			
			regulateMutation();
		}
		
		Utils.printIteration(best.getGoodnessParameters(), t, timeProvider, best.getRequestsGranted());
		logIteration();
	}

	private void printInitialGoodness() {
		Utils.printGoodnessReduced(new int[] {0, 0, 0, -data.initialGoodness, data.initialGoodness});
	}

	@Override
	public UtilityChromosome crossover(UtilityChromosome p1, UtilityChromosome p2) {
		double cross1 = 0.33;
		
		if (rand.nextDouble() <= cross1) {
			Map<Long, Map<Long, Request>> childRequests = crossover1(p1, p2);
			mutate(childRequests);
			UtilityChromosome child = new UtilityChromosome(data, childRequests);
			child.getValidSolution(rand);
			if (allowGreedy)
				child.greed(rand);
			
			return child;
		} else {
			return crossover2(p1, p2);
		}
	}

	private void mutate(Map<Long, Map<Long, Request>> childRequests) {
		mutate1(childRequests);
	}
	
	private Map<Long, Map<Long, Request>> crossover1(UtilityChromosome p1, UtilityChromosome p2) {
		Map<Long, Map<Long, Request>> childRequests = new HashMap<>();
		double firstChromeProbability = 0.8;
		double crossReferenceProbability = firstChromeProbability * 0.0;
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			boolean isFirst = rand.nextDouble() < firstChromeProbability;
			UtilityChromosome first, second;
			if (isFirst) {
				first = p1;
				second = p2;
			} else {
				first = p2;
				second = p1;
			}
			
			Map<Long, Request> requestsForStudent = new HashMap<>();
			
			if (first.approvedRequests.containsKey(student_id)) {
				for (Request r : first.approvedRequests.get(student_id).values()) {
					requestsForStudent.put(r.activity_id, r);
				}
			}
			
			if (rand.nextDouble() < crossReferenceProbability) {
				List<Long> randomActivityList = new ArrayList<>(data.requestsMapPossible.get(student_id).keySet());
				if (randomActivityList.isEmpty())
					continue;
				long randomActivity_id = randomActivityList.get(rand.nextInt(randomActivityList.size()));
				Request secondRequest = null;
				if (second.approvedRequests.get(student_id) != null) {
					secondRequest = second.approvedRequests.get(student_id).get(randomActivity_id);
				}
				
				if (secondRequest == null) {
					requestsForStudent.remove(randomActivity_id);
				} else {
					requestsForStudent.put(randomActivity_id, secondRequest);
				}
			}
			
			if (!requestsForStudent.isEmpty()) {
				childRequests.put(student_id, requestsForStudent);
			}
		}
		
		return childRequests;
	}
	
	private UtilityChromosome crossover2(UtilityChromosome p1, UtilityChromosome p2) {
		UtilityChromosome child = new UtilityChromosome(data, p1);
		child.getValidSolution(rand);
		double deleteConst = 0.15;
		int deleteCnt = (int)(p1.getRequestsGranted()*deleteConst);
		List<Long> approvedStudents1 = new ArrayList<>(p1.approvedRequests.keySet());
		
		// Delete some requests from P1
		while (deleteCnt > 0) {
			Collections.shuffle(approvedStudents1);
			for (int i=0; i<approvedStudents1.size(); i++) {
				long student_id = approvedStudents1.get(i);
				if (!child.approvedRequests.containsKey(student_id))
					continue;
				
				List<Request> deleteList = new ArrayList<>();
				
				for (Request r : child.approvedRequests.get(student_id).values()) {
					if (rand.nextBoolean())
						deleteList.add(r);
				}
				
				for (Request r : deleteList) {
					if (child.removeRequest(r)) {
						deleteCnt--;
						if (deleteCnt <= 0)
							break;
					}
				}
			}
		}
		
		List<Long> approvedStudents2 = new ArrayList<>(p2.approvedRequests.keySet());
		boolean hasAdded = true;
		while (hasAdded) {
			hasAdded = false;
			Collections.shuffle(approvedStudents2);
			for (int i=0; i<approvedStudents2.size(); i++) {
				long student_id = approvedStudents2.get(i);
				
				for (Request r : p2.approvedRequests.get(student_id).values()) {
					if (child.grantRequest(r))
						hasAdded = true;
				}
			}
		}
		
		child.getValidSolution(rand);
		if (allowGreedy)
			child.greed(rand);
		
		return child;
	}

	private void mutate1(Map<Long, Map<Long, Request>> childRequests) {
		if (rand.nextDouble() >= 0)
			return;
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			if (rand.nextDouble() >= cm)
				continue;
			
			Map<Long, Request> requestsForStudent = new HashMap<>();
			
			for (List<Request> requests : data.requestsMapPossible.get(student_id).values()) {
				int indexToGrant = rand.nextInt(requests.size() + 1) - 1;
				if (indexToGrant >= 0) {
					Request r = requests.get(indexToGrant);
					requestsForStudent.put(r.activity_id, r);
				}
			}
			
			childRequests.put(student_id, requestsForStudent);
		}
	}

	@Override
	public void mutate(UtilityChromosome child) {
		if (rand.nextDouble() >= pm)
			return;
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			if (rand.nextDouble() >= cm)
				continue;
			
			Map<Long, Request> requestsForStudent = new HashMap<>();
			
			for (List<Request> requests : data.requestsMapPossible.get(student_id).values()) {
				int indexToGrant = rand.nextInt(requests.size() + 1) - 1;
				if (indexToGrant >= 0) {
					Request r = requests.get(indexToGrant);
					requestsForStudent.put(r.activity_id, r);
				}
			}
			
			child.approvedRequests.put(student_id, requestsForStudent);
		}
		
		child.getValidSolution(rand);
		if (allowGreedy)
			child.greed(rand);
		
	}

	@Override
	protected int extractGoodness(UtilityChromosome chrome) {
		return chrome.getGoodness();
	}
	
	@Override
	public void setLogActive(boolean b) {
		logActive = b;
	}
	
	@Override
	public String getAlgorithmLog() {
		if (logActive)
			return log.toString();
		return "";
	}
	
	private void appendLogHeader() {
		if (!logActive)
			return;
		log = new StringBuilder();
		log.append(String.format("iteration,time,goodness,requestsGranted\n"));
	}
	
	private void logIteration() {
		if (!logActive)
			return;
		log.append(String.format("%d,%d,%d,%d\n", t, timeProvider.getTimePassedMilliseconds(), 
				extractGoodness(best) - data.initialGoodness, best.getRequestsGranted()));
	}

	public void setAllowGreedy(boolean b) {
		allowGreedy = b;
	}
}
