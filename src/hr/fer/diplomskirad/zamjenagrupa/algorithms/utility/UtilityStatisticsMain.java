package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedGa;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.AdvancedSimmulatedAnnealing;
import hr.fer.diplomskirad.zamjenagrupa.algorithms.utility.UtilitySimmulatedAnnealing;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.SimpleTimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

/**
 * This program solves optimization problem defined in file: Upute_zamjena_grupa.pdf.
 * In this optimization problem students want to transfer groups and they created a bundle of requests.
 * Without breaking hard constraints defined in the Upute_zamjena_grupa.pdf you have to grant as many
 * of the requests as possible, more specifically you have to gain the best possible score for granting those requests.
 * This program uses genetic algorithm to find the best solution: the best possible requests that have to be granted 
 * from the bundle to ensure the highest score.
 * @author Josip Kasap
 *
 */
public class UtilityStatisticsMain {
	
	static long timeout = 600*1000;
	
	static final long TIME_90m = 9*600*1000;
	static final long TIME_60m = 6*600*1000;
	static final long TIME_10m = 600*100;
	static final long TIME_5m = 300*1000;
	static final long TIME_2m = 120*1000;
	static final long TIME_1m = 60*1000;
	
	static long startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static String[] dirs = {"samples\\Pravi1", "samples\\Sample4", "samples\\Sample7"};
	static ITimeProvider timeProvider;
	
	static boolean allowGreedy = false;
	static boolean logActive = true;
	static int maxEval = 100000000;
	
	
	static String utilityBase = "statistics\\results";
	static String fileOut;
	static int N = 6;
	static int N2 = 10;
	
	static final String GA = "UtilityGA";
	static final String SA = "UtilitySA";
	static final String TS = "UtilityTS";
	static final String PSO = "UtilityPSO";
	static final String GA_ADV = "AdvancedGa";
	static final String SA_ADV = "AdvancedSa";
	
	static final List<String> ALGORITHMS;
	
	static {
		ALGORITHMS = new ArrayList<>();
		ALGORITHMS.add(GA);
		ALGORITHMS.add(SA);
		ALGORITHMS.add(TS);
		ALGORITHMS.add(PSO);
		ALGORITHMS.add(GA_ADV);
		//ALGORITHMS.add(SA_ADV);
	}
	
	// Environment of this program
	static DataAdvanced data;
	
	public static void main(String[] args) throws IOException {
		for (int d=0; d<dirs.length; d++)
			for (int k=0; k<ALGORITHMS.size(); k++)
				solve(dirs[2], ALGORITHMS.get(k), TIME_60m, 10);
	}
	
	private static void solve(String directory, String alg, long time, int id) throws IOException {
		initialiseParameters(directory, alg, time, id);
		timeProvider = new SimpleTimeProvider(timeout);
		
		printStatment("Preprocessing started on directory: " + dir + ".");
		
		preprocessing();
		
		printStatment("Preprocessing done! Solving with algorithm: " + alg);
		
		solveWithAlgorithm(alg);
		printStatment(alg + " is done!");
		
		System.out.printf("##################################################################################################\n");
	}
	
	private static void solveWithAlgorithm(String alg) throws IOException {
		if (alg.equalsIgnoreCase(GA)) {
			solveWithGA();
		} else if (alg.equalsIgnoreCase(SA)) {
			solveWithSA();
		} else if (alg.equalsIgnoreCase(TS)) {
			solveWithTS();
		} else if (alg.equalsIgnoreCase(PSO)) {
			solveWithPSO();
		} else if (alg.equalsIgnoreCase(GA_ADV)) {
			solveWithGApp();
		} else if (alg.equalsIgnoreCase(SA_ADV)) {
			solveWithSApp();
		}
	}
	
	private static void solveWithUtilityAlgorithm(UtilityAlgorithm alg) throws IOException {
		alg.setAllowGreedy(allowGreedy);
		alg.setLogActive(logActive);
		alg.doYourJob();
		
		String log = alg.getAlgorithmLog();
		try {
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(fileOut, log);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
			throw e;
		}
	}
	
	private static void solveWithSA() throws IOException {
		UtilitySimmulatedAnnealing alg = new UtilitySimmulatedAnnealing(data, timeProvider, maxEval);
		solveWithUtilityAlgorithm(alg);
	}

	private static void solveWithGA() throws IOException {
		int popSize = 400;
		double pm = 0.15;
		
		UtilityGeneticAlgorithm alg = new UtilityGeneticAlgorithm(data, timeProvider, popSize, maxEval, pm);
		solveWithUtilityAlgorithm(alg);
	}
	
	private static void solveWithPSO() throws IOException {
		int swarmSize = 200;
		
		UtilityParticleSwarmOptimization alg = new UtilityParticleSwarmOptimization(data, timeProvider, swarmSize, maxEval);
		solveWithUtilityAlgorithm(alg);
	}
	
	private static void solveWithTS() throws IOException {
		UtilityTabooSearch alg = new UtilityTabooSearch(data, timeProvider, maxEval);
		solveWithUtilityAlgorithm(alg);
	}
	
	private static void solveWithGApp() throws IOException {
		int popSize = 400;
		double pm = 0.02;
		
		AdvancedGa alg = new AdvancedGa(data, timeProvider, popSize, maxEval, pm);
		alg.setLogActive(logActive);
		alg.doYourJob();
		
		String log = alg.getAlgorithmLog();
		
		try {
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(fileOut, log);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}

	private static void solveWithSApp() throws IOException {
		AdvancedSimmulatedAnnealing alg = new AdvancedSimmulatedAnnealing(data, timeProvider, maxEval);
		alg.setLogActive(logActive);
		alg.doYourJob();
		
		String log = alg.getAlgorithmLog();
		
		try {
			System.out.printf("\nWriting results to file: %s.\n", fileOut);
			Utils.writeToFile(fileOut, log);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}
	
	private static void printStatment(String s) {
		double timePassed = 1.0*(System.currentTimeMillis() - startTime) / 1000.0;
		s += "\tTime passed = %.3f seconds\n";
		System.out.printf(s, timePassed);
	}
	
	/**
	 * Initializes program parameters from command line arguments:<p>
	 * <pre>
	 * "-timeout": how long will the program run (in seconds)
	 * "-award-activity": award points for each of student request for activity that is granted
	 * "-award-student": award points for student that has all of his requests for activity granted
	 * "-minmax-penalty": penalty for exceeding the preferred count of students in classroom
	 * "-students-file": position of students-file
	 * "-requests-file": position of requests-file
	 * "-overlaps-file": position of overlaps-file
	 * "-limits-file": position of limits-file
	 * </pre>
	 * @param args command line arguments
	 */
	private static void initialiseParameters(String directory, String algorithm, long time, int id) {
		startTime = System.currentTimeMillis();
		dir = directory;
		timeout = time + startTime;
		
		students = dir + File.separatorChar + "students.csv";
		limits = dir + File.separatorChar + "limits.csv";
		overlaps = dir + File.separatorChar + "overlaps.csv";
		requests = dir + File.separatorChar + "requests.csv";
		
		String dirF = dir.substring(dir.indexOf('\\') + 1);
		
		String fOut = dirF +"_" + algorithm + "_" + id + ".csv";
		
		fileOut = utilityBase + File.separatorChar + fOut;
	}

	private static void preprocessing() throws IOException {
		data = new DataAdvanced(students, requests, overlaps, limits, award_activity, award_student, minmax_penalty);
	}

}
