package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import hr.fer.diplomskirad.zamjenagrupa.data.Constants;
import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.SimpleTimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

/**
 * This program solves optimization problem defined in file: Upute_zamjena_grupa.pdf.
 * In this optimization problem students want to transfer groups and they created a bundle of requests.
 * Without breaking hard constraints defined in the Upute_zamjena_grupa.pdf you have to grant as many
 * of the requests as possible, more specifically you have to gain the best possible score for granting those requests.
 * This program uses genetic algorithm to find the best solution: the best possible requests that have to be granted 
 * from the bundle to ensure the highest score.
 * @author Josip Kasap
 *
 */
public class UtilityMain {
	
	static long timeout = 600*1000, startTime;
	static int[] award_activity = {1, 2, 4};
	static int award_student = 1;
	static int minmax_penalty = 1;
	static String students, requests, overlaps, limits, dir = "";
	static ITimeProvider timeProvider;
	static String out = "out.csv";
	static String fileOut;
	
	// Environment of this program
	static DataAdvanced data;
	
	public static void main(String[] args) throws IOException {
		initialiseParameters(Constants.SAMPLE7_FULL, Constants.TIME5m);
		timeProvider = new SimpleTimeProvider(timeout);
		
		printStatment("Preprocessing started on directory: " + dir + ".");
		
		preprocessing();
		
		printStatment("Preprocessing done!");
		
		//greedy();
		solveWithGa();
		//solveWithPSO();
		//testPSO();
		//solveWithTS();
		//solveWithSA();
		
		printStatment("Algorithm is done!");
		
	}
	
	private static void testPSO() {
		Random rand = new Random();
		
		UtilityChromosome x1 = UtilityChromosome.getRandomChromosome(data, rand);
		x1.validateAndGreed(rand);
		
		UtilityChromosome x2 = UtilityChromosome.getRandomChromosome(data, rand);
		x2.validateAndGreed(rand);
		
		UtilityPSOVelocity v = UtilityPSOVelocity.substractPositions(x1, x2, data);
		
		UtilityChromosome x1New = v.addVelocityToPosition(x2, rand);
		
		System.out.println("Testing differences ... ");
		int diffs = 0;
		
		for (long student_id : data.requestsMapPossible.keySet()) {
			
			Map<Long, Request> x1ApprovedForStudent = x1.approvedRequests.getOrDefault(student_id, new HashMap<>());
			Map<Long, Request> x1NewApprovedForStudent = x1New.approvedRequests.getOrDefault(student_id, new HashMap<>());
			Map<Long, List<Request>> possibleRequestsForStudent = data.requestsMapPossible.get(student_id);
			
			for (long activity_id : possibleRequestsForStudent.keySet()) {
				
				Request r1 = x1ApprovedForStudent.get(activity_id);
				Request r2 = x1NewApprovedForStudent.get(activity_id);
				
				if (r1 != null && r2 == null) {
					System.out.printf("%s - %s\n", r1, "#");
					diffs++;
				} else if (r2 != null && r1 == null) {
					System.out.printf("%s - %s\n", "#", r2);
					diffs++;
				} else if (r1 != null && r2 != null && !r1.equals(r2)) {
					System.out.printf("%s - %s\n", r1, r2);
					diffs++;
				}
			}
		}
		
		if (diffs == 0) {
			System.out.println("No differences found :D");
		} else {
			System.out.println();
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			System.out.println();
			System.out.printf("Found %d differences\n", diffs);
		}
	}
	
	private static void solveWithSA() {
		int maxEval = 10000000;
		boolean allowGreedy = true;
		
		UtilitySimmulatedAnnealing sa = new UtilitySimmulatedAnnealing(data, timeProvider, maxEval);
		sa.setAllowGreedy(allowGreedy);
		sa.doYourJob();
		
		try {
			UtilityChromosome best = sa.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.approvedRequests);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}
	
	private static void solveWithTS() {
		int maxEval = 10000000;
		boolean allowGreedy = false;
		
		UtilityTabooSearch ts = new UtilityTabooSearch(data, timeProvider, maxEval);
		ts.setAllowGreedy(allowGreedy);
		ts.doYourJob();
		
		try {
			UtilityChromosome best = ts.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.approvedRequests);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}

	private static void solveWithPSO() {
		int swarmSize = 150;
		int maxEval = 10000000;
		boolean allowGreedy = false;
		
		UtilityParticleSwarmOptimization pso = new UtilityParticleSwarmOptimization(data, timeProvider, swarmSize, maxEval);
		pso.setAllowGreedy(allowGreedy);
		pso.doYourJob();
		
		try {
			UtilityChromosome best = pso.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.approvedRequests);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}

	
	private static void solveWithGa() {
		int popSize = 420;
		int maxEval = 10000000;
		double pm = 0.02;
		
		UtilityGeneticAlgorithm ga = new UtilityGeneticAlgorithm(data, timeProvider, popSize, maxEval, pm);
		ga.setAllowGreedy(false);
		ga.doYourJob();
		
		try {
			UtilityChromosome best = ga.getBest();
			System.out.printf("\nWriting results to file: %s.\n", out);
			Utils.writeToFile(students, fileOut, best.approvedRequests);
		} catch (IOException e) {
			System.out.println("Unexpected error happened while trying to write result to file: " + e.getMessage());
		}
	}


	private static void greedy() {
		UtilityChromosome chrome = new UtilityChromosome(data);
		Random rand = new Random();
		
		Utils.printGoodness(chrome.getGoodnessParameters());
		
		chrome.greed(rand);
		chrome.getGoodness();
		
		Utils.printGoodness(chrome.getGoodnessParameters());
		
		String fileOut = dir + File.separatorChar + "out_test.csv";
		
		try {
			Utils.writeToFile(students, fileOut, chrome.approvedRequests);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void printStatment(String s) {
		s += "\tTime passed = %s.\n";
		System.out.printf(s, timeProvider.stringTimePassed());
	}
	
	/**
	 * Initializes program parameters from command line arguments:<p>
	 * <pre>
	 * "-timeout": how long will the program run (in seconds)
	 * "-award-activity": award points for each of student request for activity that is granted
	 * "-award-student": award points for student that has all of his requests for activity granted
	 * "-minmax-penalty": penalty for exceeding the prefered count of students in classroom
	 * "-students-file": position of students-file
	 * "-requests-file": position of requests-file
	 * "-overlaps-file": position of overlaps-file
	 * "-limits-file": position of limits-file
	 * </pre>
	 * @param args command line arguments
	 */
	private static void initialiseParameters(String samples, long time) {
		
		startTime = System.currentTimeMillis();
		timeout = time + startTime;
		students = samples + File.separatorChar + "students.csv";
		limits = samples + File.separatorChar + "limits.csv";
		overlaps = samples + File.separatorChar + "overlaps.csv";
		requests = samples + File.separatorChar + "requests.csv";
		
		fileOut = samples + File.separatorChar + out;
		
		Utils.checkExists(students, dir, "students.csv");
		Utils.checkExists(requests, dir, "requests.csv");
		Utils.checkExists(limits, dir, "limits.csv");
		Utils.checkExists(overlaps, dir, "overlaps.csv");
	}

	private static void preprocessing() throws IOException {
		data = new DataAdvanced(students, requests, overlaps, limits, award_activity, award_student, minmax_penalty);
	}

}
