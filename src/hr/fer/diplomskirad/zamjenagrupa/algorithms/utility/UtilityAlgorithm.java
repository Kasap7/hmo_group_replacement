package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

public interface UtilityAlgorithm {
	void doYourJob();
	UtilityChromosome getBest();
	int getIterationCount();
	void setLogActive(boolean b);
	String getAlgorithmLog();
	void setAllowGreedy(boolean b);
}
