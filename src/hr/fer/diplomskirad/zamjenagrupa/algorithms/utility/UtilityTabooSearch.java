package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils.Pair;

public class UtilityTabooSearch implements UtilityAlgorithm {

	private UtilityTabooList tabooList;
	private int C;
	private DataAdvanced data;
	private ITimeProvider timeProvider;
	private UtilityChromosome best;
	private UtilityChromosome current;
	private Random rand;
	private int t = 0;
	private int maxEval;
	private int bestIter = 0;
	private static final int TERMINATION_THR = 10000000;
	private StringBuilder log;
	
	private int K;
	private int M;
	private double tabooSize = 0.085;
	private double kPercentage = 0.01;
	private static final int K_MIN = 5;
	private static final int K_MAX = 8;
	
	private boolean allowGreedy = false;
	private boolean terminateInactivity = false;
	private boolean logActive = false;
	
	public UtilityTabooSearch(DataAdvanced data, ITimeProvider timeProvider, int maxEval) {
		this.data = data;
		this.timeProvider = timeProvider;
		this.maxEval = maxEval;
		this.rand = new Random();
		
		initializeTSParams();
	}

	private void initializeTSParams() {
		C = getCapacity();
		K = calculateK();
		M = calculateM();
	}

	private int calculateM() {
		//System.out.println(Math.min(K*11, 60));
		return Math.min(K*11, 60);
	}

	private int calculateK() {
		int studentsCnt = data.students.size();
		int k = (int)(kPercentage*studentsCnt);
		k = Math.max(Math.min(K_MIN, studentsCnt), k);
		k = Math.min(k, K_MAX);
		return k;
	}

	private int getCapacity() {
		int requestActivities = getRequestActivities();
		return (int)(tabooSize * requestActivities);
	}

	private int getRequestActivities() {
		return (int) data.requestList.stream().map(r -> Pair.of(r.student_id, r.activity_id)).distinct().count();
	}

	@Override
	public void doYourJob() {
		current = best = getInitialSolution();
		t++;
		tabooList = new UtilityTabooList(C);
		int previouslyBestGoodness = extractGoodness(best);
		long currentTime = System.currentTimeMillis();
		int iterationWithoutProgress = 0;
		
		System.out.println();
		printInitialGoodness();
		Utils.printHeader();
		printCurrent();
		
		appendLogHeader();
		logIteration();
		
		while (t < maxEval && timeProvider.isTimedOut() == false) {
			t++;
			iterationWithoutProgress++;
			
			UtilityChromosome currentNext = getBestNeighbour();
			updateTabooList(currentNext);
			current = currentNext;
			
			if (extractGoodness(current) > extractGoodness(best)) {
				best = current;
				bestIter = t;
				logIteration();
				currentTime = System.currentTimeMillis();
				iterationWithoutProgress = 0;
			}
			
			if (t % 100 == 0) {
				printIteration();
			}
			//printCurrent();
			/*
			if (t % 100 == 0 && previouslyBestGoodness > extractGoodness(best)) {
				printBest();
			} else if (t % (4*100) == 0 && (System.currentTimeMillis() - currentTime) > 30*1000) {
				printBest();
				currentTime = System.currentTimeMillis();
			}
			*/
			
			if (terminateInactivity && iterationWithoutProgress >= TERMINATION_THR) {
				System.out.println(" >>> No progress for a long time, terminating the algorithm...");
				break;
			}
		}
		
		printBest();
		logIteration();
	}

	private void printIteration() {
		printCurrent();
		printBest();
		System.out.println("   ...   ");
		
	}

	private void printCurrent() {
		Utils.printIteration(current.getGoodnessParameters(), t, timeProvider, current.getRequestsGranted());
	}
	
	private void printBest() {
		Utils.printIteration(best.getGoodnessParameters(), bestIter, timeProvider, best.getRequestsGranted());
	}

	private void updateTabooList(UtilityChromosome currentNext) {
		for (long student_id : data.requestsMapPossible.keySet()) {
			if (!currentNext.approvedRequests.containsKey(student_id) && !current.approvedRequests.containsKey(student_id)) {
				continue;
			} else if (!currentNext.approvedRequests.containsKey(student_id)) {
				for (Request r : current.approvedRequests.get(student_id).values()) {
					tabooList.add(null, r.student_id, r.activity_id);
				}
			} else if (!current.approvedRequests.containsKey(student_id)) {
				for (Request r : currentNext.approvedRequests.get(student_id).values()) {
					tabooList.add(r, r.student_id, r.activity_id);
				}
			} else {
				for (long activity_id : data.requestsMapPossible.get(student_id).keySet()) {
					Request rNext = currentNext.approvedRequests.get(student_id).get(activity_id);
					Request rCurrent = current.approvedRequests.get(student_id).get(activity_id);
					if ((rNext == null && rCurrent != null) || (rNext != null && rCurrent == null) || 
							((rNext != null && rCurrent != null && !rNext.equals(rCurrent)))) {
						tabooList.add(rNext, student_id, activity_id);
					}
				}
			}
		}
	}

	private int extractGoodness(UtilityChromosome chrome) {
		if (chrome != null)
			return chrome.getGoodness();
		return Integer.MIN_VALUE;
	}

	private UtilityChromosome getBestNeighbour() {
		int m = M;
		UtilityChromosome nextBest = null;
		
		while (m > 0) {
			UtilityChromosome chrome = getANeighbour();
			
			if (tabooList.isStudentTaboo(chrome.approvedRequests)) {
				continue;
			}
			
			m--;
			
			int chromeGoodness = extractGoodness(chrome);
			int nextBestGoodness = extractGoodness(nextBest);
			
			if (chromeGoodness > nextBestGoodness) {
				nextBest = chrome;
			}
		}
		
		return nextBest;
	}

	private UtilityChromosome getANeighbour() {
		long[] student_ids = getStudentIDs();
		
		Map<Long, Map<Long, Request>> approvedRequests = copyApprovedFromCurrent();
		
		for (long student_id : student_ids) {
			Map<Long, Request> approved = new HashMap<>();
			
			for (List<Request> reqList : data.requestsMapPossible.get(student_id).values()) {
				int index = getIndex(reqList);
				if (index >= 0) {
					Request r = reqList.get(index);
					approved.put(r.activity_id, r);
				}
			}
			
			if (!approved.isEmpty()) {
				approvedRequests.put(student_id, approved);
			}
		}
		
		UtilityChromosome chrome = new UtilityChromosome(data, approvedRequests);
		chrome.getValidSolution(rand);
		if (allowGreedy) 
			chrome.greed(rand);
		
		return chrome;
	}

	private long[] getStudentIDs() {
		Set<Integer> studentIndexes = new HashSet<>();
		long[] student_ids = new long[K];
		int studentsCnt = data.students.size();
		
		for (int i=0; i<K; i++) {
			int index = rand.nextInt(studentsCnt);
			while (studentIndexes.contains(index)) {
				index = rand.nextInt(studentsCnt);
			}
			studentIndexes.add(index);
			student_ids[i] = data.students.get(index);
		}
		
		return student_ids;
	}

	private int getIndex(List<Request> reqList) {
		if (reqList == null || reqList.isEmpty())
			return -1;
		long activity_id = reqList.get(0).activity_id;
		long student_id = reqList.get(0).student_id;
		int index = tabooList.getTabooIndex(student_id, activity_id, reqList);
		
		if (index <= -2) {
			int size = reqList.size();
			index = rand.nextInt(size + 1) - 1;
		}
		
		return index;
	}

	private Map<Long, Map<Long, Request>> copyApprovedFromCurrent() {
		Map<Long, Map<Long, Request>> approvedRequests = new HashMap<>();
		for (long student_id : current.approvedRequests.keySet()) {
			Map<Long, Request> approved = new HashMap<>();
			approvedRequests.put(student_id, approved);
			for (Request r : current.approvedRequests.get(student_id).values()) {
				approved.put(r.activity_id, r);
			}
		}
		return approvedRequests;
	}

	private UtilityChromosome getInitialSolution() {
		UtilityChromosome chrome = UtilityChromosome.getRandomChromosome(data, rand);
		chrome.getValidSolution(rand);
		if (allowGreedy)
			chrome.greed(rand);
		return chrome;
	}

	@Override
	public UtilityChromosome getBest() {
		return best;
	}
	
	private void printInitialGoodness() {
		Utils.printGoodnessReduced(new int[] {0, 0, 0, -data.initialGoodness, data.initialGoodness});
	}
	
	public void setAllowGreedy(boolean b) {
		allowGreedy = b;
	}
	
	@Override
	public int getIterationCount() {
		return t;
	}
	
	@Override
	public void setLogActive(boolean b) {
		logActive = b;
	}
	
	@Override
	public String getAlgorithmLog() {
		if (logActive)
			return log.toString();
		return "";
	}
	
	private void appendLogHeader() {
		if (!logActive)
			return;
		log = new StringBuilder();
		log.append(String.format("iteration,time,goodness,requestsGranted\n"));
	}
	
	private void logIteration() {
		if (!logActive)
			return;
		log.append(String.format("%d,%d,%d,%d\n", t, timeProvider.getTimePassedMilliseconds(), 
				extractGoodness(best) - data.initialGoodness, best.getRequestsGranted()));
	}
}
