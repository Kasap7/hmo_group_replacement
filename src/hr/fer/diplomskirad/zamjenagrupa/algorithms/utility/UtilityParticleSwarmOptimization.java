package hr.fer.diplomskirad.zamjenagrupa.algorithms.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import hr.fer.diplomskirad.zamjenagrupa.data.DataAdvanced;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;
import hr.fer.diplomskirad.zamjenagrupa.util.Utils;

public class UtilityParticleSwarmOptimization implements UtilityAlgorithm {
	
	private List<UtilityChromosome> x;
	private List<UtilityChromosome> p;
	private List<UtilityPSOVelocity> v;
	private List<Integer> f;
	private UtilityChromosome pG = null;
	private int bestGoodness = Integer.MIN_VALUE;
	
	private DataAdvanced data;
	private Random rand;
	private ITimeProvider timeProvider;
	private StringBuilder log;
	
	private int swarmSize;
	private int t;
	private double w, c1, c2;
	private double pm = 0.1;
	
	private boolean logActive = false;
	private boolean allowGreedy = true;
	private boolean keepVelocity = true;
	private boolean terminateInactivity = false;
	
	private int maxEval = 10000000;
	private static final int TERMINATION_THR = 10000000;
	
	public UtilityParticleSwarmOptimization(DataAdvanced data, ITimeProvider timeProvider, int swarmSize, int maxEval) {
		this.data = data;
		this.timeProvider = timeProvider;
		this.swarmSize = swarmSize;
		this.maxEval = maxEval;
		
		rand = new Random();
		t  = 0;
		
		w  = 0.4;
		c1 = 0.85;
		c2 = 0.95;
	}
	
	public void initializeSwarm() {
		x = new ArrayList<>(swarmSize);
		v = new ArrayList<>(swarmSize);
		p = new ArrayList<>(swarmSize);
		f = new ArrayList<>(swarmSize);
		
		for (int i=0; i<swarmSize; i++) {
			t++;
			UtilityChromosome chrome = UtilityChromosome.getRandomChromosome(data, rand);
			
			chrome.getValidSolution(rand);
			if (allowGreedy)
				chrome.greed(rand);
			
			UtilityPSOVelocity velocity = UtilityPSOVelocity.getRandomVelocity(rand, data);
			int goodness = extractGoodness(chrome);
			
			x.add(chrome);
			p.add(chrome);
			v.add(velocity);
			f.add(goodness);
			
			if (pG == null || goodness > bestGoodness) {
				pG = chrome;
				bestGoodness = goodness;
				logIteration();
			}
			
		}
	}
	
	private int extractGoodness(UtilityChromosome chrome) {
		return chrome.getGoodness();
	}
	
	public void doYourJob() {
		appendLogHeader();
		System.out.println();
		printInitialGoodness();
		
		initializeSwarm();
		
		Utils.printHeader();
		Utils.printIteration(pG.getGoodnessParameters(), t, timeProvider, pG.getRequestsGranted());
		
		int previouslyBestGoodness = bestGoodness;
		int iterationWithoutProgress = 0;
		long currentTime = System.currentTimeMillis();
		boolean noProgress = false;
		
		while (t < maxEval && timeProvider.isTimedOut() == false) {
			
			List<UtilityChromosome> xNew = new ArrayList<>(swarmSize);
			List<UtilityChromosome> pNew = new ArrayList<>(swarmSize);
			List<UtilityPSOVelocity> vNew = new ArrayList<>(swarmSize);
			List<Integer> fNew = new ArrayList<>(swarmSize);
			for (int i = 0; i < swarmSize; i++) {
				t++;
				//System.out.println(t);
				iterationWithoutProgress++;
				
				UtilityPSOVelocity vi = v.get(i);
				UtilityChromosome  xi = x.get(i);
				UtilityChromosome  pi = p.get(i);
				
				UtilityPSOVelocity viNew = getNewVelocity(xi, pi, vi);
				
				UtilityChromosome xiNew = getNewPosition(xi, viNew);
				
				if (keepVelocity) {
					viNew = updateVelocity(xiNew, xi);
					mutate(viNew);
				} else {
					viNew = UtilityPSOVelocity.getRandomVelocity(rand, data);
				}
				
				xNew.add(xiNew);
				vNew.add(viNew);
				
				if (updateBest(xiNew, i, pNew, fNew)) {
					logIteration();
					iterationWithoutProgress = 0;
					currentTime = System.currentTimeMillis();
				}
				
				if (t % swarmSize == 0 && bestGoodness > previouslyBestGoodness) {
					Utils.printIteration(pG.getGoodnessParameters(), t, timeProvider, pG.getRequestsGranted());
					previouslyBestGoodness = extractGoodness(pG);
				} else if (t % (4*swarmSize) == 0 && (System.currentTimeMillis() - currentTime) > 30*1000) {
					Utils.printIteration(pG.getGoodnessParameters(), t, timeProvider, pG.getRequestsGranted());
					currentTime = System.currentTimeMillis();
				}
				
				if (terminateInactivity && iterationWithoutProgress >= TERMINATION_THR) {
					System.out.println(" >>> No progress for a long time, terminating the algorithm...");
					noProgress = true;
					break;
				}
				
				x.set(i, null);
				v.set(i, null);
				p.set(i, null);
			}
			
			if (noProgress)
				break;
			
			x = xNew;
			v = vNew;
			p = pNew;
			f = fNew;
		}
		
		Utils.printIteration(pG.getGoodnessParameters(), t, timeProvider, pG.getRequestsGranted());
		logIteration();
	}

	private void mutate(UtilityPSOVelocity vi) {
		if (rand.nextDouble() >= pm)
			return;
		
		int cm = 4;
		double pM = pm;
		
		Map<Long, Map<Long, List<Request>>> requestsPossibleMap = data.requestsMapPossible;
		for (long student_id : requestsPossibleMap.keySet()) {
			Map<Long, List<Request>> requestsForStudent = requestsPossibleMap.get(student_id);
			for (long activity_id : requestsForStudent.keySet()) {
				if (rand.nextDouble() < pM) {
					int indexOld = vi.velocity.get(student_id).get(activity_id);
					indexOld += (rand.nextInt(cm) - cm/2);
					vi.velocity.get(student_id).put(activity_id, indexOld);
				}
			}
		}
	}

	private UtilityPSOVelocity updateVelocity(UtilityChromosome xiNew, UtilityChromosome xi) {
		return UtilityPSOVelocity.substractPositions(xiNew, xi, data);
	}

	private boolean updateBest(UtilityChromosome xiNew, int i, List<UtilityChromosome> pNew, List<Integer> fNew) {
		int fi = f.get(i);
		UtilityChromosome pi = p.get(i);
		int goodness = extractGoodness(xiNew);
		
		if (goodness > fi) {
			fNew.add(goodness);
			pNew.add(xiNew);
			
			if (goodness > bestGoodness) {
				pG = xiNew;
				bestGoodness = goodness;
				return true;
			}
		} else {
			fNew.add(fi);
			pNew.add(pi);
		}
		
		return false;
	}

	private UtilityChromosome getNewPosition(UtilityChromosome xi, UtilityPSOVelocity vi) {
		UtilityChromosome chrome = vi.addVelocityToPosition(xi, rand);
		chrome.getValidSolution(rand);
		if (allowGreedy)
			chrome.greed(rand);
		return chrome;
	}

	private UtilityPSOVelocity getNewVelocity(UtilityChromosome xi, UtilityChromosome pi, UtilityPSOVelocity vi) {
		UtilityPSOVelocity currentDifference = UtilityPSOVelocity.substractPositions(pi, xi, data);
		UtilityPSOVelocity globalDifference = UtilityPSOVelocity.substractPositions(pG, xi, data);
		UtilityPSOVelocity vNew = UtilityPSOVelocity.add(data, vi, currentDifference, globalDifference, w, c1, c2, rand);
		
		return vNew;
	}

	private void printInitialGoodness() {
		Utils.printGoodnessReduced(new int[] {0, 0, 0, -data.initialGoodness, data.initialGoodness});
	}

	public UtilityChromosome getBest() {
		return pG;
	}

	public void setAllowGreedy(boolean b) {
		allowGreedy = b;
	}
	
	public void setKeepVelocity(boolean b) {
		keepVelocity = b;
	}
	
	@Override
	public int getIterationCount() {
		return t;
	}
	
	@Override
	public void setLogActive(boolean b) {
		logActive = b;
	}
	
	@Override
	public String getAlgorithmLog() {
		if (logActive)
			return log.toString();
		return "";
	}
	
	private void appendLogHeader() {
		if (!logActive)
			return;
		log = new StringBuilder();
		log.append(String.format("iteration,time,goodness,requestsGranted\n"));
	}
	
	private void logIteration() {
		if (!logActive)
			return;
		log.append(String.format("%d,%d,%d,%d\n", t, timeProvider.getTimePassedMilliseconds(), 
				extractGoodness(pG) - data.initialGoodness, pG.getRequestsGranted()));
	}
}
