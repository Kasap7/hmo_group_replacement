package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.Random;

import hr.fer.diplomskirad.zamjenagrupa.util.ITimeProvider;

public abstract class Algorithm {
	protected ITimeProvider timeProvider;
	protected Random rand = new Random();
	
	public Algorithm(ITimeProvider timeProvider) {
		this.timeProvider = timeProvider;
	}
	
	public abstract void doYourJob();
}
