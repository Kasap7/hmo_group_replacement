package hr.fer.diplomskirad.zamjenagrupa.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import hr.fer.diplomskirad.zamjenagrupa.data.Data;
import hr.fer.diplomskirad.zamjenagrupa.data.Request;
import hr.fer.diplomskirad.zamjenagrupa.data.Data.Chromosome;

/**
 * This class uses genetic algorithm to find the best solution for granting requests.
 * Ga used in this class has 3-tour selection:<br>
 * It selects 3 random solutions from the population. Worst from the 3 is replaced by the child of the other 2.
 * 
 * @author Josip Kasap
 *
 */
public class Ga {
	
	private Data data;
	private List<Chromosome> population;
	private Random rand = new Random();
	
	private int maxEval, popSize;
	private Chromosome best;
	private double pm;
	private double cm = 0.3, cs = 0.2;
	private int t = 0;
	private static final int TERMINATION_THR = 50000;
	
	public Ga(Data data, int popSize, int maxEval, double pm) {
		super();
		this.data = data;
		this.popSize = popSize;
		this.maxEval = maxEval;
		
		this.pm = pm;
	}
	
	/**
	 * Generates initial population filled with greedy solutions.
	 */
	private void generateInitialPopulation() {
		population = new ArrayList<>();
		
		for(int i=0; i < popSize; i++) {
			Chromosome c = data.greedySearch(rand);
			c.f();
			
			if (best == null) {
				best = c;
			} else if (c.goodness > best.goodness) {
				best = c;
			}
			population.add(c);
			t++;
		}
	}
	
	/**
	 * Runs ga to find best sulution untill timeout.
	 */
	public void doYourJob() {
		System.out.println();
		data.printInitialGoodness();
		
		generateInitialPopulation();
		
		printHeader();
		
		printIteration();
		
		int prevGoodness = best.goodness;
		
		int iterationWithoutProgress = 0;
		long currentTime = System.currentTimeMillis();
		
		while (t < maxEval && data.isTimedOut() == false) {
			t++;
			iterationWithoutProgress++;
			
			Chromosome[] turnResults = getTurnResults();
			population.remove(turnResults[0]);
			Chromosome c = crossover(turnResults[1], turnResults[2]);
			mutation(c);
			c.f();
			population.add(c);
			
			if (c.goodness > best.goodness) {
				best = c;
				iterationWithoutProgress = 0;
				currentTime = System.currentTimeMillis();
			}			
			
			if (t % popSize == 0 && best.goodness > prevGoodness) {
				printIteration();
				prevGoodness = best.goodness;
			} else if (t % (4*popSize) == 0 && (System.currentTimeMillis() - currentTime) > 30*1000) {
				printIteration();
				currentTime = System.currentTimeMillis();
			}
			
			if (iterationWithoutProgress >= TERMINATION_THR) {
				System.out.println(" >>> No progress for a long time, terminating the algorithm...");
				break;
			}
			
			regulateMutation();
		}
		printIteration();
	}
	
	public int getIterationCount() {
		return t;
	}
	
	private void printHeader() {
		System.out.printf("\n ITERATION | GOODNESS | REQ |      A |     B |     C |     D |     E |    SUM | Time Passed\n");
		System.out.printf(  " --------------------------------------------------------------------------------------------------\n");
	}

	/**
	 * Mutation should be low at the start of algorithm and should rise by its end.
	 */
	private void regulateMutation() {
		
		if (t % 2000 == 0 && pm < 0.2) {
			pm = 1.15*pm;
		}
	}

	/**
	 * This mutation roll-backs some portion (10%) of the granted requests and replace them with random requests that can be granted.
	 * @param c chromosome that is to be mutated
	 */
	private void mutation1(Chromosome c) {
		if (rand.nextDouble() > pm)
			return;
		
		int requestsGranted = c.getRequestsGranted();
		int requestsToBeErased = (int)(requestsGranted * cm);
		List<Request> grantedRequests = new ArrayList<>(c.grantedRequests);
		
		while (requestsToBeErased > 0) {
			Collections.shuffle(grantedRequests);
			for (int i=0; i<grantedRequests.size(); i++) {
				Request request = grantedRequests.get(i);
				boolean accepted = c.rollbackRequest(request);
				if (accepted == true) {
					requestsToBeErased--;
					if (requestsToBeErased <= 0)
						break;
				}
			}
		}
		
		c.greed(rand);
	}
	
	private void mutation(Chromosome c) {
		mutation1(c);
	}

	private Chromosome crossover(Chromosome c1, Chromosome c2) {
		if (rand.nextDouble() < 0.5) {
			return crossover1(c1,c2);
		}
		return crossover2(c1,c2);
	}

	private void printIteration() {
		/*
		System.out.printf(" Iteration[%d]: Goodness = %d; RequestsGranted = %d; pm = %f; Time passed = %s.\n",
				t, best.goodness - data.initialGoodness, best.getRequestsGranted(), pm, data.stringTimePassed());
		*/
		
		System.out.printf(" [%7d]: %7d %7d | %7d %7d %7d %7d %7d %7d | %s \n", 
				t, best.goodness - data.initialGoodness, best.getRequestsGranted(),
				best.a, best.b, best.c, -best.getD(), -best.getE(), best.goodness, data.stringTimePassed());
		/*
		System.out.printf("\tGOODNESS = %d, A = %d, B = %d, C = %d, D = %d, E = %d.\n\n",
				best.goodness, best.a, best.b, best.c, -best.getD(), -best.getE());
		*/
	}
	
	/**
	 * This crossover keeps requests from first parent, deletes some of the requests from first parent, and
	 * then tries to replace them with as many requests from second parent.
	 * @param c1 first parent
	 * @param c2 second parent
	 * @return child gained from crossover of 2 parents
	 */
	private Chromosome crossover2(Chromosome c1, Chromosome c2) {
		Chromosome c = c1.copy();
		
		List<Request> toBeDeletedRequests = new ArrayList<>();
		List<Request> toBeInsertedRequests = new ArrayList<>(c2.grantedRequests);
		for (Request r : c1.grantedRequests) {
			if (rand.nextDouble() < cs) {
				toBeDeletedRequests.add(r);
			}
		}
		
		deleteMaximum(c, toBeDeletedRequests);
		insertMaximum(c, toBeInsertedRequests);
		insertMaximum(c, data.requestList);
		
		return c;
	}
	
	/**
	 * This crossover keeps all the shared requests, deletes some of the non shared requests from first parent and
	 * tries to replace it with some non shared requests from second parent.
	 * @param c1 first parent
	 * @param c2 second parent
	 * @return child gained from crossover of 2 parents
	 */
	private Chromosome crossover1(Chromosome c1, Chromosome c2) {
		Chromosome c = c1.copy();
		List<Request> toBeDeletedRequests = new ArrayList<>();
		List<Request> toBeInsertedRequests = new ArrayList<>(c2.grantedRequests);
		for (Request r : c1.grantedRequests) {
			if (c2.grantedRequests.contains(r)) {
				toBeInsertedRequests.remove(r);
			} else if (rand.nextDouble() < 0.5) {
				toBeDeletedRequests.add(r);
			}
		}
		
		deleteMaximum(c, toBeDeletedRequests);
		insertMaximum(c, toBeInsertedRequests);
		insertMaximum(c, data.requestList);
		
		return c;
	}
	
	/**
	 * Inserts maximum requests into the chromosome from the list of request in random order.
	 * @param c chromosome
	 * @param requestList list of requests
	 */
	private void insertMaximum(Chromosome c, List<Request> requestList) {
		while (true) {
			int inserted = 0, prevGoodness = c.goodness;
			Collections.shuffle(requestList);
			
			for (int i=0; i<requestList.size(); i++) {
				Request request = requestList.get(i);
				boolean accepted = c.grantRequest(request);
				
				if (accepted == true) {
					if (prevGoodness > c.goodness) {
						if (c.rollbackPreviousRequest() == false) {
							System.out.println(">> ERROR; should not have happened.");
						}
					} else {
						inserted++;
					}
					
					prevGoodness = c.goodness;
				}
			}
			
			if (inserted == 0)
				break;
		}
	}
	
	/**
	 * Deletes maximum requests from the chromosome from the list of request in random order.
	 * @param c chromosome
	 * @param requestList list of requests
	 */
	private void deleteMaximum(Chromosome c, List<Request> requestList) {
		while (true) {
			int deleted = 0;
			Collections.shuffle(requestList);
			for (int i=0; i<requestList.size(); i++) {
				Request request = requestList.get(i);
				boolean accepted = c.rollbackRequest(request);
				if (accepted == true) {
					deleted++;
				}
			}
			
			if (deleted == 0)
				break;
		}
	}

	/**
	 * Returns 3 random chromosomes.
	 * @return 3 random chromosomes
	 */
	private Chromosome[] getTurnResults() {
		int x = rand.nextInt(popSize), y = rand.nextInt(popSize), z = rand.nextInt(popSize);
		while (x == y) {
			y = rand.nextInt(popSize);
		}
		while (y == z) {
			z = rand.nextInt(popSize);
		}
		
		Chromosome cx = population.get(x);
		Chromosome cy = population.get(y);
		Chromosome cz = population.get(z);
		Chromosome[] turnResults = new Chromosome[3];
		
		if (cx.goodness < cy.goodness && cx.goodness < cz.goodness) {
			turnResults[0] = cx;
			turnResults[1] = cy;
			turnResults[2] = cz;
		} else if (cy.goodness < cx.goodness && cy.goodness < cz.goodness) {
			turnResults[0] = cy;
			turnResults[1] = cx;
			turnResults[2] = cz;
		} else {
			turnResults[0] = cz;
			turnResults[1] = cx;
			turnResults[2] = cy;
		}
		
		return turnResults;
	}
	
	/**
	 * Returns best chromosome brom the current population.
	 * @return best chromosome brom the current population
	 */
	public Chromosome getBest() {
		return best;
	}
}
